﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class StorageInfo
    {
        private string _rKMXID;
        private string _cPID;
        private string _cPName;
        private string _cPZB;
        private string _gG;
        private string _xH;
        private string _dW;
        private int _sL;
        private string _batch;
        private string _yXQ;
        private string _gYS;
        private string _hQLX;
        private decimal _cKDJ;
        private string _kFID;
        private string _cPBH;
        private string _iD;

        public string RKMXID { get => _rKMXID; set => _rKMXID = value; }
        public string CPID { get => _cPID; set => _cPID = value; }
        public string CPName { get => _cPName; set => _cPName = value; }
        public string CPZB { get => _cPZB; set => _cPZB = value; }
        public string GG { get => _gG; set => _gG = value; }
        public string XH { get => _xH; set => _xH = value; }
        public string DW { get => _dW; set => _dW = value; }
        public int SL { get => _sL; set => _sL = value; }
        public string Batch { get => _batch; set => _batch = value; }
        public string YXQ { get => _yXQ; set => _yXQ = value; }
        public string GYS { get => _gYS; set => _gYS = value; }
        public string HQLX { get => _hQLX; set => _hQLX = value; }
        public decimal CKDJ { get => _cKDJ; set => _cKDJ = value; }
        public string KFID { get => _kFID; set => _kFID = value; }
        public string CPBH { get => _cPBH; set => _cPBH = value; }
        public string ID { get => _iD; set => _iD = value; }
    }
}
