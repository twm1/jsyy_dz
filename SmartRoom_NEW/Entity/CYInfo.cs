﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class CYInfo : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string _xMDM;
        private string _xMMC;
        private int _jFSL;
        private int _lYSL;
        private int _gHSL;
        private int _cYSL;
        private int _kCSL;
        private int _aQKC;
        private string _gG;
        private int _sHSL;

        public string XMDM
        {
            get { return _xMDM; }
            set
            {
                _xMDM = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("XMDM"));
            }
        }

        public string XMMC
        {
            get { return _xMMC; }
            set
            {
                _xMMC = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("XMMC"));
            }
        }

        public int JFSL
        {
            get { return _jFSL; }
            set
            {
                _jFSL = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("JFSL"));
            }
        }

        public int LYSL
        {
            get { return _lYSL; }
            set
            {
                _lYSL = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("LYSL"));
            }
        }

        public int GHSL
        {
            get { return _gHSL; }
            set
            {
                _gHSL = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("GHSL"));
            }
        }

        public int CYSL
        {
            get { return _cYSL; }
            set
            {
                _cYSL = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("CYSL"));
            }
        }

        public int KCSL { get => _kCSL; set => _kCSL = value; }
        public int AQKC { get => _aQKC; set => _aQKC = value; }
        public string GG { get => _gG; set => _gG = value; }
        public int SHSL { get => _sHSL; set => _sHSL = value; }
    }
}
