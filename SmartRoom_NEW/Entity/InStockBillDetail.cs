﻿using System.ComponentModel;

namespace Entity
{
    public class InStockBillDetail : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private string _sFBM;
        private string _cPMC;
        private string _gG;
        private string _sCCJ;
        private string _dW;
        private int _sL;

        public string SFBM
        {
            get { return _sFBM; }
            set
            {
                _sFBM = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SFBM"));
            }
        }
        public string CPMC
        {
            get { return _cPMC; }
            set
            {
                _cPMC = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("CPMC"));
            }
        }

        public string GG
        {
            get { return _gG; }
            set
            {
                _gG = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("GG"));
            }
        }

        public string SCCJ
        {
            get { return _sCCJ; }
            set
            {
                _dW = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SCCJ"));
            }
        }

        public string DW
        {
            get { return _dW; }
            set
            {
                _dW = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("DW"));
            }
        }

        public int SL
        {
            get { return _sL; }
            set
            {
                _sL = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SL"));
            }
        }
        
        

    }
}
