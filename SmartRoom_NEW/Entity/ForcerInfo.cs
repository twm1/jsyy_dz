﻿using System;

namespace Entity
{
    public class ForcerInfo
    {
        private string _iD;

        private string _hosID;

        private string _forcerNo;

        private string _forcerName;

        private string _remark;

        private bool _isDisable;

        private DateTime _dateTime;

        private string _userID;

        private string _macAddress;

        private string _kFID;

        private string _kfName;

        private string _depID;

        public string ID { get => _iD; set => _iD = value; }
        public string HosID { get => _hosID; set => _hosID = value; }
        public string ForcerNo { get => _forcerNo; set => _forcerNo = value; }
        public string ForcerName { get => _forcerName; set => _forcerName = value; }
        public string Remark { get => _remark; set => _remark = value; }
        public bool IsDisable { get => _isDisable; set => _isDisable = value; }
        public DateTime DateTime { get => _dateTime; set => _dateTime = value; }
        public string UserID { get => _userID; set => _userID = value; }
        public string MacAddress { get => _macAddress; set => _macAddress = value; }
        public string KFID { get => _kFID; set => _kFID = value; }
        public string DepID { get => _depID; set => _depID = value; }
        public string KfName { get => _kfName; set => _kfName = value; }
    }
}
