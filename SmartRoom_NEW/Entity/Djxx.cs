﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Djxx : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string code;
        private string msg;
        private string tag;
        private string data;

        public string Code { get => code; set => code = value; }
        public string Msg { get => msg; set => msg = value; }
        public string Tag { get => tag; set => tag = value; }
        public string Data { get => data; set => data = value; }
    }
}
