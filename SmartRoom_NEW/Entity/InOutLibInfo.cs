﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class InOutLibInfo
    {
        /// <summary>
        /// 序号
        /// </summary>
        public int SequenceNumber { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        public string ProductName { get; set; }
        /// <summary>
        /// 规格
        /// </summary>
        public string Model { get; set; }
        /// <summary>
        /// 型号
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// RFID编号
        /// </summary>
        public string RFID { get; set; }
        /// <summary>
        /// 生产厂商
        /// </summary>
        public string Manufacturer { get; set; }
        /// <summary>
        /// 入库时间
        /// </summary>
        public DateTime? InLibDate { get; set; }
        /// <summary>
        /// 存放人
        /// </summary>
        public string StoreMan { get; set; }
        /// <summary>
        /// 出库时间
        /// </summary>
        public DateTime? OutLibDate { get; set; }
        /// <summary>
        /// 领用人
        /// </summary>
        public string UseMan { get; set; }
    }
}
