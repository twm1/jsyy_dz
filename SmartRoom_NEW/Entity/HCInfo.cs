﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class HCInfo
    {
        //类型，业务时间，仓库，操作人，项目代码，商品名称，规格，数量
        private string type;
        private string time;
        private string wareHouse;
        private string userName;
        private string xmdm;
        private string goods_name;
        private string gg;
        private string count;
        public string Type { get => type; set => type = value; }
        public string Time { get => time; set => time = value; }
        public string WareHouse { get => wareHouse; set => wareHouse = value; }
        public string UserName { get => userName; set => userName = value; }
        public string Xmdm { get => xmdm; set => xmdm = value; }
        public string Goods_name { get => goods_name; set => goods_name = value; }
        public string Gg { get => gg; set => gg = value; }
        public string Count { get => count; set => count = value; }
    }
}
