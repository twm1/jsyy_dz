﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class InBillInfo
    {
        private string _billNo;

        private string _userID;

        private string _dJID;

        private string _kFID;

        private string _cKType;

        private string _userName;

        private string _remark;

        private string _depID;

        private string _cKKFName;

        private string _rKKFName;

        private string _dJLX;

        private string _sJ;

        private string _hosID;
        //单号类型 ckdh or rkdh
        private string _dHLX;


        private List<InBillDetailInfo> _detailList;

        public string BillNo { get => _billNo; set => _billNo = value; }
        public string UserID { get => _userID; set => _userID = value; }
        public string DJID { get => _dJID; set => _dJID = value; }
        public string KFID { get => _kFID; set => _kFID = value; }
        public string CKType { get => _cKType; set => _cKType = value; }
        public string UserName { get => _userName; set => _userName = value; }
        public string Remark { get => _remark; set => _remark = value; }
        public List<InBillDetailInfo> DetailList { get => _detailList; set => _detailList = value; }
        public string DepID { get => _depID; set => _depID = value; }
        public string CKKFName { get => _cKKFName; set => _cKKFName = value; }
        public string RKKFName { get => _rKKFName; set => _rKKFName = value; }
        public string DJLX { get => _dJLX; set => _dJLX = value; }
        public string SJ { get => _sJ; set => _sJ = value; }
        public string HosID { get => _hosID; set => _hosID = value; }
        public string DHLX { get => _dHLX; set => _dHLX = value; }
    }
}
