﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ProductInfo
    {
        private string _iD;

        public string ID
        {
            get { return _iD; }
            set { _iD = value; }
        }
        private string _cPID;
        /// <summary>
        /// CPID
        /// </summary>
        public string CPID
        {
            get { return _cPID; }
            set { _cPID = value; }
        }

        private string _onlyBarCode;

        public string OnlyBarCode
        {
            get { return _onlyBarCode; }
            set { _onlyBarCode = value; }
        }

        private string _productNo;
        /// <summary>
        /// 产品编号
        /// </summary>
        public string ProductNo
        {
            get { return _productNo; }
            set { _productNo = value; }
        }

        private string _productName;
        /// <summary>
        /// 产品名称
        /// </summary>
        public string ProductName
        {
            get { return _productName; }
            set { _productName = value; }
        }

        private string _productSpec;
        /// <summary>
        /// 产品规格
        /// </summary>
        public string ProductSpec
        {
            get { return _productSpec; }
            set { _productSpec = value; }
        }

        private string _productType;
        /// <summary>
        /// 产品型号
        /// </summary>
        public string ProductType
        {
            get { return _productType; }
            set { _productType = value; }
        }

        private string _supplier;
        /// <summary>
        /// 供应商
        /// </summary>
        public string Supplier
        {
            get { return _supplier; }
            set { _supplier = value; }
        }
        private string _forcerID;

        public string ForcerID
        {
            get { return _forcerID; }
            set { _forcerID = value; }
        }
        private int _qty;

        public int Qty
        {
            get { return _qty; }
            set { _qty = value; }
        }

        private string _rFID;

        public string RFID
        {
            get { return _rFID; }
            set { _rFID = value; }
        }

        private int _port;

        public int Port
        {
            get { return _port; }
            set { _port = value; }
        }

        private string _userNo;

        public string UserNo
        {
            get { return _userNo; }
            set { _userNo = value; }
        }

        private DateTime _inDateTime;

        public DateTime InDateTime
        {
            get { return _inDateTime; }
            set { _inDateTime = value; }
        }

        private DateTime _outDateTime;

        public DateTime OutDateTime
        {
            get { return _outDateTime; }
            set { _outDateTime = value; }
        }
    }
}
