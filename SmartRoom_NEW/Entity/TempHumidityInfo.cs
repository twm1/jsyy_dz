﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class TempHumidityInfo
    {
        public int ID { get; set; }
        public string ForcerNo { get; set; }
        public double Temperature { get; set; }
        public double Humidity { get; set; }
        public DateTime DateTime { get; set; }
        public int Hour { get; set; }
    }
}
