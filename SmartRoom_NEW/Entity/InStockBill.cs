﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class InStockBill
    {
        private string _bill_ID;

        private string _in_dept_name;

        private string _fill_date;

        private string _userID;
        public string Bill_ID { get => _bill_ID; set => _bill_ID = value; }
        public string In_dept_name { get => _in_dept_name; set => _in_dept_name = value; }
        public string Fill_date { get => _fill_date; set => _fill_date = value; }
        public string UserID { get => _userID; set => _userID = value; }
    }
}
