﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeviceTool
{
    public static class CModbusDll
    {
        public static byte[] WriteDO(int addr,int io,bool openclose)
        {
            byte[] src = new byte[8];
            src[0] = (byte)addr;
            src[1] = 0x05;
            src[2] = 0x00;
            src[3] = (byte)io;
            src[4] = (byte)((openclose) ? 0xff : 0x00);
            src[5] = 0x00;
            ushort crc = CMBRTU.CalculateCrc(src,6);
            src[6] = (byte)(crc & 0xff);
            src[7] = (byte)(crc>>8);
            return src;
        }
        public static byte[] WriteAllDO(int addr, int ionum, bool openclose)
        {
            byte[] src = new byte[10];
            src[0] = (byte)addr;
            src[1] = 0x0f;
            src[2] = 0x00;
            src[3] = 0x00;
            src[4] = 0x00;
            src[5] = (byte)ionum;
            src[6] = 0x01;
            src[7] = (byte)((openclose)?0xff:0x00);
            ushort crc = CMBRTU.CalculateCrc(src, 8);
            src[8] = (byte)(crc & 0xff);
            src[9] = (byte)(crc >> 8);
            return src;
        }
        public static byte[] ReadDO(int addr, int donum)
        {
            byte[] src = new byte[8];
            src[0] = (byte)addr;
            src[1] = 0x01;
            src[2] = 0x00;
            src[3] = 0x00;
            src[4] = 0x00;
            src[5] = (byte)donum;
            ushort crc = CMBRTU.CalculateCrc(src, 6);
            src[6] = (byte)(crc & 0xff);
            src[7] = (byte)(crc >> 8);
            return src;
        }
        public static byte[] ReadDI(int addr,int dinum)
        {
            byte[] src = new byte[8];
            src[0] = (byte)addr;
            src[1] = 0x02;
            src[2] = 0x00;
            src[3] = 0x00;
            src[4] = 0x00;
            src[5] = (byte)dinum;
            ushort crc = CMBRTU.CalculateCrc(src, 6);
            src[6] = (byte)(crc & 0xff);
            src[7] = (byte)(crc >> 8);
            return src;
        }

        /// <summary>
        /// 开门指令
        /// </summary>
        /// <param name="addr"></param>
        /// <param name="ionum"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        public static byte[] QuickOpen(int addr, int ionum,int time)
        {
            byte[] src = new byte[13];
            src[0] = (byte)addr;
            src[1] = 0x10;
            src[2] = 0x00;
            src[3] = (byte)(ionum == 0 ? 3 : ionum * 5 + 3);
            src[4] = 0x00;
            src[5] = 0x02;
            src[6] = 0x04;
            src[7] = 0x00;
            src[8] = 0x04;
            src[9] = 0x00;
            src[10] = (byte)time;
            ushort crc = CMBRTU.CalculateCrc(src, 11);
            src[11] = (byte)(crc & 0xff);
            src[12] = (byte)(crc >> 8);
            return src;
        }
    }
}
