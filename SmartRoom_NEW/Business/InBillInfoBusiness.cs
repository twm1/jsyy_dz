﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;

namespace Business
{
    
    public class InBillInfoBusiness
    {
        DataFactory.DataFactory dataFactory = new DataFactory.DataFactory();

        public string InsertCKInfo(InBillInfo obj)
        {
            if (obj == null)
                return "-100";
            return dataFactory.InsertCKInfo(obj);
        }

        public string InsertRKInfo(InBillInfo obj)
        {
            if (obj == null)
                return "-100";
            return dataFactory.InsertRKInfo(obj);
        }

        public List<InBillInfo> GetBillInfo(string KFID, string BeginDate, string EndDate, string LX)
        {
            return dataFactory.GetBillInfo(KFID,BeginDate,EndDate,LX);
        }

        public List<InBillDetailInfo> GetBillDetailInfo(string _CKID)
        {
            return dataFactory.GetBillDetailInfo(_CKID);
        }

    }
}
