﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;

namespace Business
{
    
    public class InStockBillBusiness
    {
        DataFactory.DataFactory dataFactory = new DataFactory.DataFactory();

        public List<InStockBill> GetInStockBill(string KFID)
        {
            return dataFactory.GetInStockBill(KFID);
        }

        public List<InStockBillDetail> GetInStockBillDetail(string KFID)
        {
            return dataFactory.GetInStockBillDetail(KFID);
        }

        public string InsertZDRKInfo(InStockBill obj)
        {
            if (obj == null)
                return "-1";

            return dataFactory.InsertZDRKInfo(obj);
        }

    }
}
