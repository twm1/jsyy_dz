﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;
using DataFactory;

namespace Business
{
    public class UserBusiness
    {
        DataFactory.DataFactory dataFactory = new DataFactory.DataFactory();
        public UserInfo GetUserInfoByNamePass(string UserName, string PassWord)
        {
            return dataFactory.GetUsersByNamePass(UserName,PassWord);
        }

        //public UserInfo GetUserInfoByKeyWord(string KeyWord)
        //{
        //    return dataFactory.GetUsersByKeyWord(KeyWord);
        //}

        public UserInfo GetUesrInfoByUserID(string UserID)
        {
            return dataFactory.GetUsersByUserID(UserID);
        }

        public List<UserFingerPrint> GetUserFingerPrint(string KFID)
        {
            return dataFactory.GetUserByFingerPrint(KFID);
        }

        //public List<UserInfo> GetUserListByKFID(string KFID)
        //{
        //    return dataFactory.GetUserListByKFID(KFID);
        //}
        /// <summary>
        /// 人脸信息
        /// </summary>
        /// <returns></returns>
        public List<UserInfo> GetUsersByface()
        //public List<UserInfo> GetUsersByface(string KFID)
        {
            return dataFactory.GetUsersByface();
            //return dataFactory.GetUsersByface(KFID);
        }
    }
}
