﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataFactory;
using Entity;

namespace Business
{    
    public class StorageBusiness
    {
        DataFactory.DataFactory factory = new DataFactory.DataFactory();

        public List<StorageInfo> GetStorageInfos(StorageInfo obj)
        {
            return factory.GetStorage(obj);
        }
        public List<HCInfo> getHCInfo(string id, string startDate, string enddate, string KFID)
        {
            return factory.getHCInfo(id, startDate, enddate, KFID);
        }
    }
}
