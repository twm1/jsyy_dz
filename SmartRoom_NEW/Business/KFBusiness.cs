﻿using Entity;
using System.Collections.Generic;
using DataFactory;

namespace Business
{
    public class KFBusiness
    {
        DataFactory.DataFactory m_DataFactory = new DataFactory.DataFactory();
        public List<KFInfo> GetAllKFInfo()
        {
            return m_DataFactory.GetKFList();
        }

        //public KFInfo GetZKKFInfo(string DepID)
        //{
        //    return m_DataFactory.GetZKKF(DepID);
        //}
    }
}
