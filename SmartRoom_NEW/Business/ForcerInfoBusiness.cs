﻿using Entity;
using System;

namespace Business
{
    public class ForcerInfoBusiness
    {
        DataFactory.DataFactory m_DataFactory = new DataFactory.DataFactory();
        /// <summary>
        /// 注册一条设备信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool InsertForcerInfo(ForcerInfo obj)
        {
            obj.ForcerNo = markOddNumber();
            return m_DataFactory.InsertForceInfo(obj);
        }


        /// <summary>
        /// 获取一条设备信息
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public ForcerInfo CheckForcerIsRegiser(string macAddress)
        {
            return m_DataFactory.CheckForcerIsRegiser(macAddress);
        }

        string markOddNumber()
        {
            string vResult = string.Format("JSYY_F{0}{1}{2}{3}",
                DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Millisecond);
            return vResult;
        }
    }
}
