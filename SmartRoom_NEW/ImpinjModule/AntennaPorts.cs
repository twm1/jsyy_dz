﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JW.UHF;

namespace ImpinjModule
{
    public class AntennaPorts
    {
        public int AntennaIndex { get; set; }
        public bool Exist { get; set; }
        public int Power { get; set; }
    }
}