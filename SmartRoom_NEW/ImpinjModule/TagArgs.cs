﻿using System;
using System.Text;


namespace ImpinjModule
{
    public class TagArgs:EventArgs
    {
        public TagArgs() { }

        public TagArgs(string epc, uint time, int port, float rssi, string tid, string ip)
        {
            Epc = epc;
            PortnNmber = port;
            Rssi = rssi;
            Tid = tid;
            Ip = ip;
        }

        public string Epc { get; set; }
        public int PortnNmber { get; set; }
        public float Rssi { get; set; }
        public string Tid { get; set; }
        public string Ip { get; set; }

        //public InventoryPacket Tag { get; set; }
    }
}
