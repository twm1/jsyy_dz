﻿using System;
using System.Text;

namespace ImpinjModule
{
    public class ReaderSetting
    {
        public ReaderSetting() { }
        public ReaderSetting(string ip, int timeout, int power, short rssi, uint _fixed)
        {
            ReaderIP = ip;
            TimeOut = timeout;
            Power = power;
            Rssi = rssi;
            Fixed = _fixed;
        }

        public string ReaderIP { get; set; }
        public int TimeOut { get; set; }
        public int Power { get; set; }
        public short Rssi { get; set; }
        public uint Fixed { get; set; }
        //test
        public double Frequency { get; set; }
    }
}
