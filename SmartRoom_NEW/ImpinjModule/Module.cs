﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Runtime.InteropServices;
using LNUtility;
using JW.UHF;
namespace ImpinjModule
{
    public class Module
    {
        #region 变量定义
        /// <summary>
        /// 返回状态
        /// </summary>
        Result result = Result.OK;
        /// <summary>
        /// 读写器对象
        /// </summary>
        public  JWReader jwReader = null;

        #region Add20160104
        /// <summary>
        /// 天线配置对象
        /// </summary>
        List<AntennaPort> listAp;
        #endregion

        #region Add20160112
        /// <summary>
        /// 是否监听
        /// </summary>
        private bool IsListen;

        /// <summary>
        /// 开启连接状态获取线程
        /// </summary>
        Thread ConnectStatusThread = null;

        /// <summary>
        /// 盘点是否被打断
        /// </summary>
        private bool IsReadInterrupt;
        #endregion

        #region Add20160121
        public bool IsNeedInventory;
        #endregion

        /// <summary>
        /// 是否读取
        /// </summary>
        bool IsRead;

        /// <summary>
        /// 开启读写线程
        /// </summary>
        Thread startThread = null;

        /// <summary>
        /// 读取事件
        /// </summary>
        public event EventHandler<TagArgs> ReadTag;
        /// <summary>
        /// 配置
        /// </summary>
        ReaderSetting rsetting;

        #region Add20160304群读
        public bool IsGroupRead;
        #endregion

        #endregion

        #region 构造函数
        public Module()
        {
            rsetting = new ReaderSetting();
            listAp = new List<AntennaPort>();
            IsNeedInventory = true;
            IsGroupRead = false;
        }
        #endregion

        #region 事件定义
        public virtual void OnTagReport(TagArgs arg)
        {
            if (ReadTag != null)
            {
                ReadTag.BeginInvoke(this, arg, null, null);
            }
        }
        #endregion

        #region 读写器事件
        /// <summary>
        /// 停止读取
        /// </summary>
        public void StopInventory()
        {
            if (jwReader != null)
            {
                IsRead = false;
                result = jwReader.RFID_Stop_Inventory();//停止当前UHF操作
                Logger.WriteLog(string.Format("jwReader.RFID_Stop_Inventory():{0}", result));
            }

            if (startThread != null)
            {
                startThread.Join(200);
                startThread.Abort();
                startThread = null;

                IsReadInterrupt = false;
            }
        }

        /// <summary>
        /// 断开连接
        /// </summary>
        public void CloseReader(bool isRestart = false)
        {
            if (!isRestart)
                ConnectStatusListenEnd(); //Add20160112
            if (jwReader != null)
            {
                result = jwReader.RFID_Close();//关闭读写器
                Logger.WriteLog(string.Format("jwReader.RFID_Close():{0}", result));
            }
            if (this.startThread != null)
            {
                this.startThread.Join(200);
                this.startThread.Abort();
                this.startThread = null;
            }
            jwReader = null;
        }

        /// <summary>
        /// 开启读取线程
        /// </summary>
        public void StartInventory()
        {
            if (startThread == null)
            {
                startThread = new Thread(new ThreadStart(Start));
                startThread.Start();
                startThread.IsBackground = true;
            }
        }


        #region Add20160112
        public void ReStartInventory()
        {
            if (jwReader != null)
            {
                IsRead = false;
                result = jwReader.RFID_Stop_Inventory();//停止当前UHF操作
                Logger.WriteLog(string.Format("jwReader.RFID_Stop_Inventory():{0}", result));
            }

            if (startThread != null)
            {
                startThread.Join(200);
                startThread.Abort();
                startThread = null;
            }

            if (startThread == null)
            {
                startThread = new Thread(new ThreadStart(Start));
                startThread.Start();
                startThread.IsBackground = true;
            }
        }
        #endregion

        /// <summary>
        /// 开始读取
        /// </summary>
        private void Start()
        {
            IsRead = true;
            //while (IsRead)
            {
                if (jwReader != null)
                {
                    IsReadInterrupt = true; //Add20160112
#if debug20160405
                    result = jwReader.RFID_Start_Inventory();
#else
                    if (!this.IsGroupRead)
                    {
                        result = jwReader.RFID_Start_Inventory();
                        Logger.WriteLog(string.Format("jwReader.RFID_Start_Inventory():{0}", result));
                    }
                    else
                    {

                        #region 群读TID_20160304
                        AccessParam accessParam = new AccessParam();
                        accessParam.Bank = MemoryBank.TID;
                        accessParam.OffSet = 0;
                        accessParam.Count = 12;
                        result = jwReader.RFID_GroupRead(accessParam);
                        Logger.WriteLog(string.Format("jwReader.RFID_GroupRead():{0}", result));
                        #endregion
                    }
#endif
                }
            }
        }

        #region Add20160112
        //连接结果事件参数
        ConnectStatueEventArgs ConnectResult_e = new ConnectStatueEventArgs();
        //连接结果输出事件
        public delegate void OperReadResultConnetEvent(object sender, ConnectStatueEventArgs e);
        public event OperReadResultConnetEvent ConnetStatueEvent;
        #endregion

        /// <summary>
        /// 替换字符'-'
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static String FormatHexStr(String str)
        {
            if (str == null)
                return "";
            else
                return str.Replace("-", "");
        }

        /// <summary>
        /// 组装存取参数
        /// </summary>
        /// <returns></returns>
        private AccessParam AssembleWriteAccessParam(string EPC)
        {
            int length = FormatHexStr(EPC).Length;

            if (length % 2 != 0)
            {
                return null;
            }
            AccessParam ap = new AccessParam();
            ap.Bank = MemoryBank.EPC;
            ap.Count = length;
            return ap;
        }

        /// <summary>
        /// 数据写入
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Write(string value)
        {
            try
            {
                setEPC(value);
                String writeValue = FormatHexStr(value);
                if (writeValue.Trim().Equals("") || writeValue.Length % 4 != 0)
                {
                    return false;
                }

                AccessParam ap = AssembleWriteAccessParam(value);

                if (ap == null)
                    return false;
                DateTime starttime = DateTime.Now;

                int writeCount = 0;

                bool writeSuccess = false;

                while (writeCount < Constants.WRITE_COUNT)
                {
                    Result result = Result.OK;
                    result = jwReader.RFID_Write(ap, writeValue);

                    writeCount++;

                    if (result != Result.OK)//写入失败
                        continue;
                    else
                    {
                        writeSuccess = true;
                        break;
                    }
                }

                if (!writeSuccess)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 设置当前EPC值
        /// </summary>
        /// <param name="epc"></param>
        public void setEPC(String epc)
        {
            RfidCriteria rc = new RfidCriteria();
            rc.Bank = MemoryBank.EPC;
            rc.OffSet = 32;//以bit为单位,前面32bit为epc长度等信息
            rc.Mask = ToByteByHexStr(epc);
            rc.Count = epc.Length * 4;
            result = jwReader.RFID_Set_Criteria(rc);
        }

        /*******************************************************************
     * * 函数名称：ToHexByte
     * * 功    能：获取16进制字符串的字节数组
     * * 参    数：hexString 16进制字符串
     * * 返 回 值：
     * 
     * *******************************************************************/
        public static byte[] ToByteByHexStr(string hexString)
        {
            if (hexString == null)
                return null;

            hexString = hexString.Replace(" ", "");
            if ((hexString.Length % 2) != 0)
                hexString += " ";
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }

        /// <summary>
        /// 数据上报
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void TagsReport(object sender, TagsEventArgs args)
        {
            TagArgs tag = new TagArgs()
            {
                Epc = args.tag.EPC,
                Rssi = args.tag.RSSI,
                PortnNmber = args.tag.PORT,
                Tid = args.tag.DATA,
                Ip = this.rsetting.ReaderIP
            };
            OnTagReport(tag);
        }



        #region Add20160104 初始化配置天线及功率
        /// <summary>
        /// 初始化配置天线及功率
        /// </summary>
        public void ConfigAntennaPort(List<AntennaPorts> listConfig)
        {
            for (int i = 0; i < listConfig.Count; i++)
            {
                listAp.Add(new AntennaPort { AntennaIndex = listConfig[i].AntennaIndex, Power = listConfig[i].Power });
            }
        }
        #endregion

        #region Add20160107 获取/设置 设备天线及功率
        public List<AntennaPorts> GetDevAntennaPortList()
        {
            List<AntennaPorts> apList = null;
            RfidSetting rsTemp = null;
            result = jwReader.RFID_Get_Config(out rsTemp);
            Logger.WriteLog(string.Format("jwReader.RFID_Get_Config():{0}", result));
            if (rsTemp != null)
            {
                apList = new List<AntennaPorts>();// = rsTemp.AntennaPort_List;
                for (int i = 0; i < rsTemp.AntennaPort_List.Count; i++)
                {
                    apList.Add(new AntennaPorts
                    {
                        AntennaIndex = rsTemp.AntennaPort_List[i].AntennaIndex,
                        Exist = rsTemp.AntennaPort_List[i].Exist,
                        Power = rsTemp.AntennaPort_List[i].Power
                    });
                }
            }
            return apList;
        }

        public bool SetDevAntennaPortList(List<AntennaPorts> listNewAP)
        {
            List<AntennaPort> listToDevice = null;
            if (listNewAP == null)
                return false;
            listToDevice = new List<AntennaPort>();// = listNewAP;
            for (int i = 0; i < listNewAP.Count; i++)
            {
                listToDevice.Add(new AntennaPort { AntennaIndex = listNewAP[i].AntennaIndex, Power = listNewAP[i].Power });
            }
            result = jwReader.RFID_Set_Antenna(listToDevice);
            Logger.WriteLog(string.Format("jwReader.RFID_Set_Antenna():{0}", result));
            if (result != Result.OK)
                return false;
            return true;
        }
        #endregion

        #region Add20160111 获取设置GPIO
        //20160111 GPIO
        public GPIOConfigs GetDevGPIOConfig()
        {
            GPIOConfigs devio = null;
            RfidSetting rsTemp = null;
            result = jwReader.RFID_Get_Config(out rsTemp);
            Logger.WriteLog(string.Format("jwReader.RFID_Get_Config():{0}", result));
            if (rsTemp != null)
            {
                devio = new GPIOConfigs();
                devio.GPI0_VALUE = (GPIValue)rsTemp.GPIO_Config.GPI0_VALUE;
                devio.GPI1_VALUE = (GPIValue)rsTemp.GPIO_Config.GPI1_VALUE;
                devio.GPO0_VALUE = (GPOValue)rsTemp.GPIO_Config.GPO0_VALUE;
                devio.GPO1_VALUE = (GPOValue)rsTemp.GPIO_Config.GPO1_VALUE;
            }
            return devio;
        }

        public bool SetDevGPOConfig(GPIOConfigs gpiosets)
        {
            GPIOConfig setsToDevice = null;
            if (gpiosets == null)
                return false;
            setsToDevice = new GPIOConfig();
            //setsToDevice.GPI0_VALUE = (GPITriggerValue)gpiosets.GPI0_VALUE;
            //setsToDevice.GPI1_VALUE = (GPITriggerValue)gpiosets.GPI1_VALUE;
            setsToDevice.GPO0_VALUE = (GPOTriggerValue)gpiosets.GPO0_VALUE;
            setsToDevice.GPO1_VALUE = (GPOTriggerValue)gpiosets.GPO1_VALUE;
            result = jwReader.RFID_Set_GPO(setsToDevice);
            Logger.WriteLog(string.Format("jwReader.RFID_Set_GPO():{0}", result));
            if (result != Result.OK)
                return false;
            return true;
        }
        #endregion

        #region Add20160112 断线重连
        private void ConnectStatusListenStart()
        {
            if (this.ConnectStatusThread == null)
            {
                this.IsListen = true;
                this.ConnectStatusThread = new Thread(new ThreadStart(ConnectStatusListen));
                this.ConnectStatusThread.IsBackground = true;
                this.ConnectStatusThread.Start();
                //
                PostMessageOut(true, null, "监听线程启动");
            }
        }

        private void ConnectStatusListenEnd()
        {
            if (jwReader != null)
            {
                this.IsListen = false;
            }

            if (this.ConnectStatusThread != null)
            {
                this.ConnectStatusThread.Join(200);
                this.ConnectStatusThread.Abort();
                this.ConnectStatusThread = null;

                //
                PostMessageOut(false, null, "停止监听线程");
            }
        }

        private void ConnectStatusListen()
        {
            while (this.IsListen)
            {

                if (this.result != Result.OK || !this.jwReader.IsConnected)
                {
                    string printkey = ":" + result.ToString() + " jwReader.IsConnected:(" + jwReader.IsConnected.ToString() + ")";
                    PostMessageOut(false, null, printkey);

                    this.CloseReader(true);
                    PostMessageOut(false, this, "关闭以便重连");

                    if (this.ConnectRead(true))
                    {
                        PostMessageOut(true, this, "重新连接成功");


                        if (this.IsReadInterrupt || this.IsNeedInventory)
                        {
                            PostMessageOut(true, this, "继续盘点");

                            this.ReStartInventory();
                        }
                    }
                    else
                    {
                        PostMessageOut(false, null, "试图重新连接...");
                    }

                }
                Thread.Sleep(500);
            }
        }

        private void PostMessageOut(bool isConnect, Module Current, string KeyMessage)
        {
            KeyMessage += "_";
            KeyMessage = " " + KeyMessage;
            this.ConnectResult_e.isConnected = isConnect;
            this.ConnectResult_e.ThisModule = Current;
            this.ConnectResult_e.strdebug = DateTime.Now.ToString("yyyyMMdd-HH:mm:ss-fffffff") + " " + this.rsetting.ReaderIP + KeyMessage;
            if (this.ConnetStatueEvent != null)
            {
                this.ConnetStatueEvent(this, this.ConnectResult_e);
            }
        }
        #endregion

        #region Add20160121 设置ReaderSetting

        public void SetReaderSetting(string theSetIP)
        {
            this.rsetting.ReaderIP = theSetIP;
            this.rsetting.TimeOut = 3;
        }
        #endregion

        #region Add20160121 获取ReaderSetting-IpPortOrCOM

        public string GetReaderSettingIPorCOM()
        {
            return this.rsetting.ReaderIP;
        }
        #endregion

        #region Update20160112
        /// <summary>
        /// 连接读写器
        /// </summary>
        /// <returns></returns>
        public bool ConnectRead(bool isRestart = false)
        {
            try
            {
                #region 连接模块
                if (jwReader == null)
                {
                    int portIndex = this.rsetting.ReaderIP.IndexOf(":");
                    if (portIndex != -1)
                    {
                        string ip = this.rsetting.ReaderIP;
                        ip = ip.Substring(0, portIndex);
                        string port = this.rsetting.ReaderIP;
                        port = port.Substring(portIndex + 1, 4);
                        jwReader = new JWReader(ip, Convert.ToInt32(port), 1, 1, 3);
                    }
                    else
                    {
                        jwReader = new JWReader(this.rsetting.ReaderIP, 115200, this.rsetting.TimeOut, 0);
                    }
                }
                else
                {
                    PostMessageOut(false, null, "未能构造");
                }

                result = jwReader.RFID_Open();
                Logger.WriteLog(string.Format("jwReader.RFID_Open():{0}", result));

                if (!isRestart) ConnectStatusListenStart();

                if (result != Result.OK)
                {
                    PostMessageOut(false, null, "RFID_Open返回错误");
                    return false;
                }
                else
                {
                    PostMessageOut(true, this, "RFID_Open成功");
                }

                //ConnectStatusListenStart();

                #endregion



                jwReader.TagsReported += TagsReport;
                RfidSetting setting = new RfidSetting();
                jwReader.RFID_Set_Antenna_Hub(IsEnable);
                setting.AntennaPort_List = listAp;

                /*
                ap = new AntennaPort();
                ap.AntennaIndex = 1;
                ap.Power = 27;
                setting.AntennaPort_List.Add(ap);
                */

                setting.GPIO_Config = null;
                setting.Inventory_Mode = InventoryMode.Continue;
                setting.Inventory_Time = 0;
                setting.Region_List = RegionList.OPTIMAL;//.CCC;

                /*
                setting.RSSI_Filter = new RSSIFilter();
                setting.RSSI_Filter.Enable = true;
                setting.RSSI_Filter.RSSIValue = -70;
                */

                setting.Speed_Mode = SpeedMode.SPEED_FULL_POWER;
                setting.Tag_Group = new TagGroup();
                setting.Tag_Group.SearchMode = SearchMode.SINGLE_TARGET;//.DUAL_TARGET;//.SINGLE_TARGET;
                setting.Tag_Group.Session = Session.S1;//.S0;
                setting.Tag_Group.SessionTarget = SessionTarget.A;

                result = jwReader.RFID_Set_Config(setting);
                Logger.WriteLog(string.Format("jwReader.RFID_Set_Config():{0}", result));
                if (result != Result.OK) //设置读写器配置
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex.Message);
                return false;
            }
        }
        #endregion
        #endregion

        #region Get ProductInfo
        /// <summary>
        /// 获得产品信息
        /// </summary>
        /// <returns></returns>
        public ProductInfos GetProductInfo()
        {
            ProductInfo proInfo = null;
            result = jwReader.RFID_Get_Product_Info(out proInfo);
            Logger.WriteLog(string.Format("jwReader.RFID_Get_Product_Info():{0}", result));
            ProductInfos infos = null;
            if (proInfo != null)
            {
                infos = new ProductInfos();
                infos.ANTENNA_NUMBER = proInfo.ANTENNA_NUMBER.ToString();
                infos.CHIP_TYPE = proInfo.CHIP_TYPE.ToString();
                infos.COMPANY_NO = proInfo.COMPANY_NO;
                infos.MODEL_SEQUENCE_NUMBER = proInfo.MODEL_SEQUENCE_NUMBER;
                infos.MODEL_TYPE = proInfo.MODEL_TYPE;
                infos.MODEL_VERSION = proInfo.MODEL_VERSION.ToString();
                infos.PRODUCT_DATE = proInfo.PRODUCT_DATE;
                infos.PRODUCT_TYPE = proInfo.PRODUCT_TYPE.ToString();
                infos.ANTENNA_PORT_EXISTS_LIST = new List<AntennaPorts>();
                foreach (var obj in infos.ANTENNA_PORT_EXISTS_LIST)
                {
                    AntennaPorts ports = new AntennaPorts();
                    ports.AntennaIndex = obj.AntennaIndex;
                    ports.Exist = obj.Exist;
                    ports.Power = obj.Power;
                    infos.ANTENNA_PORT_EXISTS_LIST.Add(ports);
                }
            }

            return infos;
        }

        /// <summary>
        /// 调用方法!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        /// </summary>
        /// <returns></returns>
        public string GetSerialNumber()
        {
            string SerialNumber = string.Empty;
            ProductInfos info = GetProductInfo();
            if (info != null)
            {
                SerialNumber = info.CHIP_TYPE + info.MODEL_TYPE + info.PRODUCT_DATE + info.MODEL_SEQUENCE_NUMBER;
            }
            return SerialNumber;
        }
        #endregion

        public int IsEnable = 0;

        public void RFID_Set_Antenna_Hub(int _isEnable)
        {
            IsEnable=_isEnable;//启用Antenna Hub
        }
    }

    class Constants
    {
        public static String DEFAULT_EPC_READ_WORD_COUNT = "12";
        public static String DEFAULT_EPC_READ_OFFSET = "0";

        public static String DEFAULT_TID_READ_WORD_COUNT = "6";
        public static String DEFAULT_TID_READ_OFFSET = "0";

        public static String DEFAULT_USER_READ_WORD_COUNT = "6";
        public static String DEFAULT_USER_READ_OFFSET = "0";

        public static String DEFAULT_RESERVED_READ_WORD_COUNT = "4";
        public static String DEFAULT_RESERVED_READ_OFFSET = "0";

        public static String INIT_ACCESS_PASSWORD = "00000000";

        public static int READ_COUNT = 10;//读次数
        public static int WRITE_COUNT = 20;//写次数
    }

    #region Add20160112
    public class ConnectStatueEventArgs : System.EventArgs
    {
        public bool isConnected;
        public string strdebug;
        public Module ThisModule;
    }
    #endregion
}
