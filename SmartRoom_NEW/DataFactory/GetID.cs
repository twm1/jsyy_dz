﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFactory
{
    public class GetID
    {
        public static string GetIDByDate(string param)
        {
            string ID = string.Empty;
            string Prefix = param;
            if (string.IsNullOrEmpty(Prefix))
                Prefix = "IK";
            Random rdm = new Random(0000);
            int rdmValue = rdm.Next(9999);
            ID = Prefix + DateTime.Now.ToString("yyMMddHHmmssffff") + rdmValue;
            return ID;
        }

        public static string GetEPC(string param, int lenght)
        {
            string hex = ConvertTo16(param);
            string EPC = string.Empty;
            Random rdm = new Random(0000);
            int rdmValue = rdm.Next(9999);
            EPC = DateTime.Now.ToString("yyMMddHHmmssffff") + rdmValue + hex;
            EPC = EPC.Substring(0, lenght);
            return EPC;
        }

        public static string ConvertTo16(string param)
        {
            char[] values = param.ToCharArray();
            string hexOutPut = string.Empty;
            foreach (char letter in values)
            {
                // Get the integral value of the character.
                int value = Convert.ToInt32(letter);
                // Convert the decimal value to a hexadecimal value in string form.
                hexOutPut += String.Format("{0:X}", value);
            }
            return hexOutPut;
        }
    }
}
