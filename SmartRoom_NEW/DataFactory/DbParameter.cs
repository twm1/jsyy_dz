﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DataFactory
{
    public class DbParameter
    {
        private string _strSQL;

        public string StrSQL
        {
            get { return _strSQL; }
            set { _strSQL = value; }
        }

        private SqlParameter[] _param;

        public SqlParameter[] Param
        {
            get { return _param; }
            set { _param = value; }
        }
    }
}
