﻿using Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using System.Linq;
using System.Threading.Tasks;


namespace DataFactory
{
    public class DataFactory
    {
        //public List<ProductInfo> GetProduct()
        //{
        //    List<ProductInfo> list = null;
        //    try
        //    {
        //        string strSQL = string.Format(@"SELECT top 100 ID,CPID,产品编号,产品名称,规格,型号,供应商,
        //                                                B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,
        //                                                d1,d2,d3,d4,d5,d6,d7,d8,d9,d10
        //                                                FROM H_产品 cp ");

        //        MySqlDataReader read = DbHelperMySQL.ExecuteReader(strSQL, null);
        //        list = new List<ProductInfo>();
        //        while (read.Read())
        //        {
        //            ProductInfo prod = new ProductInfo();
        //            prod.ID = read.IsDBNull(0) ? "" : read.GetGuid(0).ToString();
        //            prod.CPID = read.IsDBNull(1) ? "" : read.GetGuid(1).ToString();
        //            prod.ProductNo = read.IsDBNull(2) ? "" : read.GetString(2);
        //            prod.ProductName = read.IsDBNull(3) ? "" : read.GetString(3);
        //            prod.ProductSpec = read.IsDBNull(4) ? "" : read.GetString(4);
        //            prod.ProductType = read.IsDBNull(5) ? "" : read.GetString(5);
        //            prod.Supplier = read.IsDBNull(6) ? "" : read.GetString(6);
        //            list.Add(prod);
        //        }

        //        if (!read.IsClosed)
        //            read.Close();

        //    }
        //    catch (Exception ex)
        //    {
        //        list = null;
        //    }
        //    return list;
        //}

        /// <summary>
        /// 用用户名和密码登录
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="PassWord"></param>
        /// <returns></returns>
        public UserInfo     GetUsersByNamePass(string UserName, string PassWord)
        {
            UserInfo user = null;
            try
            {
                string strSQL = string.Format(@"select a.id,a.user_id,c.user_code,c.user_code,a.psw,b.ename,a.status,a.stoc_id from cabinet_user_info a,sys_user_org b,sys_user c where a.user_id=b.user_id and a.user_id=c.id and c.user_code='{0}' and a.psw='{1}' and a.status=1", "0014"+UserName, PassWord);

                MySqlDataReader read = DbHelperMySQL.ExecuteReader(strSQL);
                while (read.Read())
                {
                    user = new UserInfo();
                    user.ID = read.IsDBNull(0) ? "" : read.GetGuid(0).ToString();
                    user.UserID = read.IsDBNull(1) ? "" : read.GetString(1);
                    user.UserName = read.IsDBNull(2) ? "" : read.GetString(2);
                    user.GH = read.IsDBNull(3) ? "" : read.GetString(3);
                    user.Pwd = read.IsDBNull(4) ? "" : read.GetString(4);
                    user.RealName = read.IsDBNull(5) ? "" : read.GetString(5);
                    user.State = read.IsDBNull(6) ? "0" : read.GetInt32(6).ToString();
                    user.DepID = read.IsDBNull(7) ? "" : read.GetString(7);
                }

                if (!read.IsClosed)
                    read.Close();

                if (user != null)
                    user.UserKFList = GetKFListByUserID(user.UserID);
            }
            catch (Exception ex)
            {
                user = null;
            }
            return user;
        }

        //账号密码登陆功能做好之前临时替代
        //public UserInfo GetUsersTemp()
        //{
        //    UserInfo user = null;
        //    try
        //    {

        //        string strSQL = string.Format(@"select id,user_id,id,id,null,ename,status,org_id from sys_user_org where status=1 and user_id='0000'");

        //        MySqlDataReader read = DbHelperMySQL.ExecuteReader(strSQL, null);
        //        while (read.Read())
        //        {
        //            user = new UserInfo();
        //            user.ID = read.IsDBNull(0) ? "" : read.GetString(0);
        //            user.UserID = read.IsDBNull(1) ? "" : read.GetString(1);
        //            user.UserName = read.IsDBNull(2) ? "" : read.GetString(2);
        //            user.GH = read.IsDBNull(3) ? "" : read.GetString(3);
        //            user.Pwd = read.IsDBNull(4) ? "" : read.GetString(4);
        //            user.RealName = read.IsDBNull(5) ? "" : read.GetString(5);
        //            user.State = read.IsDBNull(6) ? "0" : read.GetInt32(6).ToString();
        //            user.DepID = read.IsDBNull(7) ? "" : read.GetString(7);
        //        }

        //        if (!read.IsClosed)
        //            read.Close();

        //        if (user != null)
        //            user.UserKFList = GetKFListByUserID(user.UserID);
        //    }
        //    catch (Exception ex)
        //    {
        //        user = null;
        //    }
        //    return user;
        //}

        /// <summary>
        /// 用关键字登录
        /// </summary>
        /// <param name="KeyWord"></param>
        /// <returns></returns>
        //public UserInfo GetUsersByKeyWord(string KeyWord)
        //{
        //    UserInfo user = null;
        //    try
        //    {
        //        string strSQL = string.Format(@"SELECT us.[ID]
        //                                               ,us.[UserID]
        //                                               ,[UserName]
        //                                               ,[GH]
        //                                               ,[pwd]
        //                                               ,[realname]
        //                                               ,[state]
        //                                               ,[DepID]
        //                                               FROM [Sun_User] us where us.KeyWord='{0}' and  us.state=1", KeyWord);

        //        MySqlDataReader read = DbHelperMySQL.ExecuteReader(strSQL, null);
        //        while (read.Read())
        //        {
        //            user = new UserInfo();
        //            user.ID = read.IsDBNull(0) ? "" : read.GetGuid(0).ToString();
        //            user.UserID = read.IsDBNull(1) ? "" : read.GetGuid(1).ToString();
        //            user.UserName = read.IsDBNull(2) ? "" : read.GetString(2);
        //            user.GH = read.IsDBNull(3) ? "" : read.GetString(3);
        //            user.Pwd = read.IsDBNull(4) ? "" : read.GetString(4);
        //            user.RealName = read.IsDBNull(5) ? "" : read.GetString(5);
        //            user.State = read.IsDBNull(6) ? "0" : read.GetInt32(6).ToString();
        //            user.DepID = read.IsDBNull(7) ? "" : read.GetGuid(7).ToString();
        //        }

        //        if (!read.IsClosed)
        //            read.Close();

        //        if (user != null)
        //            user.UserKFList = GetKFListByUserID(user.UserID, user.DepID);
        //    }
        //    catch (Exception ex)
        //    {
        //        user = null;
        //    }
        //    return user;
        //}
        /// <summary>
        /// 获取人脸信息
        /// </summary>
        /// <returns></returns>
        /// DONE
        public List<UserInfo> GetUsersByface()
        //public List<UserInfo> GetUsersByface(string KFID)
        {
            List<UserInfo> faceuser = new List<UserInfo>();
            try
            {
                string strSQL = string.Format(@"select b.id,b.user_id,a.id,a.id,null,ename,b.status,org_id,face_info  from sys_user_org a,cabinet_user_info b where a.user_id=b.user_id and b.status=1");
                //string strSQL = string.Format(@"select u.[ID],u.[UserID] ,u.[UserName],u.[GH],u.[pwd],u.[realname],u.[state],u.[DepID],u.[facialInformation] from  sun_user u inner join H_用户库房 h on u.userid = h.userid where (u.state = 1 and h.kfid = '" + KFID + "') or u.username like 'xj%'");
                MySqlDataReader read = DbHelperMySQL.ExecuteReader(strSQL, null);
                while (read.Read())
                {
                    UserInfo user = new UserInfo();
                    user.ID = read.IsDBNull(0) ? "" : read.GetString(0);
                    user.UserID = read.IsDBNull(1) ? "" : read.GetString(1);
                    user.UserName = read.IsDBNull(2) ? "" : read.GetString(2);
                    user.GH = read.IsDBNull(3) ? "" : read.GetString(3);
                    user.Pwd = read.IsDBNull(4) ? "" : read.GetString(4);
                    user.RealName = read.IsDBNull(5) ? "" : read.GetString(5);
                    user.State = read.IsDBNull(6) ? "0" : read.GetInt32(6).ToString();
                    user.DepID = read.IsDBNull(7) ? "" : read.GetString(7);
                    user.face = read.IsDBNull(8) ? null : (byte[])read[8];// ObjectToBytes(read.GetValue(8));
                    faceuser.Add(user);
                }

                if (!read.IsClosed)
                    read.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("查询报错");
                faceuser = null;
                throw ex;
            }
            return faceuser;
        }
        //DONE
        public UserInfo GetUsersByUserID(string UserID)
        {
            UserInfo user = null;
            try
            {

                string strSQL = string.Format(@"select id,user_id,id,id,null,ename,status,org_id from sys_user_org where status=1 and user_id='{0}'", UserID);
                //string strSQL = string.Format(@"SELECT us.[ID]
                //                                       ,us.[UserID]
                //                                       ,[UserName]
                //                                       ,[GH]
                //                                       ,[pwd]
                //                                       ,[realname]
                //                                       ,[state]
                //                                       ,[DepID]
                //                                       FROM [Sun_User] us  where   us.state=1 and us.userid='{0}'", UserID);

                MySqlDataReader read = DbHelperMySQL.ExecuteReader(strSQL, null);
                while (read.Read())
                {
                    user = new UserInfo();
                    user.ID = read.IsDBNull(0) ? "" : read.GetString(0);
                    user.UserID = read.IsDBNull(1) ? "" : read.GetString(1);
                    user.UserName = read.IsDBNull(2) ? "" : read.GetString(2);
                    user.GH = read.IsDBNull(3) ? "" : read.GetString(3);
                    user.Pwd = read.IsDBNull(4) ? "" : read.GetString(4);
                    user.RealName = read.IsDBNull(5) ? "" : read.GetString(5);
                    user.State = read.IsDBNull(6) ? "0" : read.GetInt32(6).ToString();
                    user.DepID = read.IsDBNull(7) ? "" : read.GetString(7);
                }

                if (!read.IsClosed)
                    read.Close();

                if (user != null)
                    user.UserKFList = GetKFListByUserID(user.UserID);
            }
            catch (Exception ex)
            {
                user = null;
            }
            return user;
        }
        //DONE
        public List<UserFingerPrint> GetUserByFingerPrint(string KFID)
        {
            List<UserFingerPrint> list = new List<UserFingerPrint>();
            try
            {
                // string strSQL = @"select a.user_id,Fingerprint from cabinet_userfp_info a,sys_user_org b where a.status=1 and a.user_id=b.user_id and b.status=1 and (org_id='" + KFID + "' or a.user_id='JSYY-SPD:user-00688')";
                string strSQL = @"select a.user_id,Fingerprint from cabinet_userfp_info a,sys_user_org b where a.status=1 and a.user_id=b.user_id and b.status=1 and b.org_id = '" + KFID + "'";

                MySqlDataReader read = DbHelperMySQL.ExecuteReader(strSQL, null);

                while (read.Read())
                {
                    UserFingerPrint obj = new UserFingerPrint();
                    obj.UserID = read.IsDBNull(0) ? "" : read.GetString(0);
                    obj.FingerPrint = read.IsDBNull(1) ? null : (byte[])read[1];
                    list.Add(obj);
                }
                if (!read.IsClosed)
                    read.Close();
            }
            catch (Exception ex)
            {
                list = null;
            }
            return list;
        }

        /// <summary>
        /// 获取编号 不用了
        /// </summary>
        /// <param name="UserDepID"></param>
        /// <param name="lb"></param>
        /// <returns></returns>
        //public string GetBH(string UserDepID, string lb)
        //{
        //    string BH = "";
        //    string strsql = "exec H_GetBH '" + UserDepID + "','" + lb + "'";
        //    BH = Convert.ToString(DbHelperMySQL.GetSingle(strsql));
        //    return BH;
        //}

        /// <summary>
        /// 注册一个新设备
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        /// DONE
        public bool InsertForceInfo(ForcerInfo obj)
        {
            //todo 修改要写死的hos_id和user_id
            string sql = "INSERT INTO `spd`.`cabinet_base_info`(`id`, `hos_id`, `stoc_id`, `cabinet_no`, `cabinet_name`, `register_datetime`, `link_man`, `link_man_phone`, `remark`, `user_id`, `mac_address`, `is_disable`, `last_update_datetime`, `version`) VALUES (uuid(), '"+ obj.HosID +"', '"+ obj.KFID + "', '"+ obj.ForcerNo + "', '"+ obj.ForcerName + "', now(), NULL, NULL, '客户端自动注册', '0000','"+ obj.MacAddress + "', 0, now(), 1)";
            bool vResult = DbHelperMySQL.ExecuteSql(sql) > 0 ? true : false;
            return vResult;
        }

        /// <summary>
        /// 验证设备是否已经注册
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        /// DONE
        public ForcerInfo CheckForcerIsRegiser(string macAddress)
        {
            string sql = "select a.id,cabinet_no,cabinet_name,a.remark,is_disable,register_datetime,user_id,mac_address,stoc_id,ename,a.hos_id from cabinet_base_info a,sys_org b where a.stoc_id = b.id and mac_address = '" + macAddress + "'";
            ForcerInfo obj = null;

            MySqlDataReader read = DbHelperMySQL.ExecuteReader(sql, null);
            while (read.Read())
            {
                obj = new ForcerInfo();
                obj.ID = read.IsDBNull(0) ? "" : read.GetGuid(0).ToString();
                obj.ForcerNo = read.IsDBNull(1) ? "" : read.GetString(1);
                obj.ForcerName = read.IsDBNull(2) ? "" : read.GetString(2);
                obj.Remark = read.IsDBNull(3) ? "" : read.GetString(3);
                obj.IsDisable = read.IsDBNull(4) ? false : Convert.ToBoolean(read.GetInt32(4));
                obj.DateTime = read.IsDBNull(5) ? DateTime.MinValue : read.GetDateTime(5);
                obj.UserID = read.IsDBNull(6) ? "" : read.GetString(6);
                obj.MacAddress = read.IsDBNull(7) ? "" : read.GetString(7);
                obj.KFID = read.IsDBNull(8) ? "" : read.GetString(8);
                obj.KfName = read.IsDBNull(9) ? "" : read.GetString(9);
                obj.HosID = read.IsDBNull(10) ? "" : read.GetString(10);
                break;
            }

            if (!read.IsClosed)
                read.Close();
            return obj;
        }

        /// <summary>
        /// 获得所有库房信息
        /// </summary>
        /// <returns></returns>
        /// DONE
        public List<KFInfo> GetKFList()
        {
            string sql = "select id,ename from sys_org where corp_id = 'h0014'";
            
            MySqlDataReader read = DbHelperMySQL.ExecuteReader(sql, null);
            List<KFInfo> list = new List<KFInfo>();

            while (read.Read())
            {
                KFInfo kf = new KFInfo();
                kf.KFID = read.IsDBNull(0) ? "" : read.GetString(0);
                kf.KFMC = read.IsDBNull(1) ? "" : read.GetString(1);
                list.Add(kf);
            }
            if (!read.IsClosed)
                read.Close();
            return list;
        }

        /// <summary>
        /// 获得所有主库
        /// </summary>
        /// <returns></returns>
        //public KFInfo GetZKKF(string DepID)
        //{
        //    string sql = "select kfid,库房名称 from H_库房 where 库房类型='主库' and state=1 and depid = '" + DepID + "'";
        //    MySqlDataReader read = DbHelperMySQL.ExecuteReader(sql, null);
        //    KFInfo kf = new KFInfo();

        //    while (read.Read())
        //    {

        //        kf.KFID = read.IsDBNull(0) ? "" : read.GetGuid(0).ToString();
        //        kf.KFMC = read.IsDBNull(1) ? "" : read.GetString(1);
        //    }
        //    if (!read.IsClosed)
        //        read.Close();
        //    return kf;
        //}



        /// <summary>
        /// 获得用户下的仓库
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="UserDepID"></param>
        /// <returns></returns>
        /// DONE
        public List<KFInfo> GetKFListByUserID(string UserID)
        {
            string sql = "select stoc_id,ename from cabinet_user_info a,sys_org b where a.stoc_id=b.id and a.user_id='" + UserID + "' ORDER BY ename";
            MySqlDataReader read = DbHelperMySQL.ExecuteReader(sql, null);
            List<KFInfo> list = new List<KFInfo>();

            while (read.Read())
            {
                KFInfo kf = new KFInfo();
                kf.KFID = read.IsDBNull(0) ? "" : read.GetString(0);
                kf.KFMC = read.IsDBNull(1) ? "" : read.GetString(1);
                list.Add(kf);
            }

            if (!read.IsClosed)
                read.Close();
            return list;
        }

        /// <summary>
        /// 获得库房下的所有用户信息
        /// </summary>
        /// <param name="KFID"></param>
        /// <returns></returns>
        //public List<UserInfo> GetUserListByKFID(string KFID)
        //{
        //    List<UserInfo> list = new List<UserInfo>();
        //    string sql = "SELECT A.UserID, userName FROM sun_User A inner join H_用户库房 B ON A.userid=B.userid where kfid='" + KFID + "'";
        //    MySqlDataReader read = DbHelperMySQL.ExecuteReader(sql, null);
        //    while (read.Read())
        //    {
        //        UserInfo kf = new UserInfo();
        //        kf.UserID = read.IsDBNull(0) ? "" : read.GetGuid(0).ToString();
        //        kf.UserName = read.IsDBNull(1) ? "" : read.GetString(1);
        //        list.Add(kf);
        //    }

        //    if (!read.IsClosed)
        //        read.Close();
        //    return list;
        //}

        /// <summary>
        /// 写入一条出库数据
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public string InsertCKInfo(InBillInfo obj)
        {
            string goodsIds = "";
            string qtys = "";
            //加goodsid
            foreach (var detail in obj.DetailList)
            {
                if (!detail.XZ)
                    continue;
                if (detail.SL == 0)
                    continue;
                goodsIds = goodsIds + detail.CPID + ",";
            }
            goodsIds = goodsIds.Substring(0,goodsIds.Length-1) + "";
            //加数量
            foreach (var detail in obj.DetailList)
            {
                if (!detail.XZ)
                    continue;
                if (detail.SL == 0)
                    continue;
                qtys = qtys + detail.SL + ",";
            }
            qtys = qtys.Substring(0,qtys.Length - 1) +  "";
            MySqlParameter[] parameters = {
                    new MySqlParameter("@hosId", MySqlDbType.VarChar, 32),
                    new MySqlParameter("@outDeptId", MySqlDbType.VarChar, 32),
                    new MySqlParameter("@userId", MySqlDbType.VarChar, 32),
                    new MySqlParameter("@outBillId", MySqlDbType.VarChar, 32),
                    new MySqlParameter("@goodsIds", MySqlDbType.VarChar, 20000),
                    new MySqlParameter("@qtys", MySqlDbType.VarChar, 10000),
                    new MySqlParameter("@p_out", MySqlDbType.Int32)
                    };
            parameters[0].Value = obj.HosID;
            parameters[0].Direction = ParameterDirection.Input;
            parameters[1].Value = obj.KFID;
            parameters[1].Direction = ParameterDirection.Input;
            parameters[2].Value = obj.UserID;
            parameters[2].Direction = ParameterDirection.Input;
            parameters[3].Value = GetDH(obj.DHLX);
            parameters[3].Direction = ParameterDirection.Input;
            parameters[4].Value = goodsIds;
            parameters[4].Direction = ParameterDirection.Input;
            parameters[5].Value = qtys;
            parameters[5].Direction = ParameterDirection.Input;
            parameters[6].Direction = ParameterDirection.Output;
            return DbHelperMySQL.RunProcedure("p_createOutStockBill_lyjsyy", parameters);
        }

        /// <summary>
        /// 写入一条耗材还回数据
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public string InsertRKInfo(InBillInfo obj)
        {
            string goodsIds = "";
            string qtys = "";
            //加goodsid
            foreach (var detail in obj.DetailList)
            {
                if (!detail.XZ)
                    continue;
                if (detail.SL == 0)
                    continue;
                goodsIds = goodsIds + detail.CPID + ",";
            }
            goodsIds = goodsIds.Substring(0, goodsIds.Length - 1) + "";
            //加数量
            foreach (var detail in obj.DetailList)
            {
                if (!detail.XZ)
                    continue;
                if (detail.SL == 0)
                    continue;
                qtys = qtys + detail.SL + ",";
            }
            qtys = qtys.Substring(0, qtys.Length - 1) + "";
            MySqlParameter[] parameters = {
                    new MySqlParameter("@hosId", MySqlDbType.VarChar, 32),
                    new MySqlParameter("@inDeptId", MySqlDbType.VarChar, 32),
                    new MySqlParameter("@userId", MySqlDbType.VarChar, 32),
                    new MySqlParameter("@inBillId", MySqlDbType.VarChar, 32),
                    new MySqlParameter("@goodsIds", MySqlDbType.VarChar, 20000),
                    new MySqlParameter("@qtys", MySqlDbType.VarChar, 10000),
                    new MySqlParameter("@p_out", MySqlDbType.Int32)
                    };
            parameters[0].Value = obj.HosID;
            parameters[0].Direction = ParameterDirection.Input;
            parameters[1].Value = obj.KFID;
            parameters[1].Direction = ParameterDirection.Input;
            parameters[2].Value = obj.UserID;
            parameters[2].Direction = ParameterDirection.Input;
            parameters[3].Value = GetDH(obj.DHLX);
            parameters[3].Direction = ParameterDirection.Input;
            parameters[4].Value = goodsIds;
            parameters[4].Direction = ParameterDirection.Input;
            parameters[5].Value = qtys;
            parameters[5].Direction = ParameterDirection.Input;
            parameters[6].Direction = ParameterDirection.Output;
            return DbHelperMySQL.RunProcedure("p_createInStockBill_hhjsyy", parameters);
        }

        /// <summary>
        /// 获得库存信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public List<StorageInfo> GetStorage(StorageInfo obj)
        {
            List<StorageInfo> list = new List<StorageInfo>();
            string cpbh = "", cpmc = "", gg = "", xh = "", ph = "";
            if (!string.IsNullOrEmpty(obj.CPBH))
                cpbh += " and b.hit_code like '%" + obj.CPBH + "' ";
            //cpbh += " and 产品编号 like '%" + obj.CPBH + "' ";
            if (!string.IsNullOrEmpty(obj.CPName))
                cpmc += " and goods_name like '%" + obj.CPName + "%' ";
            if (!string.IsNullOrEmpty(obj.GG))
                gg += " and b.goods_gg like '%" + obj.GG + "%' ";
            if (!string.IsNullOrEmpty(obj.XH))
                xh += "";
                //xh += " and 型号 like '%" + obj.XH + "%' ";
            string sql = "select '' as RKMXID,a.hos_goods_id,b.hit_code,goods_name,'产品组别',b.goods_gg,field_code2,b.unit,a.qty,'' as pihao,'' as xiaoqi,mfrs_name,'货权类型',b.price,'' as ID from v_stockpile_hz_dept a,hos_goods_info b where dept_id='" + obj.KFID + "' and a.hos_goods_id=b.id " + cpmc + cpbh + gg + xh+ " order by goods_name";

            //string sql = "select  '' as RKMXID,A.CPID,产品编号,产品名称,产品组别,规格,型号,单位,sum(B.数量) as 数量,'' as 批号,'' as 有效期,a.供应商,a.货权类型,a.出货单价,'' as ID from H_产品 a INNER JOIN H_库存 B on A.CPID=B.CPID where B.KFID='" + obj.KFID + "' AND   数量>0  and B.state=1 " + cpmc + cpbh + gg + xh + ph + "  group by A.CPID,产品编号,产品名称,产品组别,规格,型号,单位,a.供应商,a.货权类型,a.出货单价";
            //string sql2 = "select '' as RKMXID,a.hos_goods_id,'编号',goods_name,'产品组别',b.goods_gg,field_code2,b.unit,a.qty,'' as pihao,'' as xiaoqi,mfrs_name,'货权类型',b.price,'' from v_stockpile_hz_dept a,hos_goods_info b where dept_id='" + obj.KFID + "' and a.hos_goods_id=b.id";
            MySqlDataReader reader = DbHelperMySQL.ExecuteReader(sql, null);
            while (reader.Read())
            {
                StorageInfo st = new StorageInfo();
                st.RKMXID = reader.IsDBNull(0) ? "" : reader.GetString(0);
                st.CPID = reader.IsDBNull(1) ? "" : reader.GetString(1);
                st.CPBH = reader.IsDBNull(2) ? "" : reader.GetString(2);
                st.CPName = reader.IsDBNull(3) ? "" : reader.GetString(3);
                st.CPZB = reader.IsDBNull(4) ? "" : reader.GetString(4);
                st.GG = reader.IsDBNull(5) ? "" : reader.GetString(5);
                st.XH = reader.IsDBNull(6) ? "" : reader.GetString(6);
                st.DW = reader.IsDBNull(7) ? "" : reader.GetString(7);
                st.SL = reader.IsDBNull(8) ? 0 : reader.GetInt32(8);
                st.Batch = reader.IsDBNull(9) ? "" : reader.GetString(9);
                st.YXQ = "";// reader.IsDBNull(10) ? "" : reader.GetDateTime(10).ToString();
                st.GYS = reader.IsDBNull(11) ? "" : reader.GetString(11);
                st.HQLX = reader.IsDBNull(12) ? "" : reader.GetString(12);
                st.CKDJ = reader.IsDBNull(13) ? 0 : reader.GetDecimal(13);
                st.ID = reader.IsDBNull(14) ? "" : reader.GetString(14);
                list.Add(st);
            }

            if (!reader.IsClosed)
                reader.Close();
            return list;
        }

        /// <summary>
        /// 获得交接班信息
        /// </summary>
        /// <param name="KFID">正式库ID</param>
        /// <param name="KFID2">临时库ID</param>
        /// <param name="DT1">开始时间</param>
        /// <param name="DT2">结束时间</param>
        /// <returns></returns>
        public List<CYInfo> GetJJBInfo(string KFID, string DT1, string DT2)
        {
            MySqlParameter[] parameters = {
                    new MySqlParameter("@deptId", MySqlDbType.VarChar, 32),
                    new MySqlParameter("@stDate", MySqlDbType.VarChar, 32),
                    new MySqlParameter("@endDate", MySqlDbType.VarChar, 32)
                    };
            parameters[0].Value = KFID;
            parameters[0].Direction = ParameterDirection.Input;
            parameters[1].Value = DT1;
            parameters[1].Direction = ParameterDirection.Input;
            parameters[2].Value = DT2;
            parameters[2].Direction = ParameterDirection.Input;
            MySqlDataReader reader = DbHelperMySQL.RunProcedure_To_Reader("p_dzg_analyse", parameters);

            List<CYInfo> list = new List<CYInfo>();
            while (reader.Read())
            {
                CYInfo st = new CYInfo();
                st.XMDM = reader.IsDBNull(0) ? "" : reader.GetString(0);
                st.XMMC = reader.IsDBNull(1) ? "" : reader.GetString(1);
                st.JFSL = reader.IsDBNull(2) ? 0 : reader.GetInt32(2);
                st.LYSL = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
                st.GHSL = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
                st.CYSL = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
                st.KCSL = reader.IsDBNull(6) ? 0 : reader.GetInt32(6);
                st.AQKC = reader.IsDBNull(7) ? 0 : reader.GetInt32(7);
                st.GG = reader.IsDBNull(8) ? "" : reader.GetString(8);
                st.SHSL = 0;


                CYInfo obj = list.Find(T => T.XMDM == st.XMDM.Substring(0, st.XMDM.Length - 1));
                if (obj != null)
                {
                    obj.JFSL += st.JFSL;
                    obj.LYSL += st.LYSL;
                    obj.GHSL += st.GHSL;
                    obj.CYSL += st.CYSL;
                    obj.KCSL += st.KCSL;
                    obj.AQKC += st.AQKC;
                }
                else
                {
                    list.Add(st);
                }
            }
            if (!reader.IsClosed)
                reader.Close();

            return list;
        }

        /// <summary>
        /// 获得历史交接班记录的详情
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="SJKFID"></param>
        /// <param name="KFID"></param>
        /// <param name="BeginDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        public List<CYInfo> GetLSJJBInfo(string ID, string KFID, string BeginDate, string EndDate)
        {
            List<CYInfo> list = new List<CYInfo>();
            string sql = "select pid,item_code,item_cname,charge_qty,use_qty,return_qty,diff_qty,qty,safe_qty,goods_spec,loss_qty from handing_off_sub where pid='" + ID + "'";
            MySqlDataReader reader = DbHelperMySQL.ExecuteReader(sql, null);
            //DataTable dtable = DbHelperMySQL.ExecuteDataTable(sql, null);
            while (reader.Read())
            {
                CYInfo st = new CYInfo();

                st.XMDM = reader.IsDBNull(1) ? "" : reader.GetString(1);
                st.XMMC = reader.IsDBNull(2) ? "" : reader.GetString(2);
                st.JFSL = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
                st.LYSL = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
                st.GHSL = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
                st.CYSL = reader.IsDBNull(6) ? 0 : reader.GetInt32(6);
                st.KCSL = reader.IsDBNull(7) ? 0 : reader.GetInt32(7);
                st.AQKC = reader.IsDBNull(8) ? 0 : reader.GetInt32(8);
                st.GG = reader.IsDBNull(9) ? "" : reader.GetString(9);
                st.SHSL = reader.IsDBNull(10) ? 0 : reader.GetInt32(10);
                list.Add(st);

                //CYInfo obj = list.Find(T => T.XMDM == st.XMDM.Substring(0, st.XMDM.Length - 1));
                //if (obj != null)
                //{
                //    obj.JFSL += st.JFSL;
                //    obj.LYSL += st.LYSL;
                //    obj.GHSL += st.GHSL;
                //    obj.CYSL += st.CYSL;
                //    obj.KCSL += st.KCSL;
                //    obj.AQKC += st.AQKC;
                //}
                //else
                //{
                //    list.Add(st);
                //}
            }
            if (!reader.IsClosed)
                reader.Close();

            if (list.Count == 0)
            {
                list = GetJJBInfo(KFID, BeginDate, EndDate);
            }

            return list;
        }

        /// <summary>
        /// 获得历史交接班记录的列表
        /// </summary>
        /// <param name="KFID"></param>
        /// <returns></returns>
        public List<LSJJBInfo> GetLSJJB(string KFID)
        {
            List<LSJJBInfo> list = new List<LSJJBInfo>();
            /*string sql = @"select top 100 h.id,h.ckid,h.userid,u.realname,h.begindatetime,h.enddatetime,k.库房名称,h.remark from H_交接班记录 h 
                                inner join sun_user u on h.userid=u.userid 
                                inner join H_库房 k on h.ckid=k.kfid
                                order by enddatetime desc";*/
            string sql = @"select h.id,h.stoc_id,h.user_id,u.ename,h.begin_datetime,h.end_datetime,k.ename,h.remark from handing_off_main h inner join sys_user_org u on h.user_id=u.user_id inner join sys_org k on h.stoc_id=k.id where h.stoc_id='" + KFID + "' order by end_datetime desc  limit 100";
            MySqlDataReader reader = DbHelperMySQL.ExecuteReader(sql, null);
            //会抛异常 
            while (reader.Read())
            {
                LSJJBInfo st = new LSJJBInfo();
                st.ID = reader.IsDBNull(0) ? "" : reader.GetGuid(0).ToString();
                st.CKID = reader.IsDBNull(1) ? "" : reader.GetGuid(0).ToString();
                st.UserID = reader.IsDBNull(2) ? "" : reader.GetGuid(0).ToString();
                st.UserName = reader.IsDBNull(3) ? "" : reader.GetString(3);
                st.BeginDate = reader.IsDBNull(4) ? "" : reader.GetDateTime(4).ToString("yyyy-MM-dd HH:mm:ss");
                st.EndDate = reader.IsDBNull(5) ? "" : reader.GetDateTime(5).ToString("yyyy-MM-dd HH:mm:ss");
                st.CkName = reader.IsDBNull(6) ? "" : reader.GetString(6);
                st.Remark = reader.IsDBNull(7) ? "" : reader.GetString(7);
                list.Add(st);
            }

            if (!reader.IsClosed)
                reader.Close();
            return list;
        }

        /// <summary>
        /// 保存一条交接班信息
        /// </summary>
        /// <param name="CKID"></param>
        /// <param name="DT1"></param>
        /// <param name="DT2"></param>
        /// <param name="UserID"></param>
        /// <param name="DepID"></param>
        /// <param name="Remark"></param>
        /// <returns></returns>
        public bool SaveJJBInfo(string ID, string CKID, string DT1, string DT2, string UserID, string DepID, string Remark, List<CYInfo> list)
        {
            bool result = false;
            List<string> sqlList = new List<string>();
            string sql = "INSERT into handing_off_main (ID,stoc_id,begin_datetime,end_datetime,user_id,Remark,last_update_datetime,version)values('" + ID + "','" + CKID + "','" + DT1 + "','" + DT2 + "','" + UserID + "','" + Remark + "','"+ DateTime.Now.ToString() + "',1)";
            sqlList.Add(sql);
            foreach (var obj in list)
            {
                sql = "insert into handing_off_sub (id,pid,item_code,item_cname,charge_qty,use_qty,return_qty,diff_qty,qty,safe_qty,goods_spec,loss_qty,last_update_datetime,version)values(uuid(),'" + ID + "','" + obj.XMDM + "','" + obj.XMMC + "'," + obj.JFSL + "," + obj.LYSL + "," + obj.GHSL + "," + obj.CYSL + "," + obj.KCSL + "," + obj.AQKC + ",'" + obj.GG + "'," + obj.SHSL + ",'" + DateTime.Now.ToString() + "',1)";
                sqlList.Add(sql);
            }
            if (DbHelperMySQL.ExecuteSqlTran(sqlList) > 0)
                result = true;
            return result;
        }

        /// <summary>
        /// 获得科室上一次交接班时间
        /// </summary>
        /// <param name="CKID"></param>
        /// <returns></returns>
        public string GetLastJJBRQ(string CKID)
        {
            string result = "";
            string sql = "select end_datetime from handing_off_main  where stoc_id='" + CKID + "' order by end_datetime desc limit 1;";
            result = Convert.ToString(DbHelperMySQL.GetSingle(sql));
            return result;
        }


        /// <summary>
        /// 获得历史出库单列表
        /// </summary>
        /// <param name="KFID1">临时库库房ID</param>
        /// <param name="KFID2">正式库库房ID</param>
        /// <param name="BeginDate"></param>
        /// <param name="EndDate"></param>
        /// <param name="LX"></param>
        /// <returns></returns>
        public List<InBillInfo> GetBillInfo(string KFID, string BeginDate, string EndDate, string LX)
        {
            List<InBillInfo> list = new List<InBillInfo>();
            try
            {
                string SQL = "";
                if (LX.Equals("领用"))
                {
                    SQL = @"SELECT
                            a.id,
                            a.out_dept_id,
                            a.out_dept_name,
                            a.fill_date,
                            b.ename AS filler,
                            '领用' AS type 
                            FROM
                            (
                            SELECT
	                            * 
                            FROM
	                            out_stock 
                            WHERE
	                            out_stock_kind = 80 
	                            AND pur_mode = 10 
	                            AND STATUS = 30 
	                            AND out_dept_id = '" + KFID
                                + "' AND(fill_date BETWEEN '" + BeginDate
                                + "' AND '" + EndDate + "')) a INNER JOIN sys_user_org as b ON a.filler =" +
                                " b.user_id ";
                }
                else if (LX.Equals("归还"))
                {
                    SQL = @"SELECT
                    a.id,
                    a.in_dept_id,
                    a.in_dept_name,
                    a.fill_date,
                    b.ename AS filler,
                    '归还' AS type 
                    FROM
                    (
                    SELECT
	                    * 
                    FROM
	                    in_stock 
                    WHERE
		                    in_stock_kind = 10 
	                    AND pur_mode = 10 
		            AND STATUS = 40 
		            AND in_dept_id = '" + KFID + "' AND(fill_date BETWEEN '" + BeginDate + "' AND '" + EndDate + "') ) a INNER JOIN sys_user_org as b ON a.filler = b.user_id";
                }
                else if (LX.Equals("全部"))
                {
                    SQL += @"select a.id,a.out_dept_id  as dept_id,a.out_dept_name as dept_name,a.fill_date,b.ename as filler,'领用' as type from out_stock a,sys_user_org b where a.filler = b.user_id and a.out_stock_kind = 80 AND a.pur_mode = 10 AND a.status = 30 and (fill_date between  '" + BeginDate + "' and '" + EndDate + "') and out_dept_id='" + KFID + "' union all select a.id,a.in_dept_id  as dept_id,a.in_dept_name as dept_name,a.fill_date,b.ename as filler,'归还' as type from in_stock a,sys_user_org b where a.filler = b.user_id and a.in_stock_kind = 10 AND a.pur_mode = 10 AND a.status= 40  and (fill_date between  '" + BeginDate + "' and '" + EndDate + "') and in_dept_id='" + KFID + "' order by fill_date";
                }
                MySqlDataReader reader = DbHelperMySQL.ExecuteReader(SQL, null);

                while (reader.Read())
                {
                    InBillInfo billInfo = new InBillInfo();
                    billInfo.DJID = reader.IsDBNull(0) ? "" : reader.GetString(0);
                    billInfo.KFID = reader.IsDBNull(1) ? "" : reader.GetString(1);
                    billInfo.CKKFName = reader.IsDBNull(2) ? "" : reader.GetString(2);
                    billInfo.SJ = reader.IsDBNull(3) ? "" : reader.GetDateTime(3).ToString("yyyy-MM-dd HH:mm:ss");
                    billInfo.UserName = reader.IsDBNull(4) ? "" : reader.GetString(4);
                    billInfo.DJLX = reader.IsDBNull(5) ? "" : reader.GetString(5);
                    list.Add(billInfo);
                }
                if (!reader.IsClosed)
                    reader.Close();

            }
            catch (Exception ex)
            {
                list = null;
            }
            return list;
        }
        
        /// <summary>
        /// 获得出库单明细
        /// </summary>
        /// <param name="_CKID"></param>
        /// <returns></returns>
        public List<InBillDetailInfo> GetBillDetailInfo(string _CKID)
        {
            List<InBillDetailInfo> list = new List<InBillDetailInfo>();
            try
            {
                //gourp by pihao
                // string SQL = @"select erp_code,a.goods_id,a.goods_name,a.goods_gg,sum(a.qty) as qty from (select bill_id,goods_id,goods_name,goods_gg,out_qty as qty from out_stock_list union all select bill_id,goods_id,goods_name,goods_gg,in_qty as qty from in_stock_list) a,hos_goods_info b where a.goods_id = b.id and a.bill_id='" + _CKID + "' group by erp_code,a.goods_id,a.goods_name,a.goods_gg";
                string SQL = @"SELECT
                            erp_code,
                            a.goods_id,
                            a.goods_name,
                            a.goods_gg,
                            sum( a.qty ) AS qty 
                            FROM
                            (
                            SELECT
	                            bill_id,
	                            goods_id,
	                            goods_gg,
	                            out_qty AS qty,
		                            goods_name
                            FROM
	                            out_stock_list WHERE bill_id = '" + _CKID + "' UNION ALL SELECT bill_id,goods_id, goods_gg, in_qty AS qty ,goods_name  FROM in_stock_list  WHERE bill_id = '" + _CKID + "') a left join hos_goods_info b   on a.goods_id = b.id  GROUP BY erp_code,a.goods_id, a.goods_gg,a.goods_name";

                MySqlDataReader reader = DbHelperMySQL.ExecuteReader(SQL, null);

                while (reader.Read())
                {
                    InBillDetailInfo obj = new InBillDetailInfo();
                    obj.CPBH = reader.IsDBNull(0) ? "" : reader.GetString(0);
                    obj.CPMC = reader.IsDBNull(2) ? "" : reader.GetString(2);
                    obj.GG = reader.IsDBNull(3) ? "" : reader.GetString(3);
                    obj.SL = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
                    list.Add(obj);
                }

                if (!reader.IsClosed)
                    reader.Close();
            }
            catch (Exception ex)
            {
                list = null;
            }
            return list;
        }

        /// <summary>
        /// 根据收费代码获得病人信息
        /// </summary>
        /// <param name="KFID"></param>
        /// <param name="BeginDate"></param>
        /// <param name="EndDate"></param>
        /// <param name="XMDM"></param>
        /// <returns></returns>
        public List<SYXXInfo> GetBRSYXXByDate(string KFID, string BeginDate, string EndDate, string XMDM)
        {
            MySqlParameter[] parameters = {
                    new MySqlParameter("@deptId", MySqlDbType.VarChar, 32),
                    new MySqlParameter("@itemcode", MySqlDbType.VarChar, 32),
                    new MySqlParameter("@stDate", MySqlDbType.VarChar, 32),
                    new MySqlParameter("@endDate", MySqlDbType.VarChar, 32)
                    };
            parameters[0].Value = KFID;
            parameters[0].Direction = ParameterDirection.Input;
            parameters[1].Value = XMDM+"%";
            parameters[1].Direction = ParameterDirection.Input;
            parameters[2].Value = BeginDate;
            parameters[2].Direction = ParameterDirection.Input;
            parameters[3].Value = EndDate;
            parameters[3].Direction = ParameterDirection.Input;
            MySqlDataReader reader = DbHelperMySQL.RunProcedure_To_Reader("p_dzg_patient", parameters);
            List<SYXXInfo> list = new List<SYXXInfo>();
                while (reader.Read())
                {
                    SYXXInfo obj = new SYXXInfo();
                    obj.XM = reader.IsDBNull(0) ? "无" : reader.GetString(0);
                    obj.BRH = reader.IsDBNull(1) ? "无" : reader.GetString(1);
                    obj.CWH = reader.IsDBNull(2) ? "无" : reader.GetString(2);
                    obj.SL = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
                    list.Add(obj);
                }

            if (!reader.IsClosed)
                reader.Close();
            return list;
        }

        /// <summary>
        /// 入参 单据类型 ckdh/rkdh
        /// </summary>
        /// <param name="Djlx"></param>
        /// <returns></returns>
        public  string GetDH(string Djlx)
        {
            string apiaddress = "http://172.16.10.178:8089/hisDataService/idService/getBillId/"+Djlx;
            HttpWebRequest request = WebRequest.Create(apiaddress) as HttpWebRequest;
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                try
                {
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    var dJXX = JsonConvert.DeserializeObject<Djxx>(reader.ReadToEnd());
                    reader.Close();
                    response.Close();
                    return dJXX.Data;
                }
                catch (IOException e)
                {
                    //
                    return null;
                }
            }
        }
        /// <summary>
        /// 获得整单入库主表
        /// </summary>
        /// <param name="KFID"></param>
        /// <returns></returns>
        public List<InStockBill> GetInStockBill(string KFID)
        {
            List<InStockBill> list = new List<InStockBill>();
            try
            {
                string SQL = "";
             //   SQL = @"SELECT bill_id,in_dept_name,fill_date from out_stock where out_stock_kind = 40 and out_stock_type = 10 and `status` in (30,69) and in_dept_id='" + KFID + "'";
                SQL = @"SELECT  a.bill_id,a.in_dept_name,a.fill_date FROM out_stock a LEFT JOIN dept_buy_main b ON a.source_bill_id = b.bill_id WHERE out_stock_kind = 40 AND out_stock_type = 10 AND `status` IN ( 30, 69 ) AND b.remark = '低值耗材被动补货!' and in_dept_id = '" + KFID + "' ";

                MySqlDataReader reader = DbHelperMySQL.ExecuteReader(SQL, null);

                while (reader.Read())
                {
                    InStockBill billInfo = new InStockBill();
                    billInfo.Bill_ID = reader.IsDBNull(0) ? "" : reader.GetString(0);
                    billInfo.In_dept_name = reader.IsDBNull(1) ? "" : reader.GetString(1);
                    billInfo.Fill_date = reader.IsDBNull(2) ? "" : reader.GetDateTime(2).ToString("yyyy-MM-dd HH:mm:ss");
                    list.Add(billInfo);
                }
                if (!reader.IsClosed)
                    reader.Close();

            }
            catch (Exception ex)
            {
                list = null;
            }
            return list;
        }

        /// <summary>
        /// 获得整单入库明细表
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public List<InStockBillDetail> GetInStockBillDetail(string ID)
        {
            List<InStockBillDetail> list = new List<InStockBillDetail>();
          //  string sql = "SELECT b.erp_code,a.goods_name,a.goods_gg,a.mfrs_name,a.unit,sum(a.out_qty) from out_stock_list a,hos_goods_info b where a.goods_id=b.id and bill_id = '" + ID + "' group by b.erp_code,a.goods_name,a.goods_gg,a.mfrs_name,a.unit";

            string sql = "SELECT b.erp_code,a.goods_name,a.goods_gg,a.mfrs_name,a.unit,sum( a.out_qty ) FROM out_stock_list a left join out_stock c on a.bill_id = c.bill_id " +
               "left join dept_buy_main f on c.source_bill_id = f.bill_id,hos_goods_info b WHERE a.goods_id = b.id AND f.remark = '低值耗材被动补货!' " +
              "AND a.bill_id = '" + ID + "'   GROUP BY b.erp_code,a.goods_name,a.goods_gg,a.mfrs_name,	a.unit";
            MySqlDataReader reader = DbHelperMySQL.ExecuteReader(sql, null);
            //DataTable dtable = DbHelperMySQL.ExecuteDataTable(sql, null);
            while (reader.Read())
            {
                InStockBillDetail st = new InStockBillDetail();

                st.SFBM = reader.IsDBNull(0) ? "" : reader.GetString(0);
                st.CPMC = reader.IsDBNull(1) ? "" : reader.GetString(1);
                st.GG = reader.IsDBNull(2) ? "" : reader.GetString(2);
                st.SCCJ = reader.IsDBNull(3) ? "" : reader.GetString(3);
                st.DW = reader.IsDBNull(4) ? "" : reader.GetString(4);
                st.SL = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
                list.Add(st);

            }
            if (!reader.IsClosed)
                reader.Close();

            return list;
        }
        
        /// <summary>
        /// 整单入库
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public string InsertZDRKInfo(InStockBill obj)
        {
            MySqlParameter[] parameters = {
                    new MySqlParameter("@outBillId", MySqlDbType.VarChar, 36),
                    new MySqlParameter("@inBillId", MySqlDbType.VarChar, 36),
                    new MySqlParameter("@fillerId", MySqlDbType.VarChar, 36),
                    new MySqlParameter("@p_out", MySqlDbType.Int32)
                    };
            parameters[0].Value = obj.Bill_ID;
            parameters[0].Direction = ParameterDirection.Input;
            parameters[1].Value = GetDH("rkdh"); 
            parameters[1].Direction = ParameterDirection.Input;
            parameters[2].Value = obj.UserID;
            parameters[2].Direction = ParameterDirection.Input;
            parameters[3].Direction = ParameterDirection.Output;
            return DbHelperMySQL.RunProcedure("p_createInStockBill_jsyy", parameters);            
        }

        /// 修改密码
        public bool ChangePwd(string userid, string pwd)
        {
            string sql = "update cabinet_user_info set psw='" + pwd + "'  where user_id='" + userid + "';";
            bool result = DbHelperMySQL.ExecuteSql(sql) > 0 ? true : false;
            return result;
        }

        public List<HCInfo> getHCInfo(string id, string startDate, string enddate, string kFID)
        {
            string we = "";
            if (id != null)
            {
                we = " WHERE hi.hit_code = '" + id + "'";
            }
            List<HCInfo> list = new List<HCInfo>();
            string sql = "SELECT hi.goods_gg,out_qty AS qty,hi.goods_name,ifo.dept_name,ifo.fill_date,ifo.filler,ifo.type FROM out_stock_list ol " +
                "left JOIN hos_goods_info hi ON ol.goods_id = hi.id right JOIN( " +
                "SELECT a.id, a.out_dept_id AS dept_id,a.out_dept_name AS dept_name, a.fill_date, b.ename AS filler, '领用' AS type" +
                " FROM  out_stock a,sys_user_org b WHERE  a.filler = b.user_id AND a.out_stock_kind = 80  AND a.pur_mode = 10 AND a.STATUS = 30" +
                " AND(fill_date BETWEEN '" + startDate + "' AND '" + enddate + "')  AND out_dept_id = '" + kFID + "') ifo ON ifo.id = ol.bill_id" +
                we;
            MySqlDataReader reader = DbHelperMySQL.ExecuteReader(sql, null);
            while (reader.Read())
            {
                HCInfo st = new HCInfo();

                st.Gg = reader.IsDBNull(0) ? "" : reader.GetString(0);
                st.Count = reader.IsDBNull(1) ? "" : reader.GetString(1);
                st.Goods_name = reader.IsDBNull(2) ? "" : reader.GetString(2);
                st.WareHouse = reader.IsDBNull(3) ? "" : reader.GetString(3);
                st.Time = reader.IsDBNull(4) ? "" : reader.GetString(4);
                st.UserName = reader.IsDBNull(5) ? "" : reader.GetString(5);
                st.Type = reader.IsDBNull(6) ? "" : reader.GetString(6);
                list.Add(st);

            }
            if (!reader.IsClosed)
                reader.Close();
           
            sql = "SELECT hi.goods_gg,in_qty AS qty,hi.goods_name,ifo.dept_name,ifo.fill_date,ifo.filler,ifo.type " +
                "FROM in_stock_list ol LEFT JOIN hos_goods_info hi ON ol.goods_id = hi.id RIGHT JOIN(" +
                " SELECT  a.id, a.in_dept_id AS dept_id,  a.in_dept_name AS dept_name,a.fill_date, b.ename AS filler, '归还' AS type" +
                " FROM  in_stock a,sys_user_org b  WHERE a.filler = b.user_id  AND in_dept_id = '" + kFID + "'AND(fill_date BETWEEN '"
                + startDate + "' AND '" + enddate + "')) ifo ON ifo.id = ol.bill_id " +
                we;
            MySqlDataReader reader2 = DbHelperMySQL.ExecuteReader(sql, null);
            while (reader2.Read())
            {
                HCInfo st = new HCInfo();

                try
                {
                    st.Gg = reader2.IsDBNull(0) ? "" : reader2.GetString(0);
                    st.Count = reader2.IsDBNull(1) ? "" : reader2.GetString(1);
                    st.Goods_name = reader2.IsDBNull(2) ? "" : reader2.GetString(2);
                    st.WareHouse = reader2.IsDBNull(3) ? "" : reader2.GetString(3);
                    st.Time = reader2.IsDBNull(4) ? "" : reader2.GetString(4);
                    st.UserName = reader2.IsDBNull(5) ? "" : reader2.GetString(5);
                    st.Type = reader2.IsDBNull(6) ? "" : reader2.GetString(6);
                }
                catch (Exception e) { }
                list.Add(st);

            }
            if (!reader2.IsClosed)
                reader2.Close();
            return list;
        }
    }
}
