﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Business;

namespace SmartRoom
{
    /// <summary>
    /// InputNumberWindow.xaml 的交互逻辑
    /// </summary>
    public partial class InputNumberWindow : Window
    {
        BHBusiness _bhBusiness = new BHBusiness();
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            //if (txtName.Text.Equals("Gadmin") && txtpwd.Password.Equals("Jjj111"))
            //    this.Close();
            //else
            //{
            //    Global.MessageWindow.ShowDialog("非管理员用户,不能退出当前系统！", Enum.MessageButton.Close, Enum.MessageStyle.Warn);
            //}
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtpwd.Password))
            {
                Global.MessageWindow.ShowDialog("请填写密码！", Enum.MessageButton.Close, Enum.MessageStyle.Warn);
                txtpwd.Focus();
                return;
            }
            else
            {
                if (_bhBusiness.ChangePwd(Global.User.UserID, txtpwd.Password))
                {
                    Global.MessageWindow.ShowDialog("修改密码成功!");
                }
                else
                {
                    Global.MessageWindow.ShowDialog("修改密码失败，请联系管理员!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
                }
            }
        }

        public InputNumberWindow()
        {
            InitializeComponent();
        }

        

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (Global.User != null)
                txtName.Text = Global.User.RealName;
        }

    }
}
