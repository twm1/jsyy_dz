﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartRoom.Enum
{
    public enum MessageStyle
    {
        /// <summary>
        /// 问号
        /// </summary>
        Question,
        /// <summary>
        /// 感叹号
        /// </summary>
        Warn,
        /// <summary>
        /// 错误号
        /// </summary>
        Error,
        /// <summary>
        /// 对号
        /// </summary>
        Success
    }

    public enum MessageButton
    {
        /// <summary>
        /// 是否
        /// </summary>
        YesNo,
        /// <summary>
        /// 是否取消
        /// </summary>
        YesNoCancel,
        /// <summary>
        /// 取消
        /// </summary>
        Cancel,
        /// <summary>
        /// 关闭
        /// </summary>
        Close
    }

    public enum MessageDialogResult
    {
        /// <summary>
        /// 是
        /// </summary>
        Yes,
        /// <summary>
        /// 否
        /// </summary>
        No,
        /// <summary>
        /// 取消
        /// </summary>
        Cancel
    }
}
