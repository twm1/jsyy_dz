﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartRoom
{
    public enum Status
    {
        Success,
        Fail,
        Exception,
        Timeout,
        UnknowError,
        InvaliedSession
    }
}
