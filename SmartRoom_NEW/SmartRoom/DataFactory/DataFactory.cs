﻿using Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DataFactory
{
    public class DataFactory
    {
        public List<ProductInfo> GetProduct()
        {
            List<ProductInfo> list = null;
            try
            {
                string strSQL = string.Format(@"SELECT top 100 ID,CPID,产品编号,产品名称,规格,型号,供应商,
                                                        B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,
                                                        d1,d2,d3,d4,d5,d6,d7,d8,d9,d10
                                                        FROM H_产品 cp ");

                SqlDataReader read = SqlHelper.ExecuteReader(System.Data.CommandType.Text, strSQL, null);
                list = new List<ProductInfo>();
                while (read.Read())
                {
                    ProductInfo prod = new ProductInfo();
                    prod.ID = read.IsDBNull(0) ? "" : read.GetGuid(0).ToString();
                    prod.CPID = read.IsDBNull(1) ? "" : read.GetGuid(1).ToString();
                    prod.ProductNo = read.IsDBNull(2) ? "" : read.GetString(2);
                    prod.ProductName = read.IsDBNull(3) ? "" : read.GetString(3);
                    prod.ProductSpec = read.IsDBNull(4) ? "" : read.GetString(4);
                    prod.ProductType = read.IsDBNull(5) ? "" : read.GetString(5);
                    prod.Supplier = read.IsDBNull(6) ? "" : read.GetString(6);
                    list.Add(prod);
                }

                if (!read.IsClosed)
                    read.Close();

            }
            catch (Exception ex)
            {
                list = null;
            }
            return list;
        }

        /// <summary>
        /// 用用户名和密码登录
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="PassWord"></param>
        /// <returns></returns>
        public UserInfo GetUsersByNamePass(string UserName, string PassWord)
        {
            UserInfo user = null;
            try
            {
                string strSQL = string.Format(@"SELECT us.[ID]
                                                       ,us.[UserID]
                                                       ,[UserName]
                                                       ,[GH]
                                                       ,[pwd]
                                                       ,[realname]
                                                       ,[state]
                                                       ,[DepID]
                                                       FROM [Sun_User] us where us.UserName='{0}' and us.pwd='{1}' and us.state=1", UserName, PassWord);

                SqlDataReader read = SqlHelper.ExecuteReader(System.Data.CommandType.Text, strSQL, null);
                while (read.Read())
                {
                    user = new UserInfo();
                    user.ID = read.IsDBNull(0) ? "" : read.GetGuid(0).ToString();
                    user.UserID = read.IsDBNull(1) ? "" : read.GetGuid(1).ToString();
                    user.UserName = read.IsDBNull(2) ? "" : read.GetString(2);
                    user.GH = read.IsDBNull(3) ? "" : read.GetString(3);
                    user.Pwd = read.IsDBNull(4) ? "" : read.GetString(4);
                    user.RealName = read.IsDBNull(5) ? "" : read.GetString(5);
                    user.State = read.IsDBNull(6) ? "0" : read.GetInt32(6).ToString();
                    user.DepID = read.IsDBNull(7) ? "" : read.GetGuid(7).ToString();
                }

                if (!read.IsClosed)
                    read.Close();

                if (user != null)
                    user.UserKFList = GetKFListByUserID(user.UserID, user.DepID);
            }
            catch (Exception ex)
            {
                user = null;
            }
            return user;
        }

        /// <summary>
        /// 用关键字登录
        /// </summary>
        /// <param name="KeyWord"></param>
        /// <returns></returns>
        public UserInfo GetUsersByKeyWord(string KeyWord)
        {
            UserInfo user = null;
            try
            {
                string strSQL = string.Format(@"SELECT us.[ID]
                                                       ,us.[UserID]
                                                       ,[UserName]
                                                       ,[GH]
                                                       ,[pwd]
                                                       ,[realname]
                                                       ,[state]
                                                       ,[DepID]
                                                       FROM [Sun_User] us where us.KeyWord='{0}' and  us.state=1", KeyWord);

                SqlDataReader read = SqlHelper.ExecuteReader(System.Data.CommandType.Text, strSQL, null);
                while (read.Read())
                {
                    user = new UserInfo();
                    user.ID = read.IsDBNull(0) ? "" : read.GetGuid(0).ToString();
                    user.UserID = read.IsDBNull(1) ? "" : read.GetGuid(1).ToString();
                    user.UserName = read.IsDBNull(2) ? "" : read.GetString(2);
                    user.GH = read.IsDBNull(3) ? "" : read.GetString(3);
                    user.Pwd = read.IsDBNull(4) ? "" : read.GetString(4);
                    user.RealName = read.IsDBNull(5) ? "" : read.GetString(5);
                    user.State = read.IsDBNull(6) ? "0" : read.GetInt32(6).ToString();
                    user.DepID = read.IsDBNull(7) ? "" : read.GetGuid(7).ToString();
                }

                if (!read.IsClosed)
                    read.Close();

                if (user != null)
                    user.UserKFList = GetKFListByUserID(user.UserID,user.DepID);
            }
            catch (Exception ex)
            {
                user = null;
            }
            return user;
        }
        /// <summary>
        /// 获取人脸信息
        /// </summary>
        /// <returns></returns>
        public List<UserInfo> GetUsersByface()
        {
            List<UserInfo> faceuser = new List<UserInfo>();
            try
            {
                string strSQL = string.Format(@"SELECT [ID]
                                                       ,[UserID]
                                                       ,[UserName]
                                                       ,[GH]
                                                       ,[pwd]
                                                       ,[realname]
                                                       ,[state]
                                                       ,[DepID]
                                                       ,[facialInformation]   
                                                       FROM Sun_User WHERE facialInformation is not null");
                SqlDataReader read = SqlHelper.ExecuteReader(System.Data.CommandType.Text, strSQL, null);
                while (read.Read())
                {
                    UserInfo user = new UserInfo();
                    user.ID = read.IsDBNull(0) ? "" : read.GetGuid(0).ToString();
                    user.UserID = read.IsDBNull(1) ? "" : read.GetGuid(1).ToString();
                    user.UserName = read.IsDBNull(2) ? "" : read.GetString(2);
                    user.GH = read.IsDBNull(3) ? "" : read.GetString(3);
                    user.Pwd = read.IsDBNull(4) ? "" : read.GetString(4);
                    user.RealName = read.IsDBNull(5) ? "" : read.GetString(5);
                    user.State = read.IsDBNull(6) ? "0" : read.GetInt32(6).ToString();
                    user.DepID = read.IsDBNull(7) ? "" : read.GetGuid(7).ToString();
                    user.face = read.IsDBNull(8) ? null : (byte[])read[8];// ObjectToBytes(read.GetValue(8));
                    faceuser.Add(user);
                }

                if (!read.IsClosed)
                    read.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("查询报错");
                faceuser = null;
                throw ex;
            }
            return faceuser;
        }
        public UserInfo GetUsersByUserID(string UserID)
        {
            UserInfo user = null;
            try
            {
                string strSQL = string.Format(@"SELECT us.[ID]
                                                       ,us.[UserID]
                                                       ,[UserName]
                                                       ,[GH]
                                                       ,[pwd]
                                                       ,[realname]
                                                       ,[state]
                                                       ,[DepID]
                                                       FROM [Sun_User] us  where   us.state=1 and us.userid='{0}'",UserID);

                SqlDataReader read = SqlHelper.ExecuteReader(System.Data.CommandType.Text, strSQL, null);
                while (read.Read())
                {
                    user = new UserInfo();
                    user.ID = read.IsDBNull(0) ? "" : read.GetGuid(0).ToString();
                    user.UserID = read.IsDBNull(1) ? "" : read.GetGuid(1).ToString();
                    user.UserName = read.IsDBNull(2) ? "" : read.GetString(2);
                    user.GH = read.IsDBNull(3) ? "" : read.GetString(3);
                    user.Pwd = read.IsDBNull(4) ? "" : read.GetString(4);
                    user.RealName = read.IsDBNull(5) ? "" : read.GetString(5);
                    user.State = read.IsDBNull(6) ? "0" : read.GetInt32(6).ToString();
                    user.DepID = read.IsDBNull(7) ? "" : read.GetGuid(7).ToString();
                }

                if (!read.IsClosed)
                    read.Close();

                if (user != null)
                    user.UserKFList = GetKFListByUserID(user.UserID, user.DepID);
            }
            catch (Exception ex)
            {
                user = null;
            }
            return user;
        }

        public List<UserFingerPrint> GetUserByFingerPrint(string KFID)
        {
            List<UserFingerPrint> list = new List<UserFingerPrint>();
            try
            {
                string strSQL = @"select f.UserID,FingerPrint from Sun_UserFingerPrint f 
                                            inner join sun_user u on f.userid = u.userid
                                            inner join H_用户库房 h on u.userid = h.userid
                                            where u.state = 1 and f.isdisable = 1
                                            and h.kfid = '"+KFID+"'";
                SqlDataReader read = SqlHelper.ExecuteReader(System.Data.CommandType.Text, strSQL, null);
              
                while (read.Read())
                {
                    UserFingerPrint obj = new UserFingerPrint();
                    obj.UserID = read.IsDBNull(0) ? "" : read.GetGuid(0).ToString();
                    obj.FingerPrint = read.IsDBNull(1) ? null : (byte[])read[1];
                    list.Add(obj);
                }
                if (!read.IsClosed)
                    read.Close();
            }
            catch (Exception ex)
            {
                list = null;
            }
            return list;
        }

        /// <summary>
        /// 获取编号
        /// </summary>
        /// <param name="UserDepID"></param>
        /// <param name="lb"></param>
        /// <returns></returns>
        public string GetBH(string UserDepID, string lb)
        {
            string BH = "";
            string strsql = "exec H_GetBH '" + UserDepID + "','" + lb + "'";
            BH = Convert.ToString(SqlHelper.ExecuteScalarText(strsql));
            return BH;
        }

        /// <summary>
        /// 注册一个新设备
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool InsertForceInfo(ForcerInfo obj)
        {
            string sql = "insert into H_ForcerInfo(ForcerNo,ForcerName,Remark,IsDisable,Dtime,macAddress,KFID,SJKFID) values('"+obj.ForcerNo+"','"+obj.ForcerName+"','客户端自动注册','0',getdate(),'"+obj.MacAddress+"','"+obj.KFID+"','"+obj.SJKFID+"')";
            bool vResult = SqlHelper.ExecteNonQuery(CommandType.Text, sql) > 0 ? true : false;
            return vResult;
        }

        /// <summary>
        /// 验证设备是否已经注册
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public ForcerInfo CheckForcerIsRegiser(string macAddress)
        {
            string sql = "select top 1 a.[ID],[ForcerNo],[ForcerName],[Remark],[IsDisable],a.[DTime],a.[UserID],[macAddress],a.KFID,SJKFID ,b.库房名称 ,c.库房名称 as 上级库房名称 " +
                "from H_ForcerInfo a inner join H_库房 b on a.kfid = b.kfid inner join H_库房 c on a.sjkfid = c.kfid where macAddress='"+macAddress+"'";
            ForcerInfo obj = null;

            SqlDataReader read = SqlHelper.ExecuteReader(System.Data.CommandType.Text, sql, null);
            while (read.Read())
            {
                obj = new ForcerInfo();
                obj.ID = read.IsDBNull(0) ? "" : read.GetGuid(0).ToString();
                obj.ForcerNo = read.IsDBNull(1) ? "" : read.GetString(1);
                obj.ForcerName = read.IsDBNull(2) ? "" : read.GetString(2);
                obj.Remark = read.IsDBNull(3) ? "" : read.GetString(3);
                obj.IsDisable = read.IsDBNull(4) ? false : Convert.ToBoolean(read.GetInt32(4));
                obj.DateTime = read.IsDBNull(5) ? DateTime.MinValue : read.GetDateTime(5);
                obj.UserID = read.IsDBNull(6) ? "" : read.GetGuid(6).ToString();
                obj.MacAddress = read.IsDBNull(7) ? "" : read.GetString(7);
                obj.KFID = read.IsDBNull(8) ? "" : read.GetGuid(8).ToString();
                obj.SJKFID = read.IsDBNull(9) ? "" : read.GetGuid(9).ToString();
                obj.KfName= read.IsDBNull(10) ? "" : read.GetString(10);
                obj.SjKFName= read.IsDBNull(11) ? "" : read.GetString(11);
                break;
            }

            if (!read.IsClosed)
                read.Close();
            return obj;
        }

        /// <summary>
        /// 获得所有库房信息
        /// </summary>
        /// <returns></returns>
        public List<KFInfo> GetKFList()
        {
            string sql = "select kfid,库房名称 from H_库房 where 库房类型<>'供应商' and state=1 and depid = 'B0000000-0001-0002-0000-111000000000'";
            SqlDataReader read = SqlHelper.ExecuteReader(System.Data.CommandType.Text, sql, null);
            List<KFInfo> list = new List<KFInfo>();

            while (read.Read())
            {
                KFInfo kf = new KFInfo();
                kf.KFID = read.IsDBNull(0) ? "" : read.GetGuid(0).ToString();
                kf.KFMC = read.IsDBNull(1) ? "" : read.GetString(1);
                list.Add(kf);
            }
            if (!read.IsClosed)
                read.Close();
            return list;
        }

        /// <summary>
        /// 获得所有主库
        /// </summary>
        /// <returns></returns>
        public KFInfo GetZKKF(string DepID)
        {
            string sql = "select kfid,库房名称 from H_库房 where 库房类型='主库' and state=1 and depid = '"+DepID+"'";
            SqlDataReader read = SqlHelper.ExecuteReader(System.Data.CommandType.Text, sql, null);
            KFInfo kf = new KFInfo();

            while (read.Read())
            {
                
                kf.KFID = read.IsDBNull(0) ? "" : read.GetGuid(0).ToString();
                kf.KFMC = read.IsDBNull(1) ? "" : read.GetString(1);
            }
            if (!read.IsClosed)
                read.Close();
            return kf;
        }



        /// <summary>
        /// 获得用户下的仓库
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="UserDepID"></param>
        /// <returns></returns>
        public List<KFInfo> GetKFListByUserID(string UserID,string UserDepID)
        {
            string sql = "SELECT A.KFID, 库房名称 FROM H_库房 A inner join H_用户库房 B ON A.kfid=B.kfid where b.UserID='" + UserID + "' AND DEPID='" + UserDepID + "'  and 库房类型<>'厂商' ORDER BY 库房名称";
            SqlDataReader read = SqlHelper.ExecuteReader(CommandType.Text, sql, null);
            List<KFInfo> list = new List<KFInfo>();

            while (read.Read())
            {
                KFInfo kf = new KFInfo();
                kf.KFID = read.IsDBNull(0) ? "" : read.GetGuid(0).ToString();
                kf.KFMC = read.IsDBNull(1) ? "" : read.GetString(1);
                list.Add(kf);
            }

            if (!read.IsClosed)
                read.Close();
            return list;
        }

        /// <summary>
        /// 获得库房下的所有用户信息
        /// </summary>
        /// <param name="KFID"></param>
        /// <returns></returns>
        public List<UserInfo> GetUserListByKFID(string KFID)
        {
            List<UserInfo> list = new List<UserInfo>();
            string sql = "SELECT A.UserID, userName FROM sun_User A inner join H_用户库房 B ON A.userid=B.userid where kfid='"+KFID+"'";
            SqlDataReader read = SqlHelper.ExecuteReader(CommandType.Text, sql, null);
            while (read.Read())
            {
                UserInfo kf = new UserInfo();
                kf.UserID = read.IsDBNull(0) ? "" : read.GetGuid(0).ToString();
                kf.UserName = read.IsDBNull(1) ? "" : read.GetString(1);
                list.Add(kf);
            }

            if (!read.IsClosed)
                read.Close();
            return list;
        }

        /// <summary>
        /// 写入一条出库数据
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool InsertCKInfo(InBillInfo obj)
        {
            bool result = false;
            List<string> listSQL = new List<string>();
            string sql = "Insert into H_出库单(CKID,操作员,出库类型,UserID,KFID1,KFID2,DEPID,出库单编号,备注,出库日期)" +
                " values('" + obj.DJID + "','" + obj.UserName + "','" + obj.CKType + "','" + obj.UserID + "','" + obj.CKKFID + "','" + obj.RKKFID + "','"+obj.DepID+"','" + obj.BillNo + "','" + obj.Remark + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "')";
            listSQL.Add(sql);
            foreach (var detail in obj.DetailList)
            {
                if (!detail.XZ)
                    continue;
                if (detail.SL == 0)
                    continue;
                //sql = "Insert into H_出库单明细(TagID,RKMXID,CPID,CKID,批号,有效期,数量,单价,金额) values('"+detail.TagID+"','" + detail.RKMXID + "','" + detail.CPID + "','" + detail.DJID + "','" 
                //    + detail.Batch + "','" + detail.YXQ + "','" + detail.SL + "','" + detail.DJ + "','" + detail.JE + "') \n";
                //sql = "exec H_CKDMX '"+obj.DJID+"','"+detail.CPID+"','"+detail.Batch+"',"+detail.SL+"";
                sql = "exec H_CKDMX_DZG '" + obj.DJID + "','" + detail.CPID + "'," + detail.SL + "";
                listSQL.Add(sql);
            }

            sql = "exec H_CKD_TO_KC '" + obj.DJID + "'";
            listSQL.Add(sql);
            int ExecResult=SqlHelper.ExecteNonQueryTrans(listSQL);
            if (ExecResult > 0) result = true;
            return result;
        }

        /// <summary>
        /// 获得库存信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public List<StorageInfo> GetStorage(StorageInfo obj)
        {
            List<StorageInfo> list = new List<StorageInfo>();
            string cpbh="", cpmc="", gg="", xh="", ph="";
            if (!string.IsNullOrEmpty(obj.CPBH))
                cpbh += " and 产品编号='"+obj.CPBH+"' ";
            if (!string.IsNullOrEmpty(obj.CPName))
                cpmc += " and 产品名称 like '%"+obj.CPName+"%' ";
            if (!string.IsNullOrEmpty(obj.GG))
                gg += " and 规格 like '%"+obj.GG+"%' ";
            if (!string.IsNullOrEmpty(obj.XH))
                xh += " and 型号 like '%"+obj.XH+"%' ";
            if (!string.IsNullOrEmpty(obj.Batch))
                ph += " and 批号 like '%"+obj.Batch+"%' ";
            string sql = "select  '' as RKMXID,A.CPID,产品编号,产品名称,产品组别,规格,型号,单位,sum(B.数量) as 数量,'' as 批号,'' as 有效期,a.供应商,a.货权类型,a.出货单价,'' as ID from H_产品 a INNER JOIN H_库存 B on A.CPID=B.CPID where B.KFID='" + obj.KFID + "' AND   数量>0  and B.state=1 " + cpmc + cpbh + gg + xh + ph + "  group by A.CPID,产品编号,产品名称,产品组别,规格,型号,单位,a.供应商,a.货权类型,a.出货单价";
            SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.Text, sql, null);
            while (reader.Read())
            {
                StorageInfo st = new StorageInfo();
                st.RKMXID = reader.IsDBNull(0) ? "" : reader.GetString(0);
                st.CPID = reader.IsDBNull(1) ? "" : reader.GetGuid(1).ToString();
                st.CPBH = reader.IsDBNull(2) ? "" : reader.GetString(2);
                st.CPName= reader.IsDBNull(3) ? "" : reader.GetString(3);
                st.CPZB= reader.IsDBNull(4) ? "" : reader.GetString(4);
                st.GG= reader.IsDBNull(5) ? "" : reader.GetString(5);
                st.XH= reader.IsDBNull(6) ? "" : reader.GetString(6);
                st.DW= reader.IsDBNull(7) ? "" : reader.GetString(7);
                st.SL= reader.IsDBNull(8) ? 0 : reader.GetInt32(8);
                st.Batch= reader.IsDBNull(9) ? "" : reader.GetString(9);
                st.YXQ = "";// reader.IsDBNull(10) ? "" : reader.GetDateTime(10).ToString();
                st.GYS= reader.IsDBNull(11) ? "" : reader.GetString(11);
                st.HQLX= reader.IsDBNull(12) ? "" : reader.GetString(12);
                st.CKDJ= reader.IsDBNull(13) ? 0 : reader.GetDecimal(13);
                st.ID= reader.IsDBNull(14) ? "" : reader.GetString(14);
                list.Add(st);
            }

            if (!reader.IsClosed)
                reader.Close();
            return list;
        }



        /// <summary>
        /// 获得交接班信息
        /// </summary>
        /// <param name="KFID">正式库ID</param>
        /// <param name="KFID2">临时库ID</param>
        /// <param name="DT1">开始时间</param>
        /// <param name="DT2">结束时间</param>
        /// <returns></returns>
        public List<CYInfo> GetJJBInfo(string KFID, string KFID2, string DT1, string DT2)
        {
            List<CYInfo> list = new List<CYInfo>();
            string sql = "exec [H_JJBCX] @KFID = '" + KFID + "',@KFID1='"+KFID2+"',@DT1 = '" + DT1 + "',@DT2 = '" + DT2 + "'";
            SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.Text, sql, null);
            //DataTable dtable = SqlHelper.ExecuteDataTable(sql, null);
            while (reader.Read())
            {
                CYInfo st = new CYInfo();
                st.XMDM = reader.IsDBNull(0) ? "" : reader.GetString(0);
                st.XMMC = reader.IsDBNull(1) ? "" : reader.GetString(1);
                st.JFSL = reader.IsDBNull(2) ? 0 : reader.GetInt32(2);
                st.LYSL = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
                st.GHSL = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
                st.CYSL = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
                st.KCSL = reader.IsDBNull(6) ? 0 : reader.GetInt32(6);
                st.AQKC = reader.IsDBNull(7) ? 0 : reader.GetInt32(7);
                st.GG = reader.IsDBNull(8) ? "" : reader.GetString(8);
                st.SHSL = 0;


                CYInfo obj = list.Find(T => T.XMDM == st.XMDM.Substring(0, st.XMDM.Length - 1));
                if (obj != null)
                {
                    obj.JFSL += st.JFSL;
                    obj.LYSL += st.LYSL;
                    obj.GHSL += st.GHSL;
                    obj.CYSL += st.CYSL;
                    obj.KCSL += st.KCSL;
                    obj.AQKC += st.AQKC;
                }
                else
                {
                    list.Add(st);
                }
            }
            if (!reader.IsClosed)
                reader.Close();

            return list;
        }


        public List<CYInfo> GetLSJJBInfo(string ID, string SJKFID, string KFID, string BeginDate, string EndDate)
        {
            List<CYInfo> list = new List<CYInfo>();
            string sql = "select * from H_交接班记录明细 where jjbid='"+ID+"'";
            SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.Text, sql, null);
            //DataTable dtable = SqlHelper.ExecuteDataTable(sql, null);
            while (reader.Read())
            {
                CYInfo st = new CYInfo();
                
                st.XMDM = reader.IsDBNull(1) ? "" : reader.GetString(1);
                st.XMMC = reader.IsDBNull(2) ? "" : reader.GetString(2);
                st.JFSL = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
                st.LYSL = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
                st.GHSL = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
                st.CYSL = reader.IsDBNull(6) ? 0 : reader.GetInt32(6);
                st.KCSL = reader.IsDBNull(7) ? 0 : reader.GetInt32(7);
                st.AQKC = reader.IsDBNull(8) ? 0 : reader.GetInt32(8);
                st.GG = reader.IsDBNull(9) ? "" : reader.GetString(9);
                st.SHSL= reader.IsDBNull(10) ? 0 : reader.GetInt32(10);
                list.Add(st);

                //CYInfo obj = list.Find(T => T.XMDM == st.XMDM.Substring(0, st.XMDM.Length - 1));
                //if (obj != null)
                //{
                //    obj.JFSL += st.JFSL;
                //    obj.LYSL += st.LYSL;
                //    obj.GHSL += st.GHSL;
                //    obj.CYSL += st.CYSL;
                //    obj.KCSL += st.KCSL;
                //    obj.AQKC += st.AQKC;
                //}
                //else
                //{
                //    list.Add(st);
                //}
            }
            if (!reader.IsClosed)
                reader.Close();

            if (list.Count == 0)
            {
                list=GetJJBInfo(SJKFID,KFID,BeginDate,EndDate);
            }

            return list;
        }

        public List<LSJJBInfo> GetLSJJB(string KFID)
        {
            List<LSJJBInfo> list = new List<LSJJBInfo>();
            /*string sql = @"select top 100 h.id,h.ckid,h.userid,u.realname,h.begindatetime,h.enddatetime,k.库房名称,h.remark from H_交接班记录 h 
                                inner join sun_user u on h.userid=u.userid 
                                inner join H_库房 k on h.ckid=k.kfid
                                order by enddatetime desc";*/
            string sql= @"select top 100 h.id,h.ckid,h.userid,u.realname,h.begindatetime,h.enddatetime,k.库房名称,h.remark from H_交接班记录 h 
                                inner join sun_user u on h.userid=u.userid 
                                inner join H_库房 k on h.ckid=k.kfid
                                where h.ckid='"+KFID+"' order by enddatetime desc";
            SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.Text, sql, null);
           //会抛异常 
            while (reader.Read())
            {
                LSJJBInfo st = new LSJJBInfo();
                st.ID = reader.IsDBNull(0) ? "" : reader.GetGuid(0).ToString();
                st.CKID = reader.IsDBNull(1) ? "" : reader.GetGuid(0).ToString();
                st.UserID = reader.IsDBNull(2) ? "" : reader.GetGuid(0).ToString();
                st.UserName = reader.IsDBNull(3) ? "" : reader.GetString(3);
                st.BeginDate = reader.IsDBNull(4) ? "" : reader.GetDateTime(4).ToString("yyyy-MM-dd HH:mm:ss");
                st.EndDate = reader.IsDBNull(5) ? "" : reader.GetDateTime(5).ToString("yyyy-MM-dd HH:mm:ss");
                st.CkName = reader.IsDBNull(6) ? "" : reader.GetString(6);
                st.Remark = reader.IsDBNull(7) ? "" : reader.GetString(7);
                list.Add(st);
            }

            if (!reader.IsClosed)
                reader.Close();
            return list;
        }

        /// <summary>
        /// 保存一条交接班信息
        /// </summary>
        /// <param name="CKID"></param>
        /// <param name="DT1"></param>
        /// <param name="DT2"></param>
        /// <param name="UserID"></param>
        /// <param name="DepID"></param>
        /// <param name="Remark"></param>
        /// <returns></returns>
        public bool SaveJJBInfo(string ID,string CKID, string DT1, string DT2, string UserID, string DepID, string Remark,List<CYInfo> list)
        {
            
            bool result = false;
            List<string> sqlList = new List<string>();
            string sql = "INSERT into H_交接班记录(ID,CKID,BeginDateTime,EndDateTime,UserID,DepID,Remark)values('"+ID+"','" + CKID + "','" + DT1+ "','" + DT2 + "','" + UserID + "','" + DepID + "','" + Remark + "')";
            sqlList.Add(sql);
            foreach (var obj in list)
            {
                sql = "insert into H_交接班记录明细(JJBID,项目代码,项目名称,计费数量,领用数量,归还数量,差异数量,库存数量,安全库存,规格,损耗数量)values('"+ID+"','"+obj.XMDM+"','"+obj.XMMC+"',"+obj.JFSL+","+obj.LYSL+","+obj.GHSL+","+obj.CYSL+","+obj.KCSL+","+obj.AQKC+",'"+obj.GG+"',"+obj.SHSL+")";
                sqlList.Add(sql);
            }
            if (SqlHelper.ExecteNonQueryTrans(sqlList) > 0)
                result = true;
            return result;
        }

        /// <summary>
        /// 获得科室上一次交接班时间
        /// </summary>
        /// <param name="CKID"></param>
        /// <returns></returns>
        public string GetLastJJBRQ(string CKID)
        {
            string result = "";
            string sql = "select top 1 EndDateTime from H_交接班记录  where ckid='" + CKID + "' order by EndDateTime desc";
            result = Convert.ToString(SqlHelper.ExecuteScalar(CommandType.Text,sql));
            return result;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="KFID1">临时库库房ID</param>
        /// <param name="KFID2">正式库库房ID</param>
        /// <param name="BeginDate"></param>
        /// <param name="EndDate"></param>
        /// <param name="LX"></param>
        /// <returns></returns>
        public List<InBillInfo> GetBillInfo(string KFID1, string KFID2, string BeginDate, string EndDate, string LX)
        {
            List<InBillInfo> list = new List<InBillInfo>();
            try
            {
                string kfidstr = "";
                string SQL = "";
                if (LX.Equals("领用"))
                {
                    kfidstr = " and (kfid1= '" + KFID1 + "' and kfid2='" + KFID2 + "')";

                    SQL = @"SELECT ckid,kfid1,h1.库房名称 as 出库库房,kfid2, h2.库房名称 as 入库库房,出库日期,操作员,'领用' as 单据类型 FROM h_出库单 inner join H_库房 h1 on kfid1=h1.kfid inner join H_库房 h2 on kfid2 = h2.kfid where  h_出库单.state=1 and (出库日期 between  '" + BeginDate + "' and '" + EndDate + "') " + kfidstr;
                }
                else if (LX.Equals("归还"))
                {
                    kfidstr = " and (kfid1= '" + KFID2 + "' and kfid2='" + KFID1 + "')";
                    SQL = @"SELECT ckid,kfid1,h1.库房名称 as 出库库房,kfid2, h2.库房名称 as 入库库房,出库日期,操作员,'归还' as 单据类型 FROM h_出库单 inner join H_库房 h1 on kfid1=h1.kfid inner join H_库房 h2 on kfid2 = h2.kfid where h_出库单.state=1 and (出库日期 between  '" + BeginDate + "' and '" + EndDate + "') " + kfidstr;
                }
                else if (LX.Equals("全部"))
                {
                    kfidstr = " and (kfid1= '" + KFID1 + "' and kfid2='" + KFID2 + "')";
                    SQL += @"SELECT ckid,kfid1,h1.库房名称 as 出库库房,kfid2, h2.库房名称 as 入库库房,出库日期,操作员,'领用' as 单据类型 into #temp1 FROM h_出库单 inner join H_库房 h1 on kfid1=h1.kfid inner join H_库房 h2 on kfid2 = h2.kfid where h_出库单.state=1 and (出库日期 between  '" + BeginDate + "' and '" + EndDate + "')   " + kfidstr;


                    kfidstr = " and (kfid1= '" + KFID2 + "' and kfid2='" + KFID1 + "')";
                    SQL += @"SELECT ckid,kfid1,h1.库房名称 as 出库库房,kfid2, h2.库房名称 as 入库库房,出库日期,操作员,'归还' as 单据类型 into #temp2 FROM h_出库单 inner join H_库房 h1 on kfid1=h1.kfid inner join H_库房 h2 on kfid2 = h2.kfid where h_出库单.state=1 and (出库日期 between  '" + BeginDate + "' and '" + EndDate + "')   " + kfidstr;

                    SQL += "select * from #temp1 union select * from #temp2  ";
                    SQL += "drop table #temp1 ";
                    SQL += "drop table #temp2 ";
                }

                SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.Text, SQL, null);

                while (reader.Read())
                {
                    InBillInfo billInfo = new InBillInfo();
                    billInfo.DJID = reader.IsDBNull(0) ? "" : reader.GetGuid(0).ToString();
                    billInfo.CKKFID = reader.IsDBNull(1) ? "" : reader.GetGuid(1).ToString();
                    billInfo.CKKFName = reader.IsDBNull(2) ? "" : reader.GetString(2);
                    billInfo.RKKFID = reader.IsDBNull(3) ? "" : reader.GetGuid(3).ToString();
                    billInfo.RKKFName = reader.IsDBNull(4) ? "" : reader.GetString(4);
                    billInfo.SJ = reader.IsDBNull(5) ? "" : reader.GetDateTime(5).ToString("yyyy-MM-dd HH:mm:ss");
                    billInfo.UserName = reader.IsDBNull(6) ? "" : reader.GetString(6);
                    billInfo.DJLX= reader.IsDBNull(7) ? "" : reader.GetString(7);
                    list.Add(billInfo);
                }
                if (!reader.IsClosed)
                    reader.Close();

            }
            catch (Exception ex)
            {
                list = null;
            }
            return list;
        }
        public List<InBillInfo> GetBillInfoName(string KFID1, string KFID2, string BeginDate, string EndDate, string LX,string CpName)
        {
            List<InBillInfo> list = new List<InBillInfo>();
            try
            {
                string kfidstr = "";
                string SQL = "";
                if (LX.Equals("领用"))
                {
                    kfidstr = " and (kfid1= '" + KFID1 + "' and kfid2='" + KFID2 + "')";

                    SQL = @"SELECT ckid,kfid1,h1.库房名称 as 出库库房,kfid2, h2.库房名称 as 入库库房,出库日期,操作员,'领用' as 单据类型 FROM h_出库单 inner join H_库房 h1 on kfid1=h1.kfid inner join H_库房 h2 on kfid2 = h2.kfid where  h_出库单.state=1 and (出库日期 between  '" + BeginDate + "' and '" + EndDate + "')+and h2.产品编号='" + CpName + "' " + kfidstr;
                }
                else if (LX.Equals("归还"))
                {
                    kfidstr = " and (kfid1= '" + KFID2 + "' and kfid2='" + KFID1 + "')";
                    SQL = @"SELECT ckid,kfid1,h1.库房名称 as 出库库房,kfid2, h2.库房名称 as 入库库房,出库日期,操作员,'归还' as 单据类型 FROM h_出库单 inner join H_库房 h1 on kfid1=h1.kfid inner join H_库房 h2 on kfid2 = h2.kfid where h_出库单.state=1 and (出库日期 between  '" + BeginDate + "' and '" + EndDate + "') " + kfidstr;
                }
                else if (LX.Equals("全部"))
                {
                    kfidstr = " and (kfid1= '" + KFID1 + "' and kfid2='" + KFID2 + "')";
                    SQL += @"SELECT ckid,kfid1,h1.库房名称 as 出库库房,kfid2, h2.库房名称 as 入库库房,出库日期,操作员,'领用' as 单据类型 into #temp1 FROM h_出库单 inner join H_库房 h1 on kfid1=h1.kfid inner join H_库房 h2 on kfid2 = h2.kfid where h_出库单.state=1 and (出库日期 between  '" + BeginDate + "' and '" + EndDate + "')   " + kfidstr;


                    kfidstr = " and (kfid1= '" + KFID2 + "' and kfid2='" + KFID1 + "')";
                    SQL += @"SELECT ckid,kfid1,h1.库房名称 as 出库库房,kfid2, h2.库房名称 as 入库库房,出库日期,操作员,'归还' as 单据类型 into #temp2 FROM h_出库单 inner join H_库房 h1 on kfid1=h1.kfid inner join H_库房 h2 on kfid2 = h2.kfid where h_出库单.state=1 and (出库日期 between  '" + BeginDate + "' and '" + EndDate + "')   " + kfidstr;

                    SQL += "select * from #temp1 union select * from #temp2  ";
                    SQL += "drop table #temp1 ";
                    SQL += "drop table #temp2 ";
                }

                SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.Text, SQL, null);

                while (reader.Read())
                {
                    InBillInfo billInfo = new InBillInfo();
                    billInfo.DJID = reader.IsDBNull(0) ? "" : reader.GetGuid(0).ToString();
                    billInfo.CKKFID = reader.IsDBNull(1) ? "" : reader.GetGuid(1).ToString();
                    billInfo.CKKFName = reader.IsDBNull(2) ? "" : reader.GetString(2);
                    billInfo.RKKFID = reader.IsDBNull(3) ? "" : reader.GetGuid(3).ToString();
                    billInfo.RKKFName = reader.IsDBNull(4) ? "" : reader.GetString(4);
                    billInfo.SJ = reader.IsDBNull(5) ? "" : reader.GetDateTime(5).ToString("yyyy-MM-dd HH:mm:ss");
                    billInfo.UserName = reader.IsDBNull(6) ? "" : reader.GetString(6);
                    billInfo.DJLX = reader.IsDBNull(7) ? "" : reader.GetString(7);
                    list.Add(billInfo);
                }
                if (!reader.IsClosed)
                    reader.Close();

            }
            catch (Exception ex)
            {
                list = null;
            }
            return list;
        }
        public List<InBillDetailInfo> GetBillDetailInfo(string _CKID)
        {
            List<InBillDetailInfo> list = new List<InBillDetailInfo>();
            try
            {
                string SQL = @"SELECT 产品编号,产品名称,
                            规格,型号,数量,HISDM,批号,有效期,供应商,单位,
                            单价,金额=(单价*数量),国药ERP编码,供应商JDE编码 AS 供应商ERP编码,货权类型,耗材类型,计费类型
                            FROM H_出库单明细 A INNER JOIN H_产品 B  on A.CPID=B.CPID  where 数量>0  and CKID='" + _CKID + "'  ORDER BY a.DTime";

                SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.Text, SQL, null);

                while (reader.Read())
                {
                    InBillDetailInfo obj = new InBillDetailInfo();
                    obj.CPBH = reader.IsDBNull(0) ? "" : reader.GetString(0);
                    obj.CPMC = reader.IsDBNull(1) ? "" : reader.GetString(1);
                    obj.GG = reader.IsDBNull(2) ? "" : reader.GetString(2);
                    obj.XH = reader.IsDBNull(3) ? "" : reader.GetString(3);
                    obj.SL = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
                    obj.XMDM = reader.IsDBNull(5) ? "" : reader.GetString(5);
                    
                    list.Add(obj);
                }

                if (!reader.IsClosed)
                    reader.Close();
            }
            catch (Exception ex)
            {
                list = null;
            }
            return list;
        }

        public List<SYXXInfo> GetBRSYXXByDate(string KFID, string BeginDate, string EndDate, string XMDM)
        {
            List<SYXXInfo> list = new List<SYXXInfo>();
            try
            {
                string sql = "";
                if (KFID.ToUpper() == "A1594E6F-FAEC-41E3-AAD6-15CA2A633978")
                {
                    sql = "select 病人姓名,(case when 住院号='无' then 门诊号 else 住院号 end) as 病人号,床位号,SUM(数量) as 数量 from H_HIS收费记录表 a,H_HIS收费记录明细单 b where a.ID=b.JLDID and 费用日期 between '" + BeginDate + "' and '" + EndDate + "' and 项目代码 like '" + XMDM + "H' group by 病人姓名,(case when 住院号='无' then 门诊号 else 住院号 end),床位号";
                }
                else
                {
                    sql = "select 病人姓名,(case when 住院号='无' then 门诊号 else 住院号 end) as 病人号,床位号,SUM(数量) as 数量 from H_HIS收费记录表 a,H_HIS收费记录明细单 b where a.ID=b.JLDID and 费用日期 between '" + BeginDate + "' and '" + EndDate + "' and a.ckid='" + KFID + "' and 项目代码 like '" + XMDM + "%' group by 病人姓名,(case when 住院号='无' then 门诊号 else 住院号 end),床位号";
                }

                SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.Text, sql, null);

                while (reader.Read())
                {
                    SYXXInfo obj = new SYXXInfo();
                    obj.XM = reader.IsDBNull(0) ? "无" : reader.GetString(0);
                    obj.BRH = reader.IsDBNull(1) ? "无" : reader.GetString(1);
                    obj.CWH = reader.IsDBNull(2) ? "无" : reader.GetString(2);
                    obj.SL = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);

                    list.Add(obj);
                }
            }
            catch (Exception ex)
            {
                list = null;
            }
            return list;
        }
    }
}
