﻿using Business;
using Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace SmartRoom
{
    /// <summary>
    /// OutWindowsDetail.xaml 的交互逻辑
    /// </summary>
    public partial class OutDetailWindow : Window
    {
        InBillInfo outBill = new InBillInfo();
        List<InBillDetailInfo> billList = new List<InBillDetailInfo>();

        StorageBusiness stBusiness = new StorageBusiness();
        InBillInfoBusiness outBusiness = new InBillInfoBusiness();
        BHBusiness bhBusiness = new BHBusiness();
        KFBusiness kFBusiness = new KFBusiness();

        /// <summary>
        /// 0:表示领用 从临时库出到正式库;1:表示归还 从正式库出到临时库;
        /// </summary>
        int Type = 0;

        /// <summary>
        /// 0:表示领用;1:表示归还;
        /// </summary>
        /// <param name="_type"></param>
        public OutDetailWindow(int _type)
        {
            InitializeComponent();
            Type = _type;
            if (Type == 0)
            {
                lblAppTitle.Content = "耗材领用";
                txt_CKKF.Text = Global.forcerInfo.KfName;
            }
            else
            {
                lblAppTitle.Content = "耗材归还";
                txt_RKKF.Text = Global.forcerInfo.KfName;
            }
            string strException;
            Global.lockOpt.OpenDoor(out strException);
        }


        private System.Windows.Forms.Timer _tagTimer = new System.Windows.Forms.Timer();

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            lvw_RK.ItemsSource = billList;
            if (Type == 0)//领用
            {
                outBill.KFID = Global.forcerInfo.KFID;
                outBill.DHLX = "ckdh";
                //outBill.RKKFID = Global.forcerInfo.SJKFID;
            }
            else//归还
            {
                //outBill.CKKFID = Global.forcerInfo.SJKFID;
                outBill.KFID = Global.forcerInfo.KFID;
                outBill.DHLX = "rkdh";
            }
            outBill.Remark = "低值耗材分隔区出库单";
            outBill.UserID = Global.User.UserID;
            outBill.UserName = Global.User.UserName;
            outBill.CKType = "出库";
            outBill.DetailList = billList;
            //outBill.BillNo = bhBusiness.GetBH(Global.User.DepID, "C");
            outBill.DJID = Guid.NewGuid().ToString();
            outBill.DepID = Global.User.DepID;
            outBill.HosID = Global.HosID;

            txt_CPTM.Focus();
        }


        private void btn_Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        void Submit()
        {
            try
            {
                string content = "";
                if (billList == null || billList.Count == 0)
                {
                    Global.MessageWindow.ShowDialog("当前没有可提交的数据，请核实!", Enum.MessageButton.Close, Enum.MessageStyle.Warn);
                    return;
                }
                foreach (var obj in billList)
                {
                    if ((obj.SL > obj.KCSL) && (Type == 0))
                    {
                        Global.MessageWindow.ShowDialog("数量大于库存数量,请重新输入!", Enum.MessageButton.Cancel, Enum.MessageStyle.Warn);
                        return;
                    }
                    else if (obj.SL <0)
                    {
                        Global.MessageWindow.ShowDialog("数量不能为负数，请重新输入", Enum.MessageButton.Cancel, Enum.MessageStyle.Warn);
                        return;
                    }
                    else if (!obj.XZ || obj.SL == 0)
                        continue;
                    content += obj.CPMC + "---" + obj.SL + " \n";
                }
                if (string.IsNullOrEmpty(content))
                {
                    Global.MessageWindow.ShowDialog("当前没有可提交的数据，请核实!", Enum.MessageButton.Close, Enum.MessageStyle.Warn);
                    return;
                }
                if (Enum.MessageDialogResult.Yes == Global.MessageWindow.ShowDialog("请确认" + lblAppTitle.Content + "数据 \n" + content, Enum.MessageButton.YesNo, Enum.MessageStyle.Question))
                {
                    string result = "";
                    if (Type == 0)
                    {
                        result = outBusiness.InsertCKInfo(outBill);
                    }
                    else
                    {
                        result = outBusiness.InsertRKInfo(outBill);
                    }
                        
                    if (result == "0")
                    {
                        Global.MessageWindow.ShowDialog(lblAppTitle.Content + "成功!");
                        this.Close();
                    }
                    else
                    {
                        Global.MessageWindow.ShowDialog(lblAppTitle.Content + "失败!"+ result.ToString() , Enum.MessageButton.Close, Enum.MessageStyle.Error);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }


        private void btn_Clear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lock (((ICollection)billList).SyncRoot)
                {
                    billList = null;
                    lvw_RK.Items.Clear();
                    lvw_RK.Items.Refresh();
                }
            }
            catch (Exception ex)
            {

            }
        }


        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                string strException;
                Global.lockOpt.CloseDoor(out strException);
            }
            catch (Exception ex)
            { }
        }

        private void Complete_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            try
            {
                TextBox Box = (sender as TextBox);
                InBillDetailInfo obj = Box.DataContext as InBillDetailInfo;
                SetTotalSL();
                //if (obj.SL > obj.KCSL)
                //{
                //    Global.MessageWindow.ShowDialog("数量大于库存数量,请重新输入!", Enum.MessageButton.Cancel, Enum.MessageStyle.Warn);

                //    Box.Focus();
                //    Box.SelectAll();
                //}
            }
            catch (Exception ex)
            {

            }
        }

        private bool FindRow(StorageInfo obj)
        {
            bool result = false;
       

            for (int i = billList.Count; i >0; i--)
            {
                InBillDetailInfo item = billList[i - 1];

                if (obj.CPBH.Equals(item.CPBH))
                {
                    if (item.KCSL > item.SL)
                    {
                        item.XZ = true;
                        item.SL += 1;
                        result = true;
                        return result;
                    }
                    result = true;
                    return result;
                }

            }
            SetTotalSL();
            return result;
        }

        private bool FindRow(List<StorageInfo> list)
        {
            bool result = false;

            foreach (var obj in list)
            {
                for (int i = billList.Count; i > 0; i--)
                {
                    InBillDetailInfo item = billList[i - 1];

                    //if (item.SL==0)
                    //{
                    //    billList.Remove(item);
                    //    continue;
                    //}
                    if (obj.CPBH.Equals(item.CPBH))
                    {
                        //if (item.KCSL > item.SL)
                        //{
                        //    item.XZ = true;
                        //    item.SL += 1;
                        //    result = true;
                        //    return result;
                        //}
                        result = true;
                        return result;
                    }

                }
            }
            SetTotalSL();
            return result;
        }

        private void NewScan()
        {
            txt_CPGG.Text = string.Empty;
            txt_CPMC.Text = string.Empty;
            txt_CPPH.Text = string.Empty;
            txt_CPTM.Text = string.Empty;
            txt_CPXH.Text = string.Empty;
            //txt_CPTM.Focus();
        }

        private void GetLvwRowControl(int index)
        {
            try
            {
                lvw_RK.UpdateLayout();
                var myListBoxItem = (ListViewItem)lvw_RK.ItemContainerGenerator.ContainerFromIndex(index);
                var myContentPresenter = FindVisualChild<ContentPresenter>(myListBoxItem);
                DataTemplate myDataTemplate = myContentPresenter.ContentTemplate;
                var obj = myDataTemplate.FindName("Complete", myContentPresenter);
                TextBox Complete = obj as TextBox;//自定义控件
                Complete.Focus();
                Complete.Select(Complete.Text.Length, 0);
                lvw_RK.ScrollIntoView(lvw_RK.Items[index]);
            }
            catch (Exception ex)
            {

            }
        }

        private childItem FindVisualChild<childItem>(DependencyObject obj)
            where childItem : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is childItem)
                    return (childItem)child;
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }

        private void btn_Delete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Enum.MessageDialogResult.Yes == Global.MessageWindow.ShowDialog("确认删除当前选择的行?", Enum.MessageButton.YesNo, Enum.MessageStyle.Question))
                {
                    billList.RemoveAll(T => T.XZ == true);
                    lvw_RK.Items.Refresh();
                }
            }
            catch (Exception ex)
            {

            }
        }

        void Search()
        {
            try
            {
                StorageInfo cxtj = new StorageInfo();
                cxtj.CPName = txt_CPMC.Text.Trim();
                cxtj.GG = txt_CPGG.Text.Trim();
                cxtj.Batch = txt_CPPH.Text.Trim();
                cxtj.XH = txt_CPXH.Text.Trim();
                cxtj.CPBH = txt_CPTM.Text.Trim();
                cxtj.KFID = outBill.KFID;
                List<StorageInfo> list = stBusiness.GetStorageInfos(cxtj);
                if (!FindRow(list))
                {
                    //查询对应的库存信息

                    if (list != null && list.Count > 0)
                    {
                        bool isXZ = true;
                        foreach (var item in list)
                        {
                            InBillDetailInfo obj = new InBillDetailInfo();
                            obj.XZ = isXZ;
                            obj.SL = 0;
                            //if (!isXZ)
                            //{
                            //    obj.SL = 0;
                            //}
                            obj.DJID = outBill.DJID;
                            obj.KCSL = item.SL;
                            obj.GG = item.GG;
                            obj.XH = item.XH;
                            obj.Batch = item.Batch;
                            obj.DJ = item.CKDJ;
                            obj.JE = item.CKDJ * obj.SL;
                            obj.CPMC = item.CPName;
                            obj.DW = item.DW;
                            obj.RKMXID = item.RKMXID;
                            obj.CPBH = item.CPBH;
                            obj.CPID = item.CPID;
                            obj.YXQ = item.YXQ;
                            obj.TagID = item.ID;
                            billList.Add(obj);
                            //isXZ = false;
                        }
                    }
                }
                SetTotalSL();
                lvw_RK.Items.Refresh();
                GetLvwRowControl(lvw_RK.Items.Count - 1);
                NewScan();
            }
            catch (Exception ex)
            { }
            ///唤醒键盘
             //WakeKeyboard();
        }


        private void txt_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {

                Search();
            }
        }

        private DateTime _dt;  //定义一个成员函数用于保存每次的时间点

        private void txt2_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.NumLock) return;
            TextBox Box = (sender as TextBox);
            DateTime tempDt = DateTime.Now;          //保存按键按下时刻的时间点
            TimeSpan ts = tempDt.Subtract(_dt);     //获取时间间隔
            if (ts.TotalMilliseconds < 50)
            {
                txt_CPTM.Focus();
                Box.Text = Box.Text.Length == 1 ? "0" : Box.Text.Substring(0, Box.Text.Length - 1);
                return;
            }
            _dt = DateTime.Now;
            //唤醒键盘

            //if (e.Key == System.Windows.Input.Key.Enter && obj2.SL.ToString().Length>=8)
            //{
            //    StorageInfo cxtj = new StorageInfo();
            //    cxtj.CPName = txt_CPMC.Text.Trim();
            //    cxtj.GG = txt_CPGG.Text.Trim();
            //    cxtj.Batch = txt_CPPH.Text.Trim();
            //    cxtj.XH = txt_CPXH.Text.Trim();
            //    cxtj.CPBH = obj2.SL.ToString().Substring(3);
            //    cxtj.KFID = inBill.CKKFID;
            //    if (!Box.Undo())
            //    {
            //        Box.Text = "0";
            //    }

            //    //查询对应的库存信息
            //    List<StorageInfo> list = stBusiness.GetStorageInfos(cxtj);
            //    if (!FindRow(list))
            //    {
            //        if (list != null && list.Count > 0)
            //        {
            //            bool isXZ = true;
            //            foreach (var item in list)
            //            {
            //                InBillDetailInfo obj = new InBillDetailInfo();
            //                obj.XZ = isXZ;
            //                obj.SL = 0;
            //                obj.DJID = inBill.DJID;
            //                obj.KCSL = item.SL;
            //                obj.GG = item.GG;
            //                obj.XH = item.XH;
            //                obj.Batch = item.Batch;
            //                obj.DJ = item.CKDJ;
            //                obj.JE = item.CKDJ * obj.SL;
            //                obj.CPMC = item.CPName;
            //                obj.DW = item.DW;
            //                obj.RKMXID = item.RKMXID;
            //                obj.CPBH = item.CPBH;
            //                obj.CPID = item.CPID;
            //                obj.YXQ = item.YXQ;
            //                obj.TagID = item.ID;
            //                billList.Add(obj);
            //                //isXZ = false;
            //            }
            //        }
            //    }

            //    SetTotalSL();
            //    lvw_RK.Items.Refresh();
            //    GetLvwRowControl(lvw_RK.Items.Count - 1);
            //    NewScan();
            //}
        }

        public void WakeKeyboard()
        {
            try
            {
                Process.Start(Environment.CurrentDirectory + "\\Asset\\exe\\keyboard.exe");
            }
            catch
            {
                try
                {
                    Process.Start(@"C:\Program Files\Common Files\Microsoft Shared\Ink\TabTip.exe");
                }
                catch
                { }
            }
        }

        private void Complete_Checked(object sender, RoutedEventArgs e)
        {
            SetTotalSL();
        }


        void SetTotalSL()
        {
            int Total = 0;
            int CZTotal = 0;
            foreach (var obj in billList)
            {
                Total += obj.KCSL;
                CZTotal += obj.SL;
            }
            lbl_Totl.Content = Total;
            lbl_LYTotl.Content = CZTotal;
        } 
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_Search_Click(object sender, RoutedEventArgs e)
        {
            Search();
        }

        private void btnC_Submit_Click(object sender, RoutedEventArgs e)
        {
            Submit();
        }

    }
}
