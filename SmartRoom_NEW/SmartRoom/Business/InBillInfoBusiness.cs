﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;

namespace Business
{
    
    public class InBillInfoBusiness
    {
        DataFactory.DataFactory dataFactory = new DataFactory.DataFactory();

        public bool InsertCKInfo(InBillInfo obj)
        {
            if (obj == null)
                return false;
            
            return dataFactory.InsertCKInfo(obj);
        }

        public List<InBillInfo> GetBillInfo(string KFID1, string KFID2, string BeginDate, string EndDate, string LX)
        {
            return dataFactory.GetBillInfo(KFID1,KFID2,BeginDate,EndDate,LX);
        }
        /// <summary>
        /// 根据产品编号查询归还和领用的数据
        /// </summary>
        /// <param name="KFID1"></param>
        /// <param name="KFID2"></param>
        /// <param name="BeginDate"></param>
        /// <param name="EndDate"></param>
        /// <param name="LX"></param>
        /// <returns></returns>
        public List<InBillInfo> GetBillInfoName(string KFID1, string KFID2, string BeginDate, string EndDate, string LX ,string CpName)
        {
            return dataFactory.GetBillInfoName(KFID1, KFID2, BeginDate, EndDate, LX, CpName);
        }

        public List<InBillDetailInfo> GetBillDetailInfo(string _CKID)
        {
            return dataFactory.GetBillDetailInfo(_CKID);
        }
    }
}
