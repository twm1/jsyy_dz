﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class JJBBusiness
    {
        DataFactory.DataFactory m_DataFactory = new DataFactory.DataFactory();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="KFID">正式库ID</param>
        /// <param name="KFID2">临时库ID</param>
        /// <param name="DT1">开始时间</param>
        /// <param name="DT2">结束时间</param>
        /// <returns></returns>
        public List<CYInfo> GetJJBInfo(string KFID, string KFID2, string DT1, string DT2)
        {
            List<CYInfo> list= m_DataFactory.GetJJBInfo(KFID, KFID2, DT1, DT2);
            
            return list;
        }

        public List<CYInfo> GetLSJJBInfo(string ID, string SJKFID, string KFID, string BeginDate, string EndDate)
        {
            List<CYInfo> list = m_DataFactory.GetLSJJBInfo(ID,SJKFID,KFID,BeginDate,EndDate);

            return list;
        }

        /// <summary>
        /// 保存一条交接班信息
        /// </summary>
        /// <param name="CKID"></param>
        /// <param name="DT1"></param>
        /// <param name="DT2"></param>
        /// <param name="UserID"></param>
        /// <param name="DepID"></param>
        /// <param name="Remark"></param>
        /// <returns></returns>
        public bool SaveJJBInfo(string ID,string CKID, string DT1, string DT2, string UserID, string DepID, string Remark,List<CYInfo> list)
        {
            return m_DataFactory.SaveJJBInfo(ID,CKID,DT1,DT2,UserID,DepID,Remark,list);
        }

        /// <summary>
        /// 获得科室上一次交接班时间
        /// </summary>
        /// <param name="CKID"></param>
        /// <returns></returns>
        public string GetLastJJBRQ(string CKID)
        {
            return m_DataFactory.GetLastJJBRQ(CKID);
        }

        public List<LSJJBInfo> GetLSJJB(string KFID)
        {
            return m_DataFactory.GetLSJJB(KFID);
        }

        public List<SYXXInfo> GetBRSYXXBYDate(string KFID,string BeginDate,string EndDate,string XMDM)
        {
            return m_DataFactory.GetBRSYXXByDate(KFID,BeginDate,EndDate,XMDM);
        }
    }
}
