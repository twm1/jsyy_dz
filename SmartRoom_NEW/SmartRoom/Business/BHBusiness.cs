﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class BHBusiness
    {
        DataFactory.DataFactory dataFactory = new DataFactory.DataFactory();
        public string GetBH(string DepID, string LB)
        {
            return dataFactory.GetBH(DepID,LB);
        }
    }
}
