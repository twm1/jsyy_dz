﻿using System;
using System.Configuration;
using System.Windows;
using System.Windows.Input;

namespace SmartRoom
{
    /// <summary>
    /// MenuWindows.xaml 的交互逻辑
    /// </summary>
    public partial class MenuWindows : Window
    {

        private System.Windows.Forms.Timer _tagTimer = new System.Windows.Forms.Timer();
        

        public MenuWindows()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //_tagTimer.Interval = 1;
            //_tagTimer.Tick += new EventHandler(ShowTime);
            //_tagTimer.Start();
            if (Global.User != null)
                lbl_LogName.Content = Global.User.RealName;
            if (Global.forcerInfo != null)
                lbl_ks.Content = Global.forcerInfo.KfName;
            string Exception = string.Empty;

            Global.LockPort = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["LockPort"].Value;
            Global.LockBaud = Convert.ToInt32(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["LockBaud"].Value);
            Global.WindowsList.Add(this);
         
        }




        #region  MouseLeftButtonDown
        /// <summary>
        /// 码货
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Image_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            try
            {
                //if (Global.IsTouch == 1)
                //    return;
                //Global.MessageWindow.ShowDialog("领用!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
                InStockBillWindow InStockBillWindow = new InStockBillWindow();
                Global.WindowsList.Add(InStockBillWindow);
                InStockBillWindow.ShowDialog();
                Global.WindowsList.Remove(InStockBillWindow);
            }
            catch (Exception ex)
            { }
            //try
            //{
            //    //if (Global.IsTouch == 1)
            //    //    return;
            //    //Global.MessageWindow.ShowDialog("码货!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
            //    bool isRule = false;
            //    foreach (var obj in Global.User.UserKFList)
            //    {
            //        if (obj.KFID.ToUpper().Equals("322B6C82-D538-4698-9F19-5C96C91597AE"))
            //        {
            //            isRule = true;
            //            break;
            //        }
            //    }
            //    if (isRule)
            //    {
            //        InDetailWindow inDetailWindow = new InDetailWindow(0);
            //        Global.WindowsList.Add(inDetailWindow);
            //        inDetailWindow.ShowDialog();
            //        Global.WindowsList.Remove(inDetailWindow);
            //    }
            //    else
            //    {
            //        Global.MessageWindow.ShowDialog("您没有主库操作权限!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
            //    }
            //}
            //catch (Exception ex)
            //{ }
        }

        /// <summary>
        /// 领用
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Image_MouseLeftButtonDown_2(object sender, MouseButtonEventArgs e)
        {
            try
            {
                //if (Global.IsTouch == 1)
                //    return;
                //Global.MessageWindow.ShowDialog("领用!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
                OutDetailWindow outWindow = new OutDetailWindow(0);
                Global.WindowsList.Add(outWindow);
                outWindow.ShowDialog();
                Global.WindowsList.Remove(outWindow);
            }
            catch (Exception ex)
            { }
        }

        /// <summary>
        /// 开门
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Image_MouseLeftButtonDown_3(object sender, MouseButtonEventArgs e)
        {
            try
            {
                //if (Global.IsTouch == 1)
                //    return;
                //Global.MessageWindow.ShowDialog("开门!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
                string strException;
                Global.lockOpt.OpenDoor(out strException);
            }
            catch (Exception ex)
            { }
        }

        /// <summary>
        /// 归还
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Image_MouseLeftButtonDown_5(object sender, MouseButtonEventArgs e)
        {
            try
            {
                //if (Global.IsTouch == 1)
                //    return;
                //Global.MessageWindow.ShowDialog("归还!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
                OutDetailWindow outWindow = new OutDetailWindow(1);
                Global.WindowsList.Add(outWindow);
                outWindow.ShowDialog();
                Global.WindowsList.Remove(outWindow);
            }
            catch (Exception ex)
            { }
        }

        /// <summary>
        /// 关门
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Image_MouseLeftButtonDown_6(object sender, MouseButtonEventArgs e)
        {
            try
            {
                //if (Global.IsTouch == 1)
                //    return;
                //Global.MessageWindow.ShowDialog("关门!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
                string strException;
                Global.lockOpt.CloseDoor(out strException);
                //this.Close();
            }
            catch (Exception ex)
            { }
        }

        /// <summary>
        /// 退货
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Image_MouseLeftButtonDown_7(object sender, MouseButtonEventArgs e)
        {
            //try
            //{
            //    //if (Global.IsTouch == 1)
            //    //    return;
            //    //Global.MessageWindow.ShowDialog("退货!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
            //    bool isRule = false;
            //    foreach (var obj in Global.User.UserKFList)
            //    {
            //        if (obj.KFID.ToUpper().Equals("322B6C82-D538-4698-9F19-5C96C91597AE"))
            //        {
            //            isRule = true;
            //            break;
            //        }
            //    }
            //    if (isRule)
            //    {
            //        InDetailWindow inDetailWindow = new InDetailWindow(1);
            //        Global.WindowsList.Add(inDetailWindow);
            //        inDetailWindow.ShowDialog();
            //        Global.WindowsList.Remove(inDetailWindow);
            //    }
            //    else
            //    {
            //        Global.MessageWindow.ShowDialog("您没有主库操作权限!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
            //    }
            //}
            //catch (Exception ex)
            //{ }
        }

        /// <summary>
        /// 交接班查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Image_MouseLeftButtonDown_8(object sender, MouseButtonEventArgs e)
        {
            try
            {
                //if (Global.IsTouch == 1) return;
                JJBCXWindow jjb = new JJBCXWindow();
                Global.WindowsList.Add(jjb);
                jjb.ShowDialog();
                Global.WindowsList.Remove(jjb);
            }
            catch (Exception ex)
            { }
        }
        
        /// <summary>
        /// 历史交接班
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Image_MouseLeftButtonDown_9(object sender, MouseButtonEventArgs e)
        {
            try
            {
                //if (Global.IsTouch == 1) return;
                LSJJBCXWindow jjb = new LSJJBCXWindow();
                Global.WindowsList.Add(jjb);
                jjb.ShowDialog();
                Global.WindowsList.Remove(jjb);
            }
            catch (Exception ex)
            { }
        }

        /// <summary>
        /// 领用归还查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Image_MouseLeftButtonDown_10(object sender, MouseButtonEventArgs e)
        {
            try
            {
                //if (Global.IsTouch == 1) return;
                LYJLCXWindows jjb = new LYJLCXWindows();
                Global.WindowsList.Add(jjb);
                jjb.ShowDialog();
                Global.WindowsList.Remove(jjb);
            }
            catch (Exception ex)
            { }
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Image_MouseLeftButtonDown_11(object sender, MouseButtonEventArgs e)
        {
            try
            {
                //if (Global.IsTouch == 1) return;
                InputNumberWindow gmm = new InputNumberWindow();
                Global.WindowsList.Add(gmm);
                gmm.ShowDialog();
                Global.WindowsList.Remove(gmm);
            }
            catch (Exception ex)
            { }
        }

        private void bdrTop_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //if (Global.IsTouch == 1) return;
            foSearch.IsOpen = !foSearch.IsOpen;
        }

        #endregion


        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //if (!Global.lockOpt.CheckDoorIsClose())
            //{
            //    Global.MessageWindow.ShowDialog("柜子门尚未关闭，请关闭柜门后继续操作!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
            //    return;
            //}
            Global.WindowsList.Remove(this);
        }



        #region touch事件
        /// <summary>
        /// 码货
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Image_TouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                //if (Global.IsTouch == 1)
                //    return;
                //Global.MessageWindow.ShowDialog("领用!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
                InStockBillWindow InStockBillWindow = new InStockBillWindow();
                Global.WindowsList.Add(InStockBillWindow);
                InStockBillWindow.ShowDialog();
                Global.WindowsList.Remove(InStockBillWindow);
            }
            catch (Exception ex)
            { }
            //try
            //{
            //    if (Global.IsTouch == 0)
            //        return;
            //    //Global.MessageWindow.ShowDialog("码货!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
            //    bool isRule = false;
            //    foreach (var obj in Global.User.UserKFList)
            //    {
            //        if (obj.KFID.ToUpper().Equals("322B6C82-D538-4698-9F19-5C96C91597AE"))
            //        {
            //            isRule = true;
            //            break;
            //        }
            //    }
            //    if (isRule)
            //    {
            //        InDetailWindow inDetailWindow = new InDetailWindow(0);
            //        Global.WindowsList.Add(inDetailWindow);
            //        inDetailWindow.ShowDialog();
            //        Global.WindowsList.Remove(inDetailWindow);
            //    }
            //    else
            //    {
            //        Global.MessageWindow.ShowDialog("您没有主库操作权限!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
            //    }
            //}
            //catch (Exception ex)
            //{ }
        }

        /// <summary>
        /// 领用
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Image_TouchDown_1(object sender, TouchEventArgs e)
        {
            try
            {
                if (Global.IsTouch == 0)
                    return;
                //Global.MessageWindow.ShowDialog("领用!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
                OutDetailWindow outWindow = new OutDetailWindow(0);
                Global.WindowsList.Add(outWindow);
                outWindow.ShowDialog();
                Global.WindowsList.Remove(outWindow);
            }
            catch (Exception ex)
            { }
        }

        /// <summary>
        /// 开门
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Image_TouchDown_2(object sender, TouchEventArgs e)
        {
            try
            {
                if (Global.IsTouch == 0)
                    return;
                //Global.MessageWindow.ShowDialog("开门!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
                string strException;
                Global.lockOpt.OpenDoor(out strException);
            }
            catch (Exception ex)
            { }
        }

        /// <summary>
        /// 退货
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Image_TouchDown_3(object sender, TouchEventArgs e)
        {
            //try
            //{
            //    if (Global.IsTouch == 0)
            //        return;
            //    //Global.MessageWindow.ShowDialog("退货!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
            //    bool isRule = false;
            //    foreach (var obj in Global.User.UserKFList)
            //    {
            //        if (obj.KFID.ToUpper().Equals("322B6C82-D538-4698-9F19-5C96C91597AE"))
            //        {
            //            isRule = true;
            //            break;
            //        }
            //    }
            //    if (isRule)
            //    {
            //        InDetailWindow inDetailWindow = new InDetailWindow(1);
            //        Global.WindowsList.Add(inDetailWindow);
            //        inDetailWindow.ShowDialog();
            //        Global.WindowsList.Remove(inDetailWindow);
            //    }
            //    else
            //    {
            //        Global.MessageWindow.ShowDialog("您没有主库操作权限!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
            //    }
            //}
            //catch (Exception ex)
            //{ }
        }

        /// <summary>
        /// 关门
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Image_TouchDown_4(object sender, TouchEventArgs e)
        {
            try
            {
                if (Global.IsTouch == 0)
                    return;
                //Global.MessageWindow.ShowDialog("关门!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
                string strException;
                Global.lockOpt.CloseDoor(out strException);
                this.Close();
            }
            catch (Exception ex)
            { }
        }

        /// <summary>
        /// 归还
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Image_TouchDown_5(object sender, TouchEventArgs e)
        {
            try
            {
                if (Global.IsTouch == 0)
                    return;
                //Global.MessageWindow.ShowDialog("归还!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
                OutDetailWindow outWindow = new OutDetailWindow(1);
                Global.WindowsList.Add(outWindow);
                outWindow.ShowDialog();
                Global.WindowsList.Remove(outWindow);
            }
            catch (Exception ex)
            { }
        }


        private void bdrTop_TouchDown(object sender, TouchEventArgs e)
        {
            if (Global.IsTouch == 0) return;
            foSearch.IsOpen = !foSearch.IsOpen;
        }

   

        /// <summary>
        /// 交接班查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Image_TouchDown_8(object sender, TouchEventArgs e)
        {
            try
            {
                if (Global.IsTouch == 0) return;
                JJBCXWindow jjb = new JJBCXWindow();
                Global.WindowsList.Add(jjb);
                jjb.ShowDialog();
                Global.WindowsList.Remove(jjb);
            }
            catch (Exception ex)
            { }
        }

        /// <summary>
        /// 历史交接班
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Image_TouchDown_9(object sender, TouchEventArgs e)
        {
            try
            {
                if (Global.IsTouch == 0) return;
                LSJJBCXWindow jjb = new LSJJBCXWindow();
                Global.WindowsList.Add(jjb);
                jjb.ShowDialog();
                Global.WindowsList.Remove(jjb);
            }
            catch (Exception ex)
            { }
        }

        /// <summary>
        /// 领用归还查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Image_TouchDown_10(object sender, TouchEventArgs e)
        {
            try
            {
                if (Global.IsTouch == 0) return;
                LYJLCXWindows jjb = new LYJLCXWindows();
                Global.WindowsList.Add(jjb);
                jjb.ShowDialog();
                Global.WindowsList.Remove(jjb);
            }
            catch (Exception ex)
            { }
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Image_TouchDown_11(object sender, TouchEventArgs e)
        {
            try
            {
                if (Global.IsTouch == 0) return;
                InputNumberWindow gmm = new InputNumberWindow();
                Global.WindowsList.Add(gmm);
                gmm.ShowDialog();
                Global.WindowsList.Remove(gmm);
            }
            catch (Exception ex)
            { }
        }
        #endregion



        private void img_Back_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (Global.IsTouch == 1) return;
            this.Close();
        }

        private void img_Back_TouchDown(object sender, TouchEventArgs e)
        {
            if (Global.IsTouch == 0) return;
            this.Close();
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DetailsWindow o = new DetailsWindow();
            Global.WindowsList.Add(o);
            o.ShowDialog();
            Global.WindowsList.Remove(o);
        }
    }
}
