﻿using Business;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SmartRoom
{
    /// <summary>
    /// DetailsWindow.xaml 的交互逻辑
    /// </summary>
    public partial class DetailsWindow : Window
    {

        StorageBusiness stBusiness = new StorageBusiness();

        public DetailsWindow()
        {
            InitializeComponent();
            initialized();
        }

        private void initialized()
        {
            userInfo.Content = Global.User.RealName;
            txt_DT2.Text = date2.Value.ToString("yyyy-MM-dd HH:mm:ss");
            txt_DT1.Text = date1.Value.AddHours(-date1.Value.Hour).AddMinutes(-date1.Value.Minute).ToString("yyyy-MM-dd HH:mm:ss");
            cpbh.Focus();
        }

        private void date1_ValueChanged(object sender, EventArgs e)
        {
            txt_DT1.Text = date1.Value.ToString("yyyy-MM-dd HH:mm:ss");
        }

        private void date2_ValueChanged(object sender, EventArgs e)
        {
            txt_DT2.Text = date2.Value.ToString("yyyy-MM-dd HH:mm:ss");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == System.Windows.Input.Key.Enter)
                {
                    string id = cpbh.Text.Trim() ;
                    string startDate = txt_DT1.Text.Trim();
                    string endDate = txt_DT2.Text.Trim();
                    if(startDate == null || endDate == null)
                    {
                        MessageBox.Show("开始日期和 结束日期不能为空");
                    }
                    string KFID = Global.forcerInfo.KFID;
                    Search(id,startDate,endDate, KFID);
                }
            }
            catch (Exception ew)
            {
                MessageBox.Show(ew.Message);
            }
        }

        private void Search(string id,string startDate,string enddate,string KFID)
        {

    
            List<HCInfo> hc = stBusiness.getHCInfo(id,startDate,enddate,KFID);
            hc.Sort((x, y) => x.Time.CompareTo(y.Time));
            if (hc == null)
            {
               // MessageBox.Show("数据查询为空");
                return;
            }
            LV.ItemsSource = hc;
            LV.Items.Refresh();
            cpbh.Text = null;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                string startDate = txt_DT1.Text.Trim();
                string endDate = txt_DT2.Text.Trim();

                string KFID = Global.forcerInfo.KFID;
                Search(null, startDate, endDate, KFID);
            }catch(Exception er)
            {
                MessageBox.Show(er.Message);
            }
        }

        private void Button_TouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                string startDate = txt_DT1.Text.Trim();
                string endDate = txt_DT2.Text.Trim();

                string KFID = Global.forcerInfo.KFID;
                Search(null, startDate, endDate, KFID);
            }catch(Exception er)
            {
                MessageBox.Show(er.Message);
            }
        }

        private void Button_TouchDown_1(object sender, TouchEventArgs e)
        {
            this.Close();
        }

        private void LV_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
