﻿using Business;
using Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace SmartRoom
{
    /// <summary>
    /// DetailWindow.xaml 的交互逻辑
    /// </summary>
    public partial class JJBCXWindow : Window
    {
        JJBBusiness _business = new JJBBusiness();
        BHBusiness _bhBusiness = new BHBusiness();
        List<CYInfo> billList = new List<CYInfo>();

        private System.Windows.Forms.Timer _tagTimer = new System.Windows.Forms.Timer();

        /// <summary>
        /// 0:表示码货 从主库出到临时库;1:表示退货 从临时库出到主库;
        /// </summary>
        public JJBCXWindow()
        {
            InitializeComponent();
            lblAppTitle.Content = "交接班查询";
        }


        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {

            string LastTime = _business.GetLastJJBRQ(Global.forcerInfo.KFID);

            date1.Value = DateTime.Now.AddHours(-8);
          
            txt_JBKS.Text = Global.forcerInfo.KfName;
            txt_JBR.Text = Global.User.RealName;

            if (!string.IsNullOrEmpty(LastTime))
            {
                date1.Value = Convert.ToDateTime(LastTime);
                //date1.Enabled = false;
            }
            lvw_RK.ItemsSource = billList;

            txt_DT2.Text = date2.Value.ToString("yyyy-MM-dd HH:mm:ss");
            txt_DT1.Text = date1.Value.ToString("yyyy-MM-dd HH:mm:ss");
        }




        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                string strException;
                Global.lockOpt.CloseDoor(out strException);
            }
            catch (Exception ex)
            { }
        }
  
        /// <summary>
        /// 查询 11
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_Search_Click(object sender, RoutedEventArgs e)
        {
           
            //判断是否是当前时间，如果是加一小时如果不是不加
            string datatime = DateTime.Now.ToString("yyyy-MM-dd HH");
            string datatime2 = date2.Value.ToString("yyyy-MM-dd HH");
            if (datatime2.Equals(datatime))
            {
                datatime = DateTime.Now.AddHours(+1).ToString("yyyy-MM-dd HH:mm:ss");
            }
            else
            { 
                datatime = date2.Value.ToString("yyyy-MM-dd HH:mm:ss");
            }

            WaitWindow wait = new WaitWindow("正在查询，请稍等...");
            try
            {
          
                billList = _business.GetJJBInfo(Global.forcerInfo.KFID, date1.Value.ToString("yyyy-MM-dd HH:mm:ss"), datatime);

                lvw_RK.ItemsSource = billList;
                lvw_RK.Items.Refresh();
                SetTotalSL();
            }catch(Exception ew)
            {

            }
            finally
            {
                wait.Close();
            }
         
        }

        private void txt_Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void txt_Submit_Click(object sender, RoutedEventArgs e)
        {
            //if (Global.IsTouch == 1) return;
            SubmitJJB();
        }

        private void txt_Submit_TouchDown(object sender, System.Windows.Input.TouchEventArgs e)
        {
            if (Global.IsTouch == 0) return;
            SubmitJJB();
        }

        void SubmitJJB()
        {
            try
            {
                
                if (billList == null || billList.Count == 0)
                {
                    Global.MessageWindow.ShowDialog("当前无可交接班数据，请查询后再试!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
                    return;
                }
                string ID = Guid.NewGuid().ToString();
                if (_business.SaveJJBInfo(ID, Global.forcerInfo.KFID, date1.Value.ToString("yyyy-MM-dd HH:mm:ss"), date2.Value.ToString("yyyy-MM-dd HH:mm:ss"), Global.User.UserID, Global.User.DepID, txt_Remark.Text, billList))
                {
                    Global.MessageWindow.ShowDialog("交接班成功!");
                }
                else
                {
                    Global.MessageWindow.ShowDialog("交接班失败，请联系管理员!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("交接班信息出现异常！" + ex.Message);
            }
        }


        void SetTotalSL()
        {
            int LYHJ = 0;
            int JFHJ = 0;
            int GHHJ = 0;
            int CYHJ = 0;
            int SHSL = 0;
            string shxx = string.Empty;
            foreach (var obj in billList)
            {
                LYHJ += obj.LYSL;
                JFHJ += obj.JFSL;
                GHHJ += obj.GHSL;
                CYHJ += obj.CYSL;
                SHSL += obj.SHSL;
                if (obj.SHSL > 0)
                {
                    shxx += obj.XMMC + ":损耗" + obj.SHSL+";";
                }
            }
            txt_Remark.Text = "总计费数量=" + JFHJ + "; 总领用数量=" + LYHJ + "; 总归还数量=" + GHHJ + "; 总差异数=" + CYHJ + ";总损耗数量:"+SHSL+"损耗明细--"+shxx;
        }


        private void date1_ValueChanged(object sender, EventArgs e)
        {
            txt_DT1.Text = date1.Value.ToString("yyyy-MM-dd HH:mm:ss");
        }

        private void date2_ValueChanged(object sender, EventArgs e)
        {
            txt_DT2.Text = date2.Value.ToString("yyyy-MM-dd HH:mm:ss");
        }

        private void lvw_RK_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {

            WaitWindow wait = new WaitWindow("正在查询，请稍等...");
            try
            {

                string XMDD = (lvw_RK.SelectedItem as CYInfo).XMDM;
                List<SYXXInfo> syxxList = _business.GetBRSYXXBYDate(Global.forcerInfo.KFID, date1.Value.ToString("yyyy-MM-dd HH:mm:ss"), date2.Value.ToString("yyyy-MM-dd HH:mm:ss"), XMDD);
                lvw_SYXX.ItemsSource = syxxList;
                lvw_SYXX.Items.Refresh();
             
            }
            catch (Exception ex)
            {

            }
            finally
            {
                wait.Close();
            }
        }

        private void Complete_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                TextBox Box = (sender as TextBox);
                CYInfo obj = Box.DataContext as CYInfo;

                SetTotalSL();
                int SHSL = 0;
                if (!int.TryParse(obj.SHSL.ToString(),out SHSL))
                {
                    Global.MessageWindow.ShowDialog("请输入合法的损耗数量!", Enum.MessageButton.Cancel, Enum.MessageStyle.Warn);

                    Box.Focus();
                    Box.SelectAll();
                }
            }
            catch (Exception ex)
            {
                Global.MessageWindow.ShowDialog("请输入合法的损耗数量!", Enum.MessageButton.Cancel, Enum.MessageStyle.Warn);
            }
        }

        private void txt_Close_TouchDown(object sender, System.Windows.Input.TouchEventArgs e)
        {
            try
            {
                if (Global.IsTouch == 0)
                    return;
                this.Close();
            }
            catch (Exception ex)
            { }
        }

      
    }
}
