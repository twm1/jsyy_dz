﻿using Business;
using Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using SmartRoom;
namespace SmartRoom
{
    /// <summary>
    /// DetailWindow.xaml 的交互逻辑
    /// </summary>
    public partial class InStockBillWindow : Window
    {
        InStockBillBusiness _business = new InStockBillBusiness();
        List<InStockBillDetail> billList = new List<InStockBillDetail>();
        List<InStockBill> lsBillList = new List<InStockBill>();


        public InStockBillWindow()
        {
            InitializeComponent();
            lblAppTitle.Content = "低值柜入库确认";
        }


        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            LoadData();
        }

        void LoadData()
        {
            try
            {
                lvw_RK.ItemsSource = billList;
                lvw_JJB.ItemsSource = lsBillList;
                lsBillList = _business.GetInStockBill(Global.forcerInfo.KFID);
                lvw_JJB.ItemsSource = lsBillList;
                lvw_JJB.Items.Refresh();
            }
            catch (Exception ex)
            { }
        }

        void SubmitData()
        {
            try
            {
                InStockBill obj = lvw_JJB.SelectedItem as InStockBill;
                obj.UserID = Global.User.UserID;
                string content = "";
                if (billList == null || billList.Count == 0)
                {
                    Global.MessageWindow.ShowDialog("当前没有可提交的数据，请核实!", Enum.MessageButton.Close, Enum.MessageStyle.Warn);
                    return;
                }
                content += obj.Bill_ID;
                if (Enum.MessageDialogResult.Yes == Global.MessageWindow.ShowDialog("请确认" + lblAppTitle.Content + "数据 \n" + content, Enum.MessageButton.YesNo, Enum.MessageStyle.Question))
                {
                    string result = _business.InsertZDRKInfo(obj);
                    if (result == "0")
                    {
                        Global.MessageWindow.ShowDialog(lblAppTitle.Content + "成功!");
                        this.Close();
                    }
                    else
                    {
                        Global.MessageWindow.ShowDialog(lblAppTitle.Content + "失败!" + result, Enum.MessageButton.Close, Enum.MessageStyle.Error);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }


        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                string strException;
                Global.lockOpt.CloseDoor(out strException);
            }
            catch (Exception ex)
            { }
        }


        private void btn_Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        private void btn_Refresh_Click(object sender, RoutedEventArgs e)
        {
            LoadData();
        }

        private void btn_Submit_Click(object sender, RoutedEventArgs e)
        {
            SubmitData();
        }

        private void lvw_JJB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            

          
        }

        private void lvw_JJB_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                InStockBill obj = lvw_JJB.SelectedItem as InStockBill;
                billList = _business.GetInStockBillDetail(obj.Bill_ID);
                lvw_RK.ItemsSource = billList;
                lvw_RK.Items.Refresh();
            }
            catch { }
        }

    }
}
