﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftTool
{
    public enum LogType
    {
        /// <summary>
        /// 错误
        /// </summary>
        Error=0,
        /// <summary>
        /// 警告
        /// </summary>
        Warn=1,
        /// <summary>
        /// 信息
        /// </summary>
        Message,
    }
}
