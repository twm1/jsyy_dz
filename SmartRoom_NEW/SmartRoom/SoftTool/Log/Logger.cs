﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SoftTool
{
    public static class Logger
    {
        public static string logPath = Environment.CurrentDirectory + "\\Assets\\logs\\";
        public static void WriteLog(string message)
        {
            WriteLog(message,LogType.Error);
        }

        public static void WriteLog(string message, LogType type)
        {
            try
            {
                string logFile = DateTime.Now.ToShortDateString().Replace(@"/", @"-").Replace(@"\", @"-") + ".log";
                if (!Directory.Exists(logPath))
                    Directory.CreateDirectory(logPath);

                using (StreamWriter sw = File.AppendText(logPath + logFile))
                {
                    sw.WriteLine("*********************************************************************");
                    sw.WriteLine("");
                    sw.WriteLine(DateTime.Now.ToString() + ":" + type.ToString());
                    sw.WriteLine(message);
                    sw.WriteLine("");
                    sw.WriteLine("*********************************************************************");
                }
            }
            catch { }
        }
    }
}