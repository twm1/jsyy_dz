﻿/* ==============================================================================
  *	Copyright (C) 2013 ***, ltd. All right reserved。
  * FileName:  ConfigManager.cs, based on .net framwork 4.0.30319.296
  * Author:  LuoGang
  * Create Date:  LuoGang create at  2013/4/9 9:32:08
  * Version :  1.0.0.0
  * Function:  配置文件操作类
  * Modify:  
  * ==============================================================================
*/
using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SoftTool
{
    public class ConfigManager
    {
        System.Configuration.Configuration config = null;
        public ConfigManager()
        {
            config = ConfigurationManager.OpenExeConfiguration(
            ConfigurationUserLevel.None);
        }

        /// <summary>
        /// 添加键值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void AddAppSetting(string key, string value)
        {
            config.AppSettings.Settings.Add(key, value);
            config.Save();
        }

        /// <summary>
        /// 修改键值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SaveAppSetting(string key, string value)
        {
            config.AppSettings.Settings.Remove(key);
            config.AppSettings.Settings.Add(key, value);
            config.Save();
        }

        /// <summary>
        /// 获得键值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetAppSetting(string key)
        {
            return config.AppSettings.Settings[key].Value;
        }

        /// <summary>
        /// 移除键值
        /// </summary>
        /// <param name="key"></param>
        public void DelAppSetting(string key)
        {
            config.AppSettings.Settings.Remove(key);
            config.Save();
        }
    }
}
