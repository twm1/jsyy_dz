﻿using Business;
using Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace SmartRoom
{
    /// <summary>
    /// OutWindowsDetail.xaml 的交互逻辑
    /// </summary>
    public partial class OutDetailWindow : Window
    {
        InBillInfo outBill = new InBillInfo();
        List<InBillDetailInfo> billList = new List<InBillDetailInfo>();

        StorageBusiness stBusiness = new StorageBusiness();
        InBillInfoBusiness outBusiness = new InBillInfoBusiness();
        BHBusiness bhBusiness = new BHBusiness();
        KFBusiness kFBusiness = new KFBusiness();

        /// <summary>
        /// 0:表示领用 从临时库出到正式库;1:表示归还 从正式库出到临时库;
        /// </summary>
        int Type = 0;

        /// <summary>
        /// 0:表示领用;1:表示归还;
        /// </summary>
        /// <param name="_type"></param>
        public OutDetailWindow(int _type)
        {
            InitializeComponent();
            Type = _type;
            if (Type == 0)
            {
                lblAppTitle.Content = "耗材领用";
                txt_RKKF.Text = Global.forcerInfo.SjKFName;
                txt_CKKF.Text = Global.forcerInfo.KfName;
            }
            else
            {
                lblAppTitle.Content = "耗材归还";

                txt_RKKF.Text = Global.forcerInfo.KfName;
                txt_CKKF.Text = Global.forcerInfo.SjKFName;
            }
            string strException;
            Global.lockOpt.OpenDoor(out strException);
        }


        private System.Windows.Forms.Timer _tagTimer = new System.Windows.Forms.Timer();

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            lvw_RK.ItemsSource = billList;
            if (Type == 0)//领用
            {
                outBill.CKKFID = Global.forcerInfo.KFID;
                outBill.RKKFID = Global.forcerInfo.SJKFID;
            }
            else//归还
            {
                outBill.CKKFID = Global.forcerInfo.SJKFID;
                outBill.RKKFID = Global.forcerInfo.KFID;
            }
            outBill.Remark = "低值耗材分隔区出库单";
            outBill.UserID = Global.User.UserID;
            outBill.UserName = Global.User.UserName;
            outBill.CKType = "出库";
            outBill.DetailList = billList;
            outBill.BillNo = bhBusiness.GetBH(Global.User.DepID, "C");
            outBill.DJID = Guid.NewGuid().ToString();
            outBill.DepID = Global.User.DepID;

            txt_CPTM.Focus();
        }


        private void btn_Exit_Click(object sender, RoutedEventArgs e)
        {
            if (Global.IsTouch == 1)
                return;
            this.Close();
        }


        /// <summary>
        /// 提交
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnC_Submit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string content = "";
                if (billList == null || billList.Count == 0)
                {
                    Global.MessageWindow.ShowDialog("当前没有可提交的数据，请核实!", Enum.MessageButton.Close, Enum.MessageStyle.Warn);
                    return;
                }
                foreach (var obj in billList)
                {
                    if (!obj.XZ || obj.SL == 0)
                        continue;
                    content += obj.CPMC + "---" + obj.SL + " \n";
                }
                if (string.IsNullOrEmpty(content))
                {
                    Global.MessageWindow.ShowDialog("当前没有可提交的数据，请核实!", Enum.MessageButton.Close, Enum.MessageStyle.Warn);
                    return;
                }
                if (Enum.MessageDialogResult.Yes == Global.MessageWindow.ShowDialog("请确认" + lblAppTitle.Content + "数据 \n" + content, Enum.MessageButton.YesNo, Enum.MessageStyle.Question))
                {
                    if (outBusiness.InsertCKInfo(outBill))
                    {
                        Global.MessageWindow.ShowDialog(lblAppTitle.Content + "成功!");
                        this.Close();
                    }
                    else
                    {
                        Global.MessageWindow.ShowDialog(lblAppTitle.Content + "失败!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }


        private void btn_Clear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lock (((ICollection)billList).SyncRoot)
                {
                    billList = null;
                    lvw_RK.Items.Clear();
                    lvw_RK.Items.Refresh();
                }
            }
            catch (Exception ex)
            {

            }
        }


        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                string strException;
                Global.lockOpt.CloseDoor(out strException);
            }
            catch (Exception ex)
            { }
        }

        private void Complete_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            try
            {
                TextBox Box = (sender as TextBox);
                InBillDetailInfo obj = Box.DataContext as InBillDetailInfo;
                SetTotalSL();
                if (obj.SL > obj.KCSL)
                {
                    Global.MessageWindow.ShowDialog("数量大于库存数量,请重新输入!", Enum.MessageButton.Cancel, Enum.MessageStyle.Warn);

                    Box.Focus();
                    Box.SelectAll();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private bool FindRow(StorageInfo obj)
        {
            bool result = false;
            //foreach (var item in billList)
            //{
            //    if (!item.XZ)
            //    {
            //        billList.Remove(item);
            //        continue;
            //    }
            //    if (obj.CPBH.Equals(item.CPBH))
            //    {
            //        if (item.KCSL > item.SL)
            //        {
            //            item.XZ = true;
            //            item.SL += 1;
            //            result = true;
            //            return result;
            //        }
            //    }
            //}

            for (int i = billList.Count; i >0; i--)
            {
                InBillDetailInfo item = billList[i - 1];

                //if (item.SL==0)
                //{
                //    billList.Remove(item);
                //    continue;
                //}
                if (obj.CPBH.Equals(item.CPBH))
                {
                    if (item.KCSL > item.SL)
                    {
                        item.XZ = true;
                        item.SL += 1;
                        result = true;
                        return result;
                    }
                    result = true;
                    return result;
                }

            }
            SetTotalSL();
            return result;
        }

        private bool FindRow(List<StorageInfo> list)
        {
            bool result = false;

            foreach (var obj in list)
            {
                for (int i = billList.Count; i > 0; i--)
                {
                    InBillDetailInfo item = billList[i - 1];

                    //if (item.SL==0)
                    //{
                    //    billList.Remove(item);
                    //    continue;
                    //}
                    if (obj.CPBH.Equals(item.CPBH))
                    {
                        //if (item.KCSL > item.SL)
                        //{
                        //    item.XZ = true;
                        //    item.SL += 1;
                        //    result = true;
                        //    return result;
                        //}
                        result = true;
                        return result;
                    }

                }
            }
            SetTotalSL();
            return result;
        }

        private void NewScan()
        {
            txt_CPGG.Text = string.Empty;
            txt_CPMC.Text = string.Empty;
            txt_CPPH.Text = string.Empty;
            txt_CPTM.Text = string.Empty;
            txt_CPXH.Text = string.Empty;
            txt_CPTM.Focus();
        }


        private void btn_Delete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Enum.MessageDialogResult.Yes == Global.MessageWindow.ShowDialog("确认删除当前选择的行?", Enum.MessageButton.YesNo, Enum.MessageStyle.Question))
                {
                    billList.RemoveAll(T => T.XZ == true);
                    lvw_RK.Items.Refresh();
                }
            }
            catch (Exception ex)
            {

            }
        }


        private void txt_Search_Click(object sender, RoutedEventArgs e)
        {
            StorageInfo cxtj = new StorageInfo();
            cxtj.CPName = txt_CPMC.Text.Trim();
            cxtj.GG = txt_CPGG.Text.Trim();
            cxtj.Batch = txt_CPPH.Text.Trim();
            cxtj.XH = txt_CPXH.Text.Trim();
            cxtj.CPBH = txt_CPTM.Text.Trim();
            cxtj.KFID = outBill.CKKFID;
            List<StorageInfo> list = stBusiness.GetStorageInfos(cxtj);
            if (!FindRow(list))
            {
                //查询对应的库存信息
               
                if (list != null && list.Count > 0)
                {
                    bool isXZ = true;
                    foreach (var item in list)
                    {
                        InBillDetailInfo obj = new InBillDetailInfo();
                        obj.XZ = isXZ;
                        obj.SL = 0;
                        //if (!isXZ)
                        //{
                        //    obj.SL = 0;
                        //}
                        obj.DJID = outBill.DJID;
                        obj.KCSL = item.SL;
                        obj.GG = item.GG;
                        obj.XH = item.XH;
                        obj.Batch = item.Batch;
                        obj.DJ = item.CKDJ;
                        obj.JE = item.CKDJ * obj.SL;
                        obj.CPMC = item.CPName;
                        obj.DW = item.DW;
                        obj.RKMXID = item.RKMXID;
                        obj.CPBH = item.CPBH;
                        obj.CPID = item.CPID;
                        obj.YXQ = item.YXQ;
                        obj.TagID = item.ID;
                        billList.Add(obj);
                        //isXZ = false;
                    }
                }
            }
            SetTotalSL();
            lvw_RK.Items.Refresh();
            NewScan();
        }

        private void txt_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                txt_Search_Click(null, null);
            }
        }

        private void Complete_Checked(object sender, RoutedEventArgs e)
        {
            SetTotalSL();
        }


        void SetTotalSL()
        {
            int Total = 0;
            int CZTotal = 0;
            foreach (var obj in billList)
            {
                Total += obj.KCSL;
                CZTotal += obj.SL;
            }
            lbl_Totl.Content = Total;
            lbl_LYTotl.Content = CZTotal;
        }

        private void lvw_RK_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
          
        }

        private void lvw_RK_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                if (Global.IsTouch == 1)
                    return;
                //InBillDetailInfo obj = lvw_RK.SelectedItem as InBillDetailInfo;
                //string message ="产品名称:"+ obj.CPMC+"\n";
                //message +="规格:"+obj.GG+ "\n";
                //message += "型号:" + obj.XH + "\n";
                //message += "库存数量:" + obj.KCSL;
                //InputNumberWindow _window = new InputNumberWindow();
                //_window.Message = message;
                //_window.Number = obj.SL;
                //_window.OldNumber = obj.KCSL;
                //_window.ShowDialog();
                //if (_window.DialogResult == true)
                //{
                //    obj.SL = _window.Number;
                //}
                //obj.XZ = !obj.XZ;
                //lvw_RK.Items.Refresh();
                //txt_CPTM.Focus();
            }
            catch { }
        }

        private void lvw_RK_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            try
            {
                return;
                if (e.Key == System.Windows.Input.Key.Enter)
                {
                    InBillDetailInfo obj = lvw_RK.SelectedItem as InBillDetailInfo;
                    //obj.XZ = !obj.XZ;
                    string message = "产品名称:" + obj.CPMC + "\n";
                    message += "规格:" + obj.GG + "\n";
                    message += "型号:" + obj.XH + "\n";
                    message += "库存数量:" + obj.KCSL;
                    InputNumberWindow _window = new InputNumberWindow();
                    _window.Message = message;
                    _window.Number = obj.SL;
                    _window.OldNumber = obj.KCSL;
                    _window.ShowDialog();
                    if (_window.DialogResult == true)
                    {
                        obj.SL = _window.Number;
                    }
                    lvw_RK.Items.Refresh();
                }
            }
            catch (Exception ex)
            { }
        }

        private void lvw_RK_TouchDown(object sender, System.Windows.Input.TouchEventArgs e)
        {
            return;
            try
            {
                if (Global.IsTouch == 0)
                    return;
                InBillDetailInfo obj = lvw_RK.SelectedItem as InBillDetailInfo;        
                lvw_RK.Items.Refresh();
                txt_CPTM.Focus();
            }
            catch { }
        }

        private void btn_Exit_TouchDown(object sender, System.Windows.Input.TouchEventArgs e)
        {
            if (Global.IsTouch == 0) return;
            this.Close();
        }
    }
}
