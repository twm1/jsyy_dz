﻿using Business;
using Entity;
using SoftTool;
using System;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;

namespace SmartRoom
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {

        System.Windows.Forms.Timer timer1 = null;
        int AutoExitTimer = 300;//默认5分钟

        protected override void OnStartup(StartupEventArgs e)
        {

            //InputNumberWindow a = new InputNumberWindow();
            //a.ShowDialog();
            //return;
            //string message = @"2019年5月22日-5月31日：文峰、水南、乌江、丁江、白水";
            //MessageWindow me = new MessageWindow();
         
            //me.ShowDialog(message);
            //return;
            //LoginWindows loginWindows1 = new LoginWindows();
            //loginWindows1.ShowDialog();
            //return;

            try
            {
                Engine.InitEngines();//初始化人脸识别引擎
                AutoExitTimer = Convert.ToInt32(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["AutoExitTimer"].Value);
            }
            catch { }
            //启用定时器
            if (timer1 == null)
            {
                timer1 = new System.Windows.Forms.Timer();
            }
            timer1.Interval =5000;
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Start();


            string appName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name.ToString();
            int processCount = Process.GetProcessesByName(appName).Where(o => o.Id != Process.GetCurrentProcess().Id).Count();
            if (processCount > 0)
            {
                MessageBox.Show("当前程序已经在运行，不能重复启动！");
                Application.Current.Shutdown();
            }
            else
            {
                string macAddress = DeviceInfo.GetMacAddress();//GetCpuID();
                ForcerInfoBusiness forcerInfoBusiness = new ForcerInfoBusiness();
                ForcerInfo obj = forcerInfoBusiness.CheckForcerIsRegiser(macAddress);
                if (obj == null)
                {
                    CheckLisenseWindow checkLisenseWindow = new CheckLisenseWindow();
                    checkLisenseWindow.ShowDialog();
                }
                else if (!obj.IsDisable)
                {
                    Global.MessageWindow.ShowDialog("当前设备尚未开通，请联系管理员！",Enum.MessageButton.Close,Enum.MessageStyle.Warn);                    
                }
                else
                {
                    Global.forcerInfo = obj;
                    LoginWindows loginWindows = new LoginWindows();
                    loginWindows.ShowDialog();
                }
                Application.Current.Shutdown();
                //base.OnStartup(e);
            }
        }

        protected override void OnExit(ExitEventArgs e)
        {
            try
            {
                if (!timer1.Enabled)
                    timer1.Stop();
                if (timer1 != null)
                {
                    timer1.Dispose();
                    timer1 = null;
                }
            }
            catch (Exception ex)
            { }
        }

        private  void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                //判断空闲时间是否超过15分钟,超过则自动弹出视频播放窗口
                if (GetIdleTick() / 1000 >= AutoExitTimer)
                {
                    foreach (Window obj in Global.WindowsList)
                        obj.Close();
                }
            }
            catch (Exception ex)
            { }
        }

        /// <summary>
        /// 获取鼠标键盘空闲时间
        /// </summary>
        /// <returns></returns>
        public  long GetIdleTick()
        {
            if (Global.SystemIdelTime == 0)
            {
                Global.SystemIdelTime = 1;
                return 0;
            }
            LASTINPUTINFO lastInputInfo = new LASTINPUTINFO();
            lastInputInfo.cbSize = Marshal.SizeOf(lastInputInfo);
            if (!GetLastInputInfo(ref lastInputInfo)) return 0;
            return Global.SystemIdelTime=Environment.TickCount - (long)lastInputInfo.dwTime;
        }


        [StructLayout(LayoutKind.Sequential)]
        private struct LASTINPUTINFO
        {
            [MarshalAs(UnmanagedType.U4)]
            public int cbSize;
            [MarshalAs(UnmanagedType.U4)]
            public uint dwTime;
        }
        /// <summary>
        /// 调用windows API获取鼠标键盘空闲时间
        /// </summary>
        /// <param name="plii"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        private static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);

    }
}