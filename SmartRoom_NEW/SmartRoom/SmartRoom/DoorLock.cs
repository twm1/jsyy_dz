﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartRoom
{
    public class DoorLock
    {
        private SerialPort iSerialPort;

        public DoorLock()
        {
            iSerialPort = new SerialPort();
            iSerialPort.DataReceived += new SerialDataReceivedEventHandler(ReceivedComData);
        }


        /// <summary>
        /// 关闭串口
        /// </summary>
        /// <returns></returns>
        public bool CloseCom()
        {
            bool result = false;
            if (iSerialPort.IsOpen)
                iSerialPort.Close();
            return result;
        }

        /// <summary>
        /// 打开控制门锁的串口
        /// </summary>
        /// <param name="strPort"></param>
        /// <param name="nBaudrate"></param>
        /// <param name="strException"></param>
        /// <returns></returns>
        public int OpenCom(out string strException)
        {
            strException = string.Empty;

            if (iSerialPort.IsOpen)
            {
                iSerialPort.Close();
            }

            try
            {
                iSerialPort.PortName = Global.LockPort;
                iSerialPort.BaudRate = Global.LockBaud;
                iSerialPort.ReadTimeout = 200;
                iSerialPort.Open();
            }
            catch (System.Exception ex)
            {
                strException = ex.Message;
                return -1;
            }
            return 0;
        }

        public void OpenDoor(out string strException)
        {
            strException = string.Empty;
            if (!iSerialPort.IsOpen)
                OpenCom(out strException);

            SendMessage(OpenDoorCmd());
        }

        public void CloseDoor(out string strException)
        {
            strException = string.Empty;
            if (!iSerialPort.IsOpen)
                OpenCom(out strException);

            SendMessage(CloseDoorCmd());
        }

        public byte[] OpenDoorCmd()
        {
            byte[] src = new byte[8];
            src[0] = 0x55;
            src[1] = 0x56;
            src[2] = 0x00;
            src[3] = 0x00;
            src[4] = 0x00;
            src[5] = 0x01;
            src[6] = 0x01;
            src[7] = 0xAD;
            return src;
        }

        public byte[] CloseDoorCmd()
        {
            byte[] src = new byte[8];
            src[0] = 0x55;
            src[1] = 0x56;
            src[2] = 0x00;
            src[3] = 0x00;
            src[4] = 0x00;
            src[5] = 0x01;
            src[6] = 0x02;
            src[7] = 0xAE;
            return src;
        }

        /// <summary>
        /// 接收串口响应数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReceivedComData(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                int nCount = iSerialPort.BytesToRead;
                if (nCount == 0)
                {
                    return;
                }

                byte[] btAryBuffer = new byte[nCount];
                iSerialPort.Read(btAryBuffer, 0, nCount);

            }
            catch (System.Exception ex)
            {

            }
        }

        /// <summary>
        /// 向串口发送数据
        /// </summary>
        /// <param name="btArySenderData"></param>
        /// <returns></returns>
        public int SendMessage(byte[] btArySenderData)
        {
            //串口连接方式

            if (!iSerialPort.IsOpen)
            {
                return -1;
            }

            iSerialPort.Write(btArySenderData, 0, btArySenderData.Length);
            return 0;
        }

        public int SendMessage(string btArySenderData)
        {
            //串口连接方式

            if (!iSerialPort.IsOpen)
            {
                return -1;
            }

            iSerialPort.Write(btArySenderData);
            return 0;
        }
    }
}
