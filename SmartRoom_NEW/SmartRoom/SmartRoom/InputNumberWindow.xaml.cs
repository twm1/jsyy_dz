﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SmartRoom
{
    /// <summary>
    /// InputNumberWindow.xaml 的交互逻辑
    /// </summary>
    public partial class InputNumberWindow : Window
    {
        public int Number
        {
            get
            {
                int vNumber = 0;
                int.TryParse(TextBox_Number.Text, out vNumber);
                return vNumber;
            }
            set { TextBox_Number.Text = value.ToString(); }
        }

        public int OldNumber
        {
            get;set;
        }
        public string Message
        {
            get { return txt_Message.Text; }
            set { txt_Message.Text = value; }
        }
        public InputNumberWindow()
        {
            InitializeComponent();
           
        }


        private void Button_QueRen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int vNumber = 0;
                int.TryParse(TextBox_Number.Text, out vNumber);
                Number = vNumber;

                if (OldNumber > 0 && Number > OldNumber)
                {
                    MessageBox.Show("库存数不能大于操作数!");
                    TextBox_Number.Text = OldNumber.ToString();
                    TextBox_Number.Focus();
                    return;
                }
                DialogResult = true;
                Close();
            }
            catch (Exception ex)
            {

            }
        }

        private void Button_QuXiao_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            TextBox_Number.Focus();
            TextBox_Number.SelectAll();
        }

        private void TextBox_Number_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Button_QueRen_Click(null,null);
            }
        }
    }
}
