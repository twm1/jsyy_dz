﻿using DeviceTool;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SmartRoom
{
    /// <summary>
    /// 电控门锁 通过中控板控制门开门关
    /// </summary>
    public class LockOperator
    {
        public string _Port;
        public string _Address="254";
        public int _BaudRat;
        /// <summary>
        /// 用来控制锁的串口类
        /// </summary>
        SerialPort _lockPort = null;

        private bool _isOpen;

        private List<DoorInfo> listDoor;

        int SendTimeCount = 0;

        /// <summary>
        /// 读取事件
        /// </summary>
        public event EventHandler<List<DoorInfo>> ReadTag;

        public virtual void OnTagReport(List<DoorInfo> arg)
        {
            if (ReadTag != null)
            {
                ReadTag.BeginInvoke(this, arg, null, null);
            }
        }

        public List<DoorInfo> ListDoor
        {
            get { return listDoor; }
            set { listDoor = value; }
        }
        /// <summary>
        /// 串口是否打开
        /// </summary>
        public bool IsOpen
        {
            get {
                if (_lockPort == null)
                    return false;
                _isOpen = _lockPort.IsOpen;
                return _isOpen; 
            }
        }

        /// <summary>
        /// 锁状态监控
        /// </summary>
        System.Windows.Threading.DispatcherTimer _lockStateTimer = null;
        /// <summary>
        /// 连接锁
        /// </summary>
        /// <returns></returns>
        public bool ConnectLock()
        {
            if (_lockPort!=null && _lockPort.IsOpen)
            {
                return true;
            }
            _lockPort = new SerialPort(Global.LockPort);
            _lockPort.BaudRate = Convert.ToInt32(Global.LockBaud);
            _lockPort.DataBits = 8;
            _lockPort.Open();
            if (!_lockPort.IsOpen)
            {
                return false;
            }

            ////检测线程
            //_lockStateTimer = null;
            //_lockStateTimer = new System.Windows.Threading.DispatcherTimer();
            //_lockStateTimer.Tick += new EventHandler(lockTime);
            //_lockStateTimer.Interval = new TimeSpan(0, 0, 0, 1);
            //_lockStateTimer.Start();
            return true;
        }

        public bool CloseCom()
        {
            if (_lockPort != null && !_lockPort.IsOpen)
            {
                return true;
            }
            _lockPort.Close();
       
            return true;
        }

        /// <summary>
        /// 验证门控锁
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lockTime(object sender, EventArgs e)
        {
            try
            {
                _lockStateTimer.Stop();
                string strMessage = string.Empty;
                if (_lockPort != null && _lockPort.IsOpen)
                {
                    byte[] info = CModbusDll.ReadDI(Convert.ToInt16(_Address), 4);
                    byte[] rst = sendinfo(info);
                    if (rst != null)
                    {
                        listDoor = new List<DoorInfo>();


                        DoorInfo A = new DoorInfo();
                        A.DoorName = "A柜门";
                        A.IsClose =  ((rst[0] & 0x01) == 0x00) ? false : true;
                        listDoor.Add(A);


                        DoorInfo B = new DoorInfo();
                        B.DoorName = "B柜门";
                        B.IsClose = true;// ((rst[0] & 0x02) == 0x00) ? false : true;
                        listDoor.Add(B);
                     

                        DoorInfo C = new DoorInfo();
                        C.DoorName = "C柜门";
                        C.IsClose = true;// ((rst[0] & 0x04) == 0x00) ? false : true;
                        listDoor.Add(C);


                        DoorInfo D = new DoorInfo();
                        D.DoorName = "D柜门";
                        D.IsClose = true;// ((rst[0] & 0x08) == 0x00) ? false : true;
                        listDoor.Add(D);

                        if (A.IsClose && B.IsClose && C.IsClose && D.IsClose)
                        {
                            OnTagReport(listDoor);
                        }
                    }

                    if (listDoor != null)
                    {
                        var obj = listDoor.Where(T => T.IsClose == false).FirstOrDefault();
                        if (obj != null)
                            SendTimeCount++;
                    }

                }
                else
                {
                    //_lockPort.Open();
                    ConnectLock();
                }
            }
            catch (Exception ex)
            {
               
            }
            finally
            {
                _lockStateTimer.Start();
            }
        }

        /// <summary>
        /// 指令包装
        /// </summary>
        /// <param name="src"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        private byte[] analysisRcv(byte[] src, int len)
        {
            if (len < 6) return null;
            if (src[0] != Convert.ToInt16(_Address)) return null;

            switch (src[1])
            {
                case 0x01:
                    if (CMBRTU.CalculateCrc(src, 6) == 0x00)
                    {
                        byte[] dst = new byte[1];
                        dst[0] = src[3];
                        return dst;
                    }
                    break;
                case 0x02:
                    if (CMBRTU.CalculateCrc(src, src[2] + 5) == 0x00)
                    {
                        byte[] dst = new byte[src[2]];
                        for (int i = 0; i < src[2]; i++)
                            dst[i] = src[3 + i];
                        return dst;
                    }
                    break;
                case 0x05:
                    if (CMBRTU.CalculateCrc(src, 8) == 0x00)
                    {
                        byte[] dst = new byte[1];
                        dst[0] = src[4];
                        return dst;
                    }
                    break;
                case 0x0f:
                    if (CMBRTU.CalculateCrc(src, 8) == 0x00)
                    {
                        byte[] dst = new byte[1];
                        dst[0] = 1;
                        return dst;
                    }
                    break;
            }
            return null;
        }

        /// <summary>
        /// 发送信息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        private byte[] sendinfo(byte[] info)
        {
            try
            {
                if (_lockPort==null || !_lockPort.IsOpen)
                {
                    ConnectLock();
                }
                _lockPort.WriteTimeout = 1000;
                _lockPort.Write(info, 0, info.Length);

                byte[] data = new byte[2048];
                _lockPort.ReadTimeout = 2000;
                int len = _lockPort.Read(data, 0, data.Length);


                return analysisRcv(data, len);
            }
            catch (Exception ex)
            {
                _lockPort.Close();
            }
            return null;
        }

        /// <summary>
        /// 快速打开门
        /// </summary>
        /// <param name="io"></param>
        public void QuickOpen(int[] ios,int time)
        {
            try
            {
                foreach (int io in ios)
                {
                    byte[] info = CModbusDll.QuickOpen(Convert.ToInt16(_Address), io, time);
                    byte[] rst = sendinfo(info);
                    Thread.Sleep(100);
                }
            }
            catch (Exception ex)
            {
                //AddListItems("打开柜门出现异常！" + ex.Message);
            }
        }

        /// <summary>
        /// 快速打开门
        /// </summary>
        /// <param name="io"></param>
        public void QuickOpen(int[] ios)
        {
            try
            {

                foreach (int io in ios)
                {
                    byte[] info = CModbusDll.QuickOpen(Convert.ToInt16(_Address), io, 5);
                    byte[] rst = sendinfo(info);
                    Thread.Sleep(100);
                }
            }
            catch (Exception ex)
            {
                //AddListItems("打开柜门出现异常！" + ex.Message);
            }
        }

        /// <summary>
        /// 打开或者关闭柜门
        /// </summary>
        /// <param name="ios"></param>
        /// <param name="isOpen">True为打开,False为关闭</param>
        public void OpenOrClose(int[] ios,bool isOpen)
        {
            foreach (int io in ios)
            {
                byte[] info = CModbusDll.WriteDO(Convert.ToInt16(_Address), io,isOpen);
                byte[] rst = sendinfo(info);
                Thread.Sleep(100);
            }
        }
    }

    public class DoorInfo
    {
        private string _doorName;

        public string DoorName
        {
            get { return _doorName; }
            set { _doorName = value; }
        }

        private bool _isClose;

        public bool IsClose
        {
            get { return _isClose; }
            set { _isClose = value; }
        }
    }
}
