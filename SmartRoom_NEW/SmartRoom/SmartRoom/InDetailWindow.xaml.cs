﻿using Business;
using Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace SmartRoom
{
    /// <summary>
    /// DetailWindow.xaml 的交互逻辑
    /// </summary>
    public partial class InDetailWindow : Window
    {
        InBillInfo inBill = new InBillInfo();
        List<InBillDetailInfo> billList = new List<InBillDetailInfo>();

        StorageBusiness stBusiness = new StorageBusiness();
        InBillInfoBusiness inBusiness = new InBillInfoBusiness();
        BHBusiness bhBusiness = new BHBusiness();
        KFBusiness kFBusiness = new KFBusiness();
        KFInfo kfinfo = null;
        /// <summary>
        /// 0:表示码货 从主库出到临时库;1:表示退货 从临时库出到主库;
        /// </summary>
        int Type = 0;

        private System.Windows.Forms.Timer _tagTimer = new System.Windows.Forms.Timer();

        /// <summary>
        /// 0:表示码货 从主库出到临时库;1:表示退货 从临时库出到主库;
        /// </summary>
        public InDetailWindow(int _type)
        {
            InitializeComponent();
            Type = _type;
            kfinfo = kFBusiness.GetZKKFInfo(Global.User.DepID);
            if (_type == 0)
            {
                lblAppTitle.Content = "耗材码货";
                txt_CKKF.Text = kfinfo.KFMC;
                txt_RKKF.Text = Global.forcerInfo.KfName;
            }
            else
            {
                lblAppTitle.Content = "耗材退货";
                txt_CKKF.Text = Global.forcerInfo.KfName;
                txt_RKKF.Text = kfinfo.KFMC;
            }
           
            string strException;
            Global.lockOpt.OpenDoor(out strException);
        }


        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            if (Type == 0)
            {
                inBill.CKKFID = kfinfo.KFID;
                inBill.RKKFID = Global.forcerInfo.KFID;
            }
            else
            {
                inBill.CKKFID = Global.forcerInfo.KFID;
                inBill.RKKFID = kfinfo.KFID;
            }
            lvw_RK.ItemsSource = billList;
            
            inBill.Remark = "低值耗材分隔区出库单";
            inBill.UserID = Global.User.UserID;
            inBill.UserName = Global.User.UserName;
            inBill.CKType = "出库";
            inBill.DetailList = billList;
            inBill.BillNo = bhBusiness.GetBH(Global.User.DepID,"C") ;
            inBill.DJID = Guid.NewGuid().ToString();
            inBill.DepID = Global.User.DepID;
            txt_CPTM.Focus();
        }


        private void btn_Exit_Click(object sender, RoutedEventArgs e)
        {
            if (Global.IsTouch == 1) return;
            this.Close();
        }


        /// <summary>
        /// 提交
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnC_Submit_Click(object sender, RoutedEventArgs e)
        {           
            try
            {
                string content = "";
                if (billList == null || billList.Count == 0)
                {
                    Global.MessageWindow.ShowDialog("当前没有可提交的数据，请核实!", Enum.MessageButton.Close, Enum.MessageStyle.Warn);
                    return;
                }
                foreach (var obj in billList)
                {
                    if (!obj.XZ || obj.SL == 0)
                        continue;
                    content += obj.CPMC+"---"+obj.SL+" \n";
                }
                if (string.IsNullOrEmpty(content))
                {
                    Global.MessageWindow.ShowDialog("当前没有可提交的数据，请核实!", Enum.MessageButton.Close, Enum.MessageStyle.Warn);
                    return;
                }

                if (Enum.MessageDialogResult.Yes == Global.MessageWindow.ShowDialog("请确认"+lblAppTitle.Content+"数据 \n"+content, Enum.MessageButton.YesNo, Enum.MessageStyle.Question))
                {
                    if (inBusiness.InsertCKInfo(inBill))
                    {
                        Global.MessageWindow.ShowDialog("码货成功!", Enum.MessageButton.Close, Enum.MessageStyle.Success);
                        this.Close();
                    }
                    else
                    {
                        Global.MessageWindow.ShowDialog("操作失败!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        
        private void btn_Clear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lock (((ICollection)billList).SyncRoot)
                {
                    billList.Clear();
                    lvw_RK.Items.Clear();
                    lvw_RK.Items.Refresh();
                }
            }
            catch (Exception ex)
            { 
            
            }
        }


        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                string strException;
                Global.lockOpt.CloseDoor(out strException);
            }
            catch(Exception ex) 
            { }
        }

        private void Complete_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            try
            {
                TextBox Box = (sender as TextBox);
                InBillDetailInfo obj = Box.DataContext as InBillDetailInfo;

                SetTotalSL();
                if (obj.SL > obj.KCSL)
                {
                    Global.MessageWindow.ShowDialog("数量大于库存数量,请重新输入!",Enum.MessageButton.Cancel,Enum.MessageStyle.Warn);
                    
                    Box.Focus();
                    Box.SelectAll();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private bool FindRow(StorageInfo obj)
        {
            bool result = false;
            //foreach (var item in billList)
            //{
            //    if (!item.XZ)
            //        billList.Remove(item);
            //    if (obj.CPBH.Equals(item.CPBH))
            //    {
            //        if (item.KCSL > item.SL)
            //        {
            //            item.XZ = true;
            //            item.SL += 1;
            //            result = true;
            //            return result;
            //        }
            //    }
            //}
            for (int i = billList.Count; i > 0; i--)
            {
                InBillDetailInfo item = billList[i - 1];

                //if (item.SL==0)
                //{
                //    billList.Remove(item);
                //    continue;
                //}
                if (obj.CPBH.Equals(item.CPBH))
                {
                    if (item.KCSL > item.SL)
                    {
                        item.XZ = true;
                        item.SL += 1;
                        result = true;
                        return result;
                    }
                    result = true;
                    return result;
                }

            }
            SetTotalSL();
            return result;
        }

        private bool FindRow(List<StorageInfo> list)
        {
            bool result = false;

            foreach (var obj in list)
            {
                for (int i = billList.Count; i > 0; i--)
                {
                    InBillDetailInfo item = billList[i - 1];

                    //if (item.SL==0)
                    //{
                    //    billList.Remove(item);
                    //    continue;
                    //}
                    if (obj.CPBH.Equals(item.CPBH))
                    {
                        //if (item.KCSL > item.SL)
                        //{
                        //    item.XZ = true;
                        //    item.SL += 1;
                        //    result = true;
                        //    return result;
                        //}
                        result = true;
                        return result;
                    }

                }
            }
            SetTotalSL();
            return result;
        }

        private void NewScan()
        {
            txt_CPGG.Text = string.Empty;
            txt_CPMC.Text = string.Empty;
            txt_CPPH.Text = string.Empty;
            txt_CPTM.Text = string.Empty;
            txt_CPXH.Text = string.Empty;
            txt_CPTM.Focus();
        }


        private void btn_Delete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Enum.MessageDialogResult.Yes == Global.MessageWindow.ShowDialog("确认删除所选择的行?", Enum.MessageButton.YesNo, Enum.MessageStyle.Question))
                {
                    billList.RemoveAll(T => T.XZ == true);
                    lvw_RK.Items.Refresh();
                }
            }
            catch (Exception ex)
            {

            }
        }


        private void txt_Search_Click(object sender, RoutedEventArgs e)
        {
            StorageInfo cxtj = new StorageInfo();
            cxtj.CPName = txt_CPMC.Text.Trim();
            cxtj.GG = txt_CPGG.Text.Trim();
            cxtj.Batch = txt_CPPH.Text.Trim();
            cxtj.XH = txt_CPXH.Text.Trim();
            cxtj.CPBH = txt_CPTM.Text.Trim();
            cxtj.KFID =inBill.CKKFID;
            //查询对应的库存信息
            List<StorageInfo> list = stBusiness.GetStorageInfos(cxtj);
            if (!FindRow(list))
            {
               
                if (list != null && list.Count > 0)
                {
                    bool isXZ = true;
                    foreach (var item in list)
                    {
                        InBillDetailInfo obj = new InBillDetailInfo();
                        obj.XZ = isXZ;
                        obj.SL = 0;
                        //if (!isXZ)
                        //{
                        //    obj.SL = 0;
                        //}
                        obj.DJID = inBill.DJID;
                        obj.KCSL = item.SL;
                        obj.GG = item.GG;
                        obj.XH = item.XH;
                        obj.Batch = item.Batch;
                        obj.DJ = item.CKDJ;
                        obj.JE = item.CKDJ * obj.SL;
                        obj.CPMC = item.CPName;
                        obj.DW = item.DW;
                        obj.RKMXID = item.RKMXID;
                        obj.CPBH = item.CPBH;
                        obj.CPID = item.CPID;
                        obj.YXQ = item.YXQ;
                        obj.TagID = item.ID;
                        billList.Add(obj);
                        //isXZ = false;
                    }
                }
            }
            SetTotalSL();
            lvw_RK.Items.Refresh();
            NewScan();
        }

        private void txt_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                txt_Search_Click(null,null);
            }
        }

        private void Complete_Checked(object sender, RoutedEventArgs e)
        {
            SetTotalSL();
        }

        void SetTotalSL()
        {
            try
            {
                int Total = 0;
                int CZTotal = 0;
                foreach (var obj in billList)
                {
                    Total += obj.KCSL;
                    CZTotal += obj.SL;
                }
                lbl_Totl.Content = Total;
                lbl_LYTotl.Content = CZTotal;
            }
            catch (Exception ex)
            { }
        }


        private void lvw_RK_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                if (Global.IsTouch == 1)
                    return;
                //InBillDetailInfo obj = lvw_RK.SelectedItem as InBillDetailInfo;
                ////obj.XZ = !obj.XZ;
                //string message = "产品名称:" + obj.CPMC + "\n";
                //message += "规格:" + obj.GG + "\n";
                //message += "型号:" + obj.XH + "\n";
                //message += "库存数量:" + obj.KCSL;
                //InputNumberWindow _window = new InputNumberWindow();
                //_window.Message = message;
                //_window.Number = obj.SL;
                //_window.OldNumber = obj.KCSL;
                //_window.ShowDialog();
                //if (_window.DialogResult == true)
                //{
                //    obj.SL = _window.Number;
                //}
                //lvw_RK.Items.Refresh();
                //txt_CPTM.Focus();
            }
            catch { }
        }

        private void lvw_RK_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            try
            {
                return;
                if (e.Key == System.Windows.Input.Key.Enter)
                {
                    InBillDetailInfo obj = lvw_RK.SelectedItem as InBillDetailInfo;
                    //obj.XZ = !obj.XZ;
                    string message = "产品名称:" + obj.CPMC + "\n";
                    message += "规格:" + obj.GG + "\n";
                    message += "型号:" + obj.XH + "\n";
                    message += "库存数量:" + obj.KCSL;
                    InputNumberWindow _window = new InputNumberWindow();
                    _window.Message = message;
                    _window.Number = obj.SL;
                    _window.OldNumber = obj.KCSL;
                    _window.ShowDialog();
                    if (_window.DialogResult == true)
                    {
                        obj.SL = _window.Number;
                    }
                    lvw_RK.Items.Refresh();
                }
            }
            catch (Exception ex)
            { }
        }

        private void lvw_RK_TouchDown(object sender, System.Windows.Input.TouchEventArgs e)
        {
            return;
            try
            {
                if (Global.IsTouch == 0)
                    return;
                InBillDetailInfo obj = lvw_RK.SelectedItem as InBillDetailInfo;
                lvw_RK.Items.Refresh();
                txt_CPTM.Focus();
            }
            catch { }
        }

        private void btn_Exit_TouchDown(object sender, System.Windows.Input.TouchEventArgs e)
        {
            try
            {
                if (Global.IsTouch == 0)
                    return;
                this.Close();
            }
            catch (Exception ex)
            { }
        }
    }
}
