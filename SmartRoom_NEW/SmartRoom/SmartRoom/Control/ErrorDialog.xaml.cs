﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SmartRoom
{
    /// <summary>
    /// ErrorDialog.xaml 的交互逻辑
    /// </summary>
    public partial class ErrorDialog : UserControl
    {
        public ErrorDialog()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
        }

        #region Message
        public string ErrMessage
        {
            get { return (string)GetValue(MessageProperty); }
            set { SetValue(MessageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Message.
        // This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MessageProperty =
               DependencyProperty.Register(
                  "ErrMessage", typeof(string), typeof(ErrorDialog), new UIPropertyMetadata(string.Empty));

        #endregion

        public void ShowHandlerDialog(string message)
        {
            try
            {
                ErrMessage += message;
                Visibility = Visibility.Visible;
            }
            catch
            {

            }
        }

        private void HideHandlerDialog()
        {
            Visibility = Visibility.Hidden;
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            HideHandlerDialog();
            ErrMessage = "";
        }
    }
}
