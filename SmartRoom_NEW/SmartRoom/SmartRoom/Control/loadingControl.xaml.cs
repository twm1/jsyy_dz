﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading.Tasks;

namespace SmartRoom
{
    /// <summary>
    /// loadingControl.xaml 的交互逻辑
    /// </summary>
    public partial class loadingControl : UserControl
    {
        public loadingControl()
        {
            InitializeComponent();
        }

        public event EventHandler<CompleteArgs> Completed;

        public void DoWork(Func<object, CompleteArgs> function, object args)
        {
            prloading.IsActive = true;
            Task<CompleteArgs> task = new Task<CompleteArgs>(function, args);
            task.Start();
            task.ContinueWith(t => {
                OnComplete(t.Result);
            });           
        }

        private void OnComplete(CompleteArgs args)
        {
            EventHandler<CompleteArgs> tComplet = Completed;
            if (Completed != null)
            {
                Completed(this, args);
                this.Dispatcher.Invoke(new Action(() => { prloading.IsActive = false; }));
            }
        }
    }
    public class CompleteArgs : EventArgs
    {
        public Status userstate { get; set; }
        public object Result { get; set; }
        public string Msg { get; set; }
        public CompleteArgs() { }
        public CompleteArgs(string msg, object result, Status usrState)
        {
            this.userstate = usrState;
            this.Msg = msg;
            this.Result = result;
        }
    
    }
}
