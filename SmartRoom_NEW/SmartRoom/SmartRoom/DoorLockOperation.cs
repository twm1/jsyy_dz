﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartRoom
{
    public class DoorLockOperation
    {
        /// <summary>
        /// 平移门
        /// </summary>
        DoorLock _doorLock = new DoorLock();
        /// <summary>
        /// 电控锁
        /// </summary>
        LockOperator _lockOperator = new LockOperator();

        public DoorLockOperation()
        {

        }

        /// <summary>
        /// 开门
        /// </summary>
        /// <param name="Message"></param>
        /// <returns></returns>
        public int OpenDoor(out string Message)
        {
            Message = string.Empty;
            int result = 0;
            switch (Global.LockType)
            {
                case 0:
                    _doorLock.OpenDoor(out Message);
                    break;
                case 1:
                    _lockOperator.QuickOpen(new int[] { 0 });
                    break;
                case 2:
                    _lockOperator.QuickOpen(new int[] { 0 }, Global.LockTime);
                    break;
            }
            return result;
        }

        /// <summary>
        /// 关门
        /// </summary>
        /// <param name="Message"></param>
        /// <returns></returns>
        public int CloseDoor(out string Message)
        {
            Message = string.Empty;
            int result = 0;
            switch (Global.LockType)
            {
                case 0:
                    _doorLock.CloseDoor(out Message);
                    break;
                case 1:
                    break;
                case 2:
                    break;
            }
            return result;
        }

        private bool doorIsClose;

        public bool CheckDoorIsClose()
        {
            bool isOpen = false;
            if (_lockOperator.ListDoor == null || _lockOperator.ListDoor.Count == 0)
                return isOpen;

            foreach (DoorInfo obj in _lockOperator.ListDoor)
            {
                if (!obj.IsClose)
                {
                    isOpen = obj.IsClose;
                    break;
                }
            }
            return isOpen;

        }

        public bool CloseComm()
        {
            try
            {
                switch (Global.LockType)
                {
                    case 0:
                        _doorLock.CloseCom();
                        break;
                    case 1:
                        _lockOperator.CloseCom();
                        break;
                    case 2:
                        _lockOperator.CloseCom();
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}
