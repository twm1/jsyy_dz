﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using chs;

namespace SmartRoom
{
    class HIBC
    {
        #region BSC Logic

        public static bool HIBCBarcodeDec(string Barcode1, string Barcode2, ref string __upn, ref string __lot, ref string __exp)
        {
            __upn = ""; __lot = ""; __exp = "";

            if (Barcode1 != "" && Barcode2 != "")
            {


                if ((Barcode1 == Barcode2) && (Barcode1.Length >= 24) && Barcode1.Substring(0, 2) == "01")
                {

                    Barcode2 = Barcode1.Substring(16, Barcode2.Length - 16);
                    Barcode1 = Barcode1.Substring(0, 16);

                }

                if (Barcode1.IndexOf("+$$") >= 0 && Barcode2.IndexOf("+$$") >= 0)
                {

                    Barcode1 = "";
                    Barcode2 = "";

                    Msg("扫了2次二级条形码!");
                    return false;
                }
                if (Barcode1.IndexOf("+") >= 0 && (Barcode1 + Barcode2).IndexOf("+") < 0)
                {

                    Barcode1 = "";
                    Barcode2 = "";

                    Msg("扫描错误,请重扫!");
                    return false;
                }

                if (Barcode1 == Barcode2)
                {

                    Barcode1 = "";
                    Barcode2 = "";

                    Msg("同一条形码被扫了2次!");
                    return false;
                }
                else
                {
                    if (Barcode1.IndexOf("$$") >= 0 || Barcode2.Substring(0, 2) == "01")
                    {
                      

                        Barcode1 = "";
                        Barcode2 = "";

                        Msg("Scan Err. Retry!\nMust Scan UPN Barcode First!");
                        return false;

                    }
                }



               
                string upn = GetHIBCUPN(Barcode1).ToUpper().Trim();

                string _L = "", _E = "";

                GetHIBCLot(Barcode2, false, out _L, out _E);


                __upn = upn;
                __lot = _L;
                __exp = _E;
                 


                if (_E.Length == 7)  //2009-11
                {
                    try
                    {
                        DateTime d = Convert.ToDateTime(_E + "-01");

                        TimeSpan S1 = new TimeSpan(d.Ticks);
                        TimeSpan S2 = new TimeSpan(DateTime.Now.Ticks);
                        TimeSpan S = S1.Subtract(S2);
                        if (S.Days < 32)
                        {
                            if (S.Days < 0)
                            {

                                Msg("扫描成功.\n但请注意该产品的有效期!");
                            }
                            else
                            {
                                //SunSound s = new SunSound("\\windows\\type.wav");
                                //s.Play();
                            }
                        }
                    }
                    catch
                    {
                        //SunSound s = new SunSound("\\windows\\type.wav");
                        //s.Play();
                    }
                }
                if (_E.Length == 10)  //2009-11-11
                {
                    try
                    {
                        DateTime d = Convert.ToDateTime(_E);

                        TimeSpan S1 = new TimeSpan(d.Ticks);
                        TimeSpan S2 = new TimeSpan(DateTime.Now.Ticks);
                        TimeSpan S = S1.Subtract(S2);
                        if (S.Days < 32)
                        {
                            if (S.Days < 0)
                            {
                              
                                Msg("扫描成功.\n但请注意该产品的有效期!");
                            }
                            else
                            {
                                //SunSound s = new SunSound("\\windows\\type.wav");
                                //s.Play();
                            }
                        }
                    }
                    catch
                    {

                    }

                }

                _L = _L.ToUpper().Trim();

                return true;
            }
            else
            {
                return false;
            }

        }

        public static bool EANBarcodeDec(string Barcode1, string Barcode2, ref string __upn, ref string __lot, ref string __exp)
        {
            __upn = ""; __lot = ""; __exp = ""; 

            if (Barcode1 != "" && Barcode2 != "")
            {


                if ((Barcode1 == Barcode2) && (Barcode1.Length >= 24) && Barcode1.Substring(0, 2) == "01")
                {

                    Barcode2 = Barcode1.Substring(16, Barcode2.Length - 16);
                    Barcode1 = Barcode1.Substring(0, 16);
                }
 
 
                if (Barcode1 == Barcode2)
                {
 
                   MessageBox.Show("不是规范的条形码!");
                   // return false;
                }
                else
                {
                    if (Barcode1.IndexOf("$$") >= 0 || Barcode2.Substring(0, 2) == "01")
                    {
                         

                        Barcode1 = "";
                        Barcode2 = "";

                        Msg("必须首先扫描UPN条码!");
                        return false;

                    }
                }



                string _L = "", _E = "";
                string upn;

                if (Barcode1 == Barcode2)
                {
                    upn = Barcode1;
                    _L = "";
                    _E = "";
                }
                else
                {
                    upn = GetEANUPN(Barcode1).ToUpper().Trim();
                    GetEANLot(Barcode2, out _L, out _E);
                }

                __upn = upn;
                __lot = _L;
                __exp = _E;
 


                if (_E.Length == 7)  //2009-11
                {
                    try
                    {
                        DateTime d = Convert.ToDateTime(_E + "-01");

                        TimeSpan S1 = new TimeSpan(d.Ticks);
                        TimeSpan S2 = new TimeSpan(DateTime.Now.Ticks);
                        TimeSpan S = S1.Subtract(S2);
                        if (S.Days < 32)
                        {
                            if (S.Days < 0)
                            {

                                Msg("扫描成功.\n但请注意该产品的有效期!");
                            }
                            else
                            {
                                //SunSound s = new SunSound("\\windows\\type.wav");
                                //s.Play();
                            }
                        }
                    }
                    catch
                    {
                        //SunSound s = new SunSound("\\windows\\type.wav");
                        //s.Play();
                    }
                }
                if (_E.Length == 10)  //2009-11-11
                {
                    try
                    {
                        DateTime d = Convert.ToDateTime(_E);

                        TimeSpan S1 = new TimeSpan(d.Ticks);
                        TimeSpan S2 = new TimeSpan(DateTime.Now.Ticks);
                        TimeSpan S = S1.Subtract(S2);
                        if (S.Days < 32)
                        {
                            if (S.Days < 0)
                            {

                                Msg("扫描成功.\n但请注意该产品的有效期!");
                            }
                            else
                            {
                                //SunSound s = new SunSound("\\windows\\type.wav");
                                //s.Play();
                            }
                        }
                    }
                    catch
                    {

                    }

                }

                _L = _L.ToUpper().Trim();

                return true;
            }
            else
            {
                return false;
            }

        }

        private static string GetHIBCUPN(string HIBCUPNBarcode)
        {
            string UPN = ""; 
            if (HIBCUPNBarcode.Substring(0, 2) == "01")
            {
                UPN = HIBCUPNBarcode.Substring(2, HIBCUPNBarcode.Length - 2);

                //if (UPN.Length > 2)
                //{
                //    if (UPN.Substring(0, 1) == "0") UPN = UPN.Substring(1, UPN.Length - 1);
                //}

                //if (UPN.Length > 2)
                //{
                //    if (UPN.Substring(0, 1) == "0") UPN = UPN.Substring(1, UPN.Length - 1);
                //}





            }
            else
            {
                UPN = HIBCUPNBarcode.Substring(1, HIBCUPNBarcode.Length - 2);
            }


             


            return UPN;
        }

        private static bool GetHIBCLot(string LotBarcode, bool IsUserProduceDate, out string _Lot, out string _ExpDate)
        {

            _ExpDate = ""; _Lot = "";


            if (LotBarcode.Substring(0, 1) != "+")
            {
                if (LotBarcode.Substring(0, 2) == "17")
                {
                    _ExpDate = "20" + LotBarcode.Substring(2, 2) + "-" + LotBarcode.Substring(4, 2) + "-" + LotBarcode.Substring(6, 2);
                }
                else
                {
                    _ExpDate = "";
                }

                if (LotBarcode.Length > 10)
                {
                    string shortlot = "";
                    if (LotBarcode.Substring(8, 2) == "10")
                    {
                        //01 234567 8901234
                        //17 100427 10 106066
                        shortlot = LotBarcode.Substring(8, LotBarcode.Length - 8);
                       // _Lot = App.GetMidStr(shortlot + ((char)29).ToString(), "10", ((char)29).ToString());
                    }

                    if (LotBarcode.Substring(8, 2) == "21")
                    {
                        //01 234567 8901234
                        //17 100427 10 106066
                        shortlot = LotBarcode.Substring(8, LotBarcode.Length - 8);
                       // _Lot = App.GetMidStr(shortlot + ((char)29).ToString(), "21", ((char)29).ToString());
                    }
                }
                else
                {
                    _Lot = "";
                }



                return true;

            }
            else
            {
                try
                {
                    #region 从二级条码中获取 _Lot，_ExpDate
                    //读取标志位: Qty Format Char
                    string _QtyFormatChar = "";
                    if (LotBarcode.Substring(0, 3) == "+$$")
                    {
                        _QtyFormatChar = LotBarcode.Substring(3, 1);
                        switch (_QtyFormatChar)
                        {
                            case "0":
                                _ExpDate = "20" + LotBarcode.Substring(5, 2) + "-" + LotBarcode.Substring(3, 2);  //   20YY-MM
                                _Lot = LotBarcode.Substring(7, LotBarcode.Length - 9);
                                break;
                            case "1":
                                _ExpDate = "20" + LotBarcode.Substring(3, 2) + "-" + LotBarcode.Substring(5, 2);  //   20YY-MM
                                _Lot = LotBarcode.Substring(7, LotBarcode.Length - 9);
                                break;
                            case "2":
                                _ExpDate = "20" + LotBarcode.Substring(8, 2) + "-" + LotBarcode.Substring(4, 2) + "-" + LotBarcode.Substring(6, 2);  //   20YY-MM-DD
                                _Lot = LotBarcode.Substring(10, LotBarcode.Length - 12);
                                break;
                            case "3":
                                _ExpDate = "20" + LotBarcode.Substring(4, 2) + "-" + LotBarcode.Substring(6, 2) + "-" + LotBarcode.Substring(8, 2);  //   20YY-MM-DD
                                _Lot = LotBarcode.Substring(10, LotBarcode.Length - 12);
                                break;
                            case "4":
                                _ExpDate = "20" + LotBarcode.Substring(4, 2) + "-" + LotBarcode.Substring(6, 2) + "-" + LotBarcode.Substring(8, 2) + " " + LotBarcode.Substring(10, 2) + ":00";  //   20YY-MM-DD HH:00
                                _Lot = LotBarcode.Substring(12, LotBarcode.Length - 14);
                                break;
                            case "5":
                                _ExpDate = "20" + LotBarcode.Substring(4, 2) + " Julian day:" + LotBarcode.Substring(6, 3);  //   20YY Julian day:JJJ
                                _Lot = LotBarcode.Substring(9, LotBarcode.Length - 11);
                                break;
                            case "6":
                                _ExpDate = "20" + LotBarcode.Substring(4, 2) + " Julian day:" + LotBarcode.Substring(6, 3) + " " + LotBarcode.Substring(9, 2) + ":00";  //   20YY Julian day:JJJ HH:00
                                _Lot = LotBarcode.Substring(11, LotBarcode.Length - 13);
                                break;
                            case "7":
                                _ExpDate = "";

                                // _ExpDate = App.InputBox("请依据外包装的内容，输入有效期（格式为：YYYY-MM-DD HH:MM:SS）");
                                _Lot = LotBarcode.Substring(4, LotBarcode.Length - 6);
                                Msg("不符合预定的HIBC格式");
                                return false;

                            // break;
                            case "8":

                                #region  依据第6位确定Exp Date 的格式
                                string _ExpDateFlag = LotBarcode.Substring(6, 1);
                                switch (_ExpDateFlag)
                                {
                                    case "0":
                                        _ExpDate = "20" + LotBarcode.Substring(8, 2) + "-" + LotBarcode.Substring(6, 2);  //   20YY-MM
                                        _Lot = LotBarcode.Substring(10, LotBarcode.Length - 12);
                                        break;
                                    case "1":
                                        _ExpDate = "20" + LotBarcode.Substring(8, 2) + "-" + LotBarcode.Substring(6, 2);  //   20YY-MM
                                        _Lot = LotBarcode.Substring(10, LotBarcode.Length - 12);
                                        break;
                                    case "2":
                                        _ExpDate = "20" + LotBarcode.Substring(11, 2) + "-" + LotBarcode.Substring(7, 2) + "-" + LotBarcode.Substring(9, 2);  //   20YY-MM-DD
                                        _Lot = LotBarcode.Substring(13, LotBarcode.Length - 15);
                                        break;
                                    case "3":
                                        _ExpDate = "20" + LotBarcode.Substring(7, 2) + "-" + LotBarcode.Substring(9, 2) + "-" + LotBarcode.Substring(11, 2);  //   20YY-MM-DD
                                        _Lot = LotBarcode.Substring(13, LotBarcode.Length - 15);
                                        break;
                                    case "4":
                                        _ExpDate = "20" + LotBarcode.Substring(7, 2) + "-" + LotBarcode.Substring(9, 2) + "-" + LotBarcode.Substring(11, 2) + " " + LotBarcode.Substring(13, 2) + ":00";  //   20YY-MM-DD HH:00
                                        _Lot = LotBarcode.Substring(15, LotBarcode.Length - 17);
                                        break;
                                    case "5":
                                        _ExpDate = "20" + LotBarcode.Substring(7, 2) + " Julian day:" + LotBarcode.Substring(9, 3);  //   20YY Julian day:JJJ
                                        _Lot = LotBarcode.Substring(12, LotBarcode.Length - 14);
                                        break;
                                    case "6":
                                        _ExpDate = "20" + LotBarcode.Substring(7, 2) + " Julian day:" + LotBarcode.Substring(9, 3) + " " + LotBarcode.Substring(12, 2) + ":00";  //   20YY Julian day:JJJ HH:00
                                        _Lot = LotBarcode.Substring(14, LotBarcode.Length - 16);
                                        break;
                                    case "7":


                                        if (IsUserProduceDate)
                                        {
                                            _ExpDate = "";// App.InputBox("请依据外包装的内容，输入产品的生产日期（格式与英文标签一致!）");

                                        }
                                        else
                                        {
                                            _ExpDate = "";// App.InputBox("请依据外包装的内容，输入产品的有效期（格式与英文标签一致!）");

                                        }

                                        _Lot = LotBarcode.Substring(7, LotBarcode.Length - 9);

                                        break;

                                    default:
                                        Msg("该二级条形码不符合HIBC规范！\n +$$ 8 QQ (0-7) Must 格式错误。");
                                        return false;
                                    //break;

                                }
                                #endregion
                                break;
                            case "9":
                                #region  依据第9位确定Exp Date 的格式
                                _ExpDateFlag = LotBarcode.Substring(9, 1);
                                switch (_ExpDateFlag)
                                {
                                    case "0":
                                        _ExpDate = "20" + LotBarcode.Substring(11, 2) + "-" + LotBarcode.Substring(9, 2);  //   20YY-MM
                                        _Lot = LotBarcode.Substring(13, LotBarcode.Length - 15);
                                        break;
                                    case "1":
                                        _ExpDate = "20" + LotBarcode.Substring(11, 2) + "-" + LotBarcode.Substring(9, 2);  //   20YY-MM
                                        _Lot = LotBarcode.Substring(13, LotBarcode.Length - 15);
                                        break;
                                    case "2":
                                        _ExpDate = "20" + LotBarcode.Substring(14, 2) + "-" + LotBarcode.Substring(10, 2) + "-" + LotBarcode.Substring(12, 2);  //   20YY-MM-DD
                                        _Lot = LotBarcode.Substring(16, LotBarcode.Length - 18);
                                        break;
                                    case "3":
                                        _ExpDate = "20" + LotBarcode.Substring(10, 2) + "-" + LotBarcode.Substring(12, 2) + "-" + LotBarcode.Substring(14, 2);  //   20YY-MM-DD
                                        _Lot = LotBarcode.Substring(16, LotBarcode.Length - 18);
                                        break;
                                    case "4":
                                        _ExpDate = "20" + LotBarcode.Substring(10, 2) + "-" + LotBarcode.Substring(12, 2) + "-" + LotBarcode.Substring(14, 2) + " " + LotBarcode.Substring(16, 2) + ":00";  //   20YY-MM-DD HH:00
                                        _Lot = LotBarcode.Substring(18, LotBarcode.Length - 20);
                                        break;
                                    case "5":
                                        _ExpDate = "20" + LotBarcode.Substring(10, 2) + " Julian day:" + LotBarcode.Substring(12, 3);  //   20YY Julian day:JJJ
                                        _Lot = LotBarcode.Substring(15, LotBarcode.Length - 17);
                                        break;
                                    case "6":
                                        _ExpDate = "20" + LotBarcode.Substring(10, 2) + " Julian day:" + LotBarcode.Substring(12, 3) + " " + LotBarcode.Substring(15, 2) + ":00";  //   20YY Julian day:JJJ HH:00
                                        _Lot = LotBarcode.Substring(17, LotBarcode.Length - 19);
                                        break;
                                    case "7":
                                        _ExpDate = "";
                                        //_ExpDate = App.InputBox("请依据外包装的内容，输入有效期（格式为：YYYY-MM-DD HH:MM:SS）");
                                        _Lot = LotBarcode.Substring(10, LotBarcode.Length - 12);
                                        break;

                                    default:
                                        Msg("该二级条形码不符合HIBC规范！\n +$$ 9 QQQQQ (0-7) Must 格式错误。");
                                        return false;
                                    //break;

                                }
                                #endregion
                                break;

                        }
                    }
                    else if (LotBarcode.Substring(0, 2) == "+$")
                    {
                        _Lot = LotBarcode.Substring(2, LotBarcode.Length - 4);
                        _ExpDate = "";

                    }
                    else if (LotBarcode.Substring(0, 1) == "+")
                    {
                        _Lot = LotBarcode.Substring(5, LotBarcode.Length - 8);


                        _ExpDate = "20" + LotBarcode.Substring(1, 2) + "-01-01";//

                        _ExpDate = DateTime.Parse(_ExpDate).AddDays(int.Parse(LotBarcode.Substring(3, 3))).ToString("yyyy-MM-dd");

                        //Msg("二级条形码日期形式为Julian Day :" + _ExpDate + "");
                        return true;

                    }
                    else
                    {
                        if (tiped < 2)
                        {
                            Msg("二级条形码不可识别!");
                        }
                        tiped++;

                        return false;
                    }

                    //2008-01"
                    if (_ExpDate.Length == 7) _ExpDate = _ExpDate + "-01";

                    return true;

                    #endregion

                }
                catch (Exception em)
                {
                    MessageBox.Show("二级条形码不可识别!" + em.Message, "Err");
                    return false;
                }
            }
        }

        public static string GetEANUPN(string EANUPNBarcode)
        {
            string UPN = "";

            if (EANUPNBarcode.Substring(0, 2) == "01" || EANUPNBarcode.Substring(0, 2) == "02")
            {

                UPN = EANUPNBarcode.Substring(2, 14);

                //if (UPN.Length > 2)
                //{
                //    if (UPN.Substring(0, 1) == "0") UPN = UPN.Substring(1, UPN.Length - 1);
                //}

                //if (UPN.Length > 2)
                //{
                //    if (UPN.Substring(0, 1) == "0") UPN = UPN.Substring(1, UPN.Length - 1);
                //}
            }
            else
            {
                UPN = EANUPNBarcode;
            }

            return UPN;
        }


        private static void GetEANLot(string LotBarcode, out string _Lot, out string _ExpDate)
        {

            _ExpDate = ""; _Lot = "";
            if (LotBarcode.Substring(0, 1) == "+")
            {
                return;
            }


#region 逐步缩减法


            #region L1
            //产品号
            if ((LotBarcode.Substring(0, 2) == "01") || (LotBarcode.Substring(0, 2) == "02"))
            {
                LotBarcode = LotBarcode.Substring(16, LotBarcode.Length - 16);
            }

            if ((LotBarcode.Length>2) && ((LotBarcode.Substring(0, 2) == "00")))
            {
                LotBarcode = LotBarcode.Substring(20, LotBarcode.Length - 20);
            }

            #endregion
            
            #region L2
            //有效期
            if ((LotBarcode.Length > 2) && (LotBarcode.Substring(0, 2) == "17"))
            {
                string ri = LotBarcode.Substring(6, 2);
                if (ri == "00") ri = "01";

                _ExpDate = "20" + LotBarcode.Substring(2, 2) + "-" + LotBarcode.Substring(4, 2) + "-" + ri;
                //17 121100 30 01 10 091222
                LotBarcode = LotBarcode.Substring(8, LotBarcode.Length - 8);
            }
            else if (LotBarcode.IndexOf("17") > 0)
            {
                if ((LotBarcode.Length > 2) && LotBarcode.Substring(0, 2) == "11")
                {
                    LotBarcode = LotBarcode.Substring(8, LotBarcode.Length - 8);
                }

                int x = LotBarcode.IndexOf("17");
                //_ExpDate = "20" + LotBarcode.Substring(x + 2, LotBarcode.Length - (x + 2));
                string ri = LotBarcode.Substring(x + 6, 2);
                if (ri.Equals("00")) ri = "01";
                _ExpDate = "20" + LotBarcode.Substring(x + 2, 2) + "-" + LotBarcode.Substring(x + 4, 2) + "-" + ri;
                if(x==0)
                {
                    LotBarcode = LotBarcode.Substring(8, LotBarcode.Length - 8);
                }
                else
                {
                    LotBarcode = LotBarcode.Substring(0, x);
                }
                
            }

            if (!string.IsNullOrEmpty(_ExpDate))
            {
                DateTime dtNow = Convert.ToDateTime(_ExpDate);
                if (dtNow.Day == 1)
                {
                    int days = DateTime.DaysInMonth(dtNow.Year, dtNow.Month);
                    dtNow = dtNow.AddDays(days - dtNow.Day);
                    _ExpDate = dtNow.ToString();
                }
            }
            //生产日期
            if ((LotBarcode.Length>2) && LotBarcode.Substring(0, 2) == "11" )
            {
                LotBarcode = LotBarcode.Substring(8, LotBarcode.Length - 8);
            }

            //其他日期
            if ((LotBarcode.Length>2) && ((LotBarcode.Substring(0, 2) == "13") || (LotBarcode.Substring(0, 2) == "15")))
            {
                LotBarcode = LotBarcode.Substring(8, LotBarcode.Length - 8);
            }

            #endregion

            #region L3

            ////序列号
            //if ((LotBarcode.Length > 2) && ((LotBarcode.Substring(0, 2) == "21")))
            //{
            //    _Lot = LotBarcode.Substring(2, LotBarcode.Length - 2);
            //    LotBarcode = LotBarcode.Substring(2, LotBarcode.Length - 2);
            //}
            ////批号
            //if ((LotBarcode.Length > 2) && ((LotBarcode.Substring(0, 2) == "10")))
            //{
            //    _Lot = LotBarcode.Substring(2, LotBarcode.Length - 2);
            //    LotBarcode = LotBarcode.Substring(2, LotBarcode.Length - 2);
            //}

            #endregion


#endregion


            if ((LotBarcode.Length > 3) && (LotBarcode.Substring(0, 2) == "23"))
            {
                _Lot = "";
            }

            if (LotBarcode.IndexOf("10") < 0)
            {
                _Lot = "";
            }
            else
            {
                //300110091222
                int x = LotBarcode.IndexOf("10");

                _Lot = LotBarcode.Substring(x + 2, LotBarcode.Length - (x + 2));

                if (_Lot.Length < 4)
                {
                    _Lot = "";
                }

            }


            if (_Lot == "" && LotBarcode.IndexOf("21") >= 0)
            {
                int y = LotBarcode.IndexOf("21");
                _Lot = LotBarcode.Substring(y + 2, LotBarcode.Length - (y + 2));

            }

            if (_Lot.Length >= 10)
            {
                int mx = _Lot.LastIndexOf("91");
                if (mx > 0)
                {
                    if ((_Lot.Length - mx) == 6 || (_Lot.Length - mx) == 5)
                    {
                        _Lot = _Lot.Substring(0, mx);
                    }
                }
            }
               




            //if (LotBarcode.Length > 10)
            //{
            //    string shortlot = "";
            //    if (LotBarcode.Substring(8, 2) == "10")
            //    {
            //        //01 234567 8901234
            //        //17 100427 10 106066
            //        shortlot = LotBarcode.Substring(8, LotBarcode.Length - 8);
            //        _Lot = App.GetMidStr(shortlot + ((char)29).ToString(), "10", ((char)29).ToString());
            //    }

            //    if (LotBarcode.Substring(8, 2) == "21")
            //    {
            //        //01 234567 8901234
            //        //17 100427 10 106066
            //        shortlot = LotBarcode.Substring(8, LotBarcode.Length - 8);
            //        _Lot = App.GetMidStr(shortlot + ((char)29).ToString(), "21", ((char)29).ToString());
            //    }
            //}
            //else
            //{
            //    _Lot = "";
            //}



        }

        private static int tiped = 0;

        public static void Msg(string txt)
        {           
            MessageBox.Show(txt, "HIBC Barcode", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
        }

        #endregion
    }
}
