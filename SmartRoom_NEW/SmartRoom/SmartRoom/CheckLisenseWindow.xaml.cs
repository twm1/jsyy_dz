﻿using System;
using System.Collections.Generic;
using System.Speech.Synthesis;
using System.Windows;
using Business;
using Entity;
using MahApps.Metro.Controls.Dialogs;
using SoftTool;

namespace SmartRoom
{
    /// <summary>
    /// MessageWindow.xaml 的交互逻辑
    /// </summary>
    public partial class CheckLisenseWindow : Window
    {
        SpeechSynthesizer synth = new SpeechSynthesizer();
        string message = string.Empty;

        ForcerInfoBusiness forcerInfoBusiness = new ForcerInfoBusiness();
        KFBusiness kfBusiness = new KFBusiness();
        public CheckLisenseWindow(string _msg)
        {
            InitializeComponent();
            message = _msg;
            lbl_message.Content = _msg;  
        }

        public CheckLisenseWindow()
        {
            InitializeComponent();
            message = "请输入设备名称，完成设备注册";
            lbl_message.Content = message;
        }

        private void btn_Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            //synth.Speak(message);
            txt_DeviceMac.Text = DeviceInfo.GetMacAddress();//GetCpuID();//.
            txt_DeviceName.Focus();
            List<KFInfo> list = new List<KFInfo>();
            list = kfBusiness.GetAllKFInfo();
            cbx_CK.ItemsSource = list;
            cbx_SJCK.ItemsSource = list;
        }

        private void btn_Submit_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txt_DeviceName.Text))
            {
                MessageBox.Show("请输入设备名称!");
                txt_DeviceName.Focus();
                return;
            }
            if (cbx_CK.SelectedIndex == -1 || string.IsNullOrEmpty(Convert.ToString(cbx_CK.SelectedValue)))
            {
                MessageBox.Show("请选择对应的仓库!");
                cbx_CK.Focus();
                return;
            }
            ForcerInfo obj = new ForcerInfo();
            obj.ForcerName = txt_DeviceName.Text;
            obj.MacAddress = txt_DeviceMac.Text;
            obj.KFID = cbx_CK.SelectedValue.ToString();
            obj.SJKFID = cbx_SJCK.SelectedValue.ToString();
            if (forcerInfoBusiness.InsertForcerInfo(obj))
            {
                Global.MessageWindow.ShowDialog("注册成功，请等待管理员开通！",Enum.MessageButton.Close,Enum.MessageStyle.Success);
                this.Close();
            }
            else
            {
                Global.MessageWindow.ShowDialog("注册失败，请检查网络重新提交或联系管理员！", Enum.MessageButton.Close, Enum.MessageStyle.Error);
            }
        }
    }
}
