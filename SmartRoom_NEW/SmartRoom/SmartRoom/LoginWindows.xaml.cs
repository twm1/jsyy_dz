﻿using Business;
using chs;
using Entity;
using SoftTool;
using SourceAFIS.Simple;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Speech.Synthesis;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Threading;
using ArcSoftFace.Entity;
using AForgeTest;
using ArcSoftFace.Utils;
using ArcSoftFace.SDKModels;
using ArcSoftFace.SDKUtil;

namespace SmartRoom
{
    /// <summary>
    /// LoginWindows.xaml 的交互逻辑
    /// </summary>
    public partial class LoginWindows : Window
    {
        /// <summary>
        /// 播放声音
        /// </summary>
        /// <param name="dwFreg"></param>
        /// <param name="dwDuration"></param>
        /// <returns></returns>
        [DllImport("kernel32")]
        public static extern int Beep(int dwFreg, int dwDuration);

        SpeechSynthesizer synth = new SpeechSynthesizer();

        public bool isUserLogin = false;

        private UserBusiness userBusiness = new UserBusiness();

        #region 指纹
        private Thread CollectionThread = null;

        private static System.Timers.Timer _startTimer;
        private IntPtr DeviceHandle = IntPtr.Zero;
        private UInt32 N_ADDR = 0XFFFFFFFF;//设备地址
        bool IsCollection = false;
        static AfisEngine AFIS = new AfisEngine();

        List<UserFingerPrint> _fingerList = new List<UserFingerPrint>();
        List<UserFingerBit> _userFingerList = new List<UserFingerBit>();
        #endregion
        public LoginWindows()
        {
            InitializeComponent();
            CameraHelper.IsDisplay = true;
            CameraHelper.SourcePlayer = player;
            CameraHelper.UpdateCameraDevices();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            if (txtName.Text.Equals("Gadmin") && txtpwd.Password.Equals("Jjj111"))
                this.Close();
            else
            {
                Global.MessageWindow.ShowDialog("非管理员用户,不能退出当前系统！", Enum.MessageButton.Close, Enum.MessageStyle.Warn);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                try
                {
                    Global.LockPort = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["LockPort"].Value;
                    Global.LockBaud = Convert.ToInt32(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["LockBaud"].Value);
                    Global.FingerCheckValue = Convert.ToInt32(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["FingerCheckValue"].Value);
                    Global.LockType= Convert.ToInt32(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["LockType"].Value);
                    Global.LockTime = Convert.ToInt32(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["LockTime"].Value);
                    Global.IsTouch= Convert.ToInt32(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["IsTouch"].Value);
                }
                catch (Exception ex)
                {
                    Global.MessageWindow.ShowDialog("加载配置文件出现异常!" + ex.Message, Enum.MessageButton.Close, Enum.MessageStyle.Error);
                    this.Close();
                    return;
                }

                WaitWindow wait = new WaitWindow("正在下载指纹信息，请稍等...");
                try
                {
                    _fingerList = userBusiness.GetUserFingerPrint(Global.forcerInfo.KFID);

                    foreach (UserFingerPrint obj in _fingerList)
                    {
                        //获取数据库中的指纹信息
                        Bitmap bit1 = ShowFingerPrint(obj.FingerPrint);
                        //数据库中的的指纹信息
                        Fingerprint fp = new Fingerprint();
                        Person dataPerson = new Person(fp);
                        fp.AsBitmap = bit1;
                        AFIS.Extract(dataPerson);
                        UserFingerBit fBit = new UserFingerBit();
                        fBit.UserID = obj.UserID;
                        fBit.QueryData = dataPerson;
                        _userFingerList.Add(fBit);
                    }
                }
                catch (Exception ex)
                { }
                wait.Close();
                SetTbkVisable(false);

                OpenCollection();

                isUserLogin = true;
                grd_UserName.Visibility = Visibility.Visible;
                wfh_Scan.Visibility = Visibility.Hidden;
                txtpwd.Visibility = Visibility.Visible;
                tbk_Finger.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 68, 64, 62));
                tbk_UserName.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 255, 58, 0));
                txtName.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("加载配置出现异常，请检查配置文件！");
            }
        }


        void SetTbkVisable(bool isShow)
        {
            if (isShow)
            {
                isUserLogin = false;
                wfh_Scan.Visibility = Visibility.Visible;
                grd_UserName.Visibility = Visibility.Hidden;
                tbk_Finger.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 255, 58, 0));
                tbk_UserName.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 68, 64, 62));

            }
            else 
            {
                isUserLogin = true;
                grd_UserName.Visibility = Visibility.Visible;
                wfh_Scan.Visibility = Visibility.Hidden;
                txtpwd.Visibility = Visibility.Visible;
                tbk_Finger.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 68, 64, 62));
                tbk_UserName.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 255, 58, 0));
                txtName.Focus();
            }
        }

        private void tbk_MouseDown(object sender, MouseButtonEventArgs e)
        {
            TextBlock tbk = (e.Source) as TextBlock;
            if (tbk.Name.Equals("tbk_Finger"))
            {
                SetTbkVisable(true);
                //isUserLogin = false;
                //wfh_Scan.Visibility = Visibility.Visible;
                //grd_UserName.Visibility = Visibility.Hidden;
                //tbk_Finger.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 255, 58, 0));
                //tbk_UserName.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 68, 64, 62));
              
            }
            else if (tbk.Name.Equals("tbk_UserName"))
            {
                SetTbkVisable(false);
                //isUserLogin = true;
                //grd_UserName.Visibility = Visibility.Visible;
                //wfh_Scan.Visibility = Visibility.Hidden;
                //txtpwd.Visibility = Visibility.Visible;
                //tbk_Finger.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 68, 64, 62));
                //tbk_UserName.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 255, 58, 0));
                //txtName.Focus();
            }

        }


        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            Login();
        }

        void Login()
        {
            try
            {
                UserInfo userInfo = null;
                if (isUserLogin)
                {
                    chs.SECU_Hash pwd = new SECU_Hash();
                    string mpwd = pwd.CreateHash(txtpwd.Password, SECU_Hash.PDSAHashType.MD5, "r1u9h7u8a");

                    if (string.IsNullOrEmpty(txtName.Text))
                    {
                        Global.MessageWindow.ShowDialog("请填写用户名！", Enum.MessageButton.Close, Enum.MessageStyle.Warn);
                        txtName.Focus();
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtpwd.Password))
                    {
                        Global.MessageWindow.ShowDialog("请填写密码！", Enum.MessageButton.Close, Enum.MessageStyle.Warn);
                        txtpwd.Focus();
                        return;
                    }
                    userInfo = userBusiness.GetUserInfoByNamePass(txtName.Text, mpwd);
                }
                else
                {
                    if (string.IsNullOrEmpty(txtName.Text))
                    {
                        Global.MessageWindow.ShowDialog("请填写关键字！", Enum.MessageButton.Close, Enum.MessageStyle.Warn);
                        txtName.Focus();
                        return;
                    }

                }

                if (userInfo == null)
                {
                    Global.MessageWindow.ShowDialog("用户名或密码错误,请检查后重新输入！", Enum.MessageButton.Close, Enum.MessageStyle.Error);
                    txtName.Focus();
                    return;
                }

                bool isKFRule = false;
                foreach (var obj in userInfo.UserKFList)
                {
                    if (Global.forcerInfo.KFID.Equals(obj.KFID))
                    {
                        isKFRule = true;
                        break;
                    }
                }

                if (!isKFRule)
                {
                    Global.MessageWindow.ShowDialog("您没有当前库房的登录权限，请联系管理员！", Enum.MessageButton.Close, Enum.MessageStyle.Warn);
                    txtName.Focus();
                    return;
                }

                Global.User = userInfo;

                MenuWindows menu = new MenuWindows();
                IsCollection = true;
                menu.ShowDialog();
                IsCollection = false;

                txtName.Text = "";
                txtpwd.Password = "";
                txtName.Focus();
                //tbk_Finger.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 58, 0));
                //tbk_UserName.Foreground = new SolidColorBrush(Color.FromArgb(255, 68, 64, 62));
            }
            catch (Exception ex)
            {
                Global.MessageWindow.ShowDialog("登录失败，请检查网络重新提交或联系管理员！", Enum.MessageButton.Close, Enum.MessageStyle.Error);
            }
        }

        /// <summary>
        /// 打开软键盘
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void imgKeyBoard_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Process.Start(Environment.CurrentDirectory + "\\Asset\\exe\\keyboard.exe");
            }
            catch
            {
                try
                {
                    Process.Start(@"C:\Program Files\Common Files\Microsoft Shared\Ink\TabTip.exe");
                }
                catch
                { }
            }
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void txtName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (isUserLogin)
                {
                    txtpwd.Focus();
                }
                else
                {
                    Login();
                }
            }
        }

        private void txtpwd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Login();
            }
        }

        #region 指纹
        private void StopCollection()
        {
            try
            {

                IsCollection = true;
                DriveOpration.PSCloseDeviceEx(DeviceHandle);

                if (CollectionThread != null && CollectionThread.IsAlive)
                {
                    //CollectionThread.Suspend();
                    CollectionThread.Abort();
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void OpenCollection()
        {
            try
            {
                int result = DriveOpration.PSOpenDeviceEx(ref DeviceHandle, DriveOpration.DEVICE_UDisk, DriveOpration.COM1, DriveOpration.BAUD_RATE_57600, 2, 0);
                if (DeviceHandle == IntPtr.Zero || result != 0)
                {
                    Global.MessageWindow.ShowDialog("打开指纹设备失败！", Enum.MessageButton.Close, Enum.MessageStyle.Error);
                }
                else
                {
                    CollectionThread = new Thread(new ThreadStart(FigerCollection));
                    CollectionThread.Start();
                }
            }
            catch (Exception ex)
            {
                Global.MessageWindow.ShowDialog("打开指纹设备失败！", Enum.MessageButton.Close, Enum.MessageStyle.Error);
                StopCollection();
            }
        }

        bool isLogin = true;
        private void FigerCollection()
        {
            try
            {
                while (!IsCollection)
                {

                    int ret = -1;
                    do
                    {
                        ret = DriveOpration.PSGetImage(DeviceHandle, N_ADDR);
                    }
                    while (ret != DriveOpration.PS_OK);

                    if (ret == DriveOpration.PS_OK)
                    {
                        UInt16 templateLength = 0;
                        byte[] imageData = new byte[256 * 288];
                     
                        ret = DriveOpration.PSUpImage(DeviceHandle, 1, imageData, ref templateLength);//上传图片
                                                                                                    

                        if (ret == DriveOpration.PS_OK)
                        {
                            if (ret == DriveOpration.PS_OK)
                            {
                                if (isLogin)
                                {
                                    IsCollection = true;
                                    isLogin = false;
                                    CheckFinger(imageData);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            { }
        }


        private void CheckFinger(byte[] byt)
        {
            try
            {
                Logger.WriteLog("开始验证" + DateTime.Now);
                //采集到的指纹信息
                Fingerprint fpSource = new Fingerprint();
                Person queryPerson = new Person(fpSource);
                fpSource.AsBitmap = ShowFingerPrint(byt);
                AFIS.Extract(queryPerson);
                Logger.WriteLog("准备完成" + DateTime.Now);
                #region 
                //foreach (var obj in _fingerList)
                //{
                //    Logger.WriteLog("开始转换" + DateTime.Now);
                //    //获取数据库中的指纹信息
                //    Bitmap bit1 = ShowFingerPrint(obj.FingerPrint);
                //    //数据库中的的指纹信息
                //    Fingerprint fp = new Fingerprint();
                //    Person dataPerson = new Person(fp);

                //    fp.AsBitmap = bit1;
                //    AFIS.Extract(dataPerson);
                //    Logger.WriteLog("转换完成" + DateTime.Now);
                //    AFIS.Threshold = 10;
                //    Logger.WriteLog("开始匹配" + DateTime.Now);
                //    float verify = AFIS.Verify(queryPerson, dataPerson);
                //    Logger.WriteLog("匹配完成" + DateTime.Now);
                //    if (verify > 50)//验证通过
                //    {
                //        UserInfo userInfo = userBusiness.GetUesrInfoByUserID(obj.UserID);
                //        bool isKFRule = false;
                //        foreach (var kf in userInfo.UserKFList)
                //        {
                //            if (Global.forcerInfo.KFID.Equals(kf.KFID))
                //            {
                //                isKFRule = true;
                //                break;
                //            }
                //        }

                //        if (!isKFRule)
                //        {
                //            Global.MessageWindow.ShowDialog("您没有当前库房的登录权限，请联系管理员！", Enum.MessageButton.Close, Enum.MessageStyle.Warn);
                //            txtName.Focus();
                //            return;
                //        }
                //        Global.User = userInfo;


                //        Beep(3000, 100);
                //        App.Current.Dispatcher.Invoke((Action)(() =>
                //        {
                //            MenuWindows menu = new MenuWindows();
                //            //menu.Topmost = true;
                //            menu.ShowDialog();
                //        }));


                //        //IsCollection = false;
                //        //_startTimer.Start();
                //    }
                //}
                #endregion

                bool isCheck = false;

                foreach (UserFingerBit obj in _userFingerList)
                {
                    float verify = AFIS.Verify(queryPerson, obj.QueryData);
                    if (verify >= Global.FingerCheckValue)//验证通过
                    {
                        isCheck = true;
                        UserInfo userInfo = userBusiness.GetUesrInfoByUserID(obj.UserID);
                        bool isKFRule = false;
                        foreach (var kf in userInfo.UserKFList)
                        {
                            if (Global.forcerInfo.KFID.Equals(kf.KFID))
                            {
                                isKFRule = true;
                                break;
                            }
                        }

                        if (!isKFRule)
                        {
                            Global.MessageWindow.ShowDialog("您没有当前库房的登录权限，请联系管理员！", Enum.MessageButton.Close, Enum.MessageStyle.Warn);
                            txtName.Focus();
                            return;
                        }
                        Global.User = userInfo;


                        Beep(3000, 100);
                        App.Current.Dispatcher.Invoke((Action)(() =>
                        {
                            MenuWindows menu = new MenuWindows();
                            Global.SystemIdelTime = 0;
                            //menu.Topmost = true;
                            IsCollection = false;
                            isLogin = true;
                            menu.ShowDialog();
                        }));
                        break;
                    }
                }

                if (!isCheck)
                {
                    try
                    {
                        synth.Speak("请重新输入指纹");
                    }
                    catch
                    {
                        Beep(3000, 50);
                        Beep(3000, 50);
                    }
                }
                IsCollection = false;
                isLogin = true;
            }
            catch (Exception ex)
            {
                Global.MessageWindow.ShowDialog(ex.Message, Enum.MessageButton.Close, Enum.MessageStyle.Warn);
            }
            finally
            {

            }
        }


        public Bitmap ShowFingerPrint(byte[] rgbValues)
        {
            Bitmap bmp = new Bitmap(256, 288);//指纹图片
            for (int i = 0; i < bmp.Height; i++)
            {
                for (int j = 0; j < bmp.Width; j++)
                {
                    int value = (int)rgbValues[i * bmp.Width + j];
                    System.Drawing.Color c = System.Drawing.Color.FromArgb(value, value, value);

                    bmp.SetPixel(j, i, c);
                }
            }
            return bmp;
        }
        #endregion

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                StopCollection();
                Global.lockOpt.CloseComm();
            }
            catch (Exception ex)
            {

            }
        }
        /// <summary>
        /// 获取摄像头的每一帧
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
     
        private bool isLock = true;
        ASF_SingleFaceInfo singleFaceInfo = new ASF_SingleFaceInfo();
        List<UserInfo> faceuser = new List<UserInfo>();//人脸信息框
        private void player_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            try
            {
                if (isLock==true)
                {
                    isLock = false;
                    if (CameraHelper.getvideo().IsRunning)
                    {
                        //string TheLock = "" + wfh_Scan.Visibility;
                        if (!isUserLogin)
                        {
                             CallFace();
                        }
                    }
                    else
                    {
                        CameraHelper.getvideo();
                        isLock = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Global.MessageWindow.ShowDialog(ex.Message, Enum.MessageButton.Close, Enum.MessageStyle.Warn);
            }
        }


        /// <summary>
        /// 调用人脸去识别
        /// </summary>
        public void CallFace()
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(delegate
            {
                Bitmap bitmap1 = CameraHelper.getCurrentVideo();
                ASF_MultiFaceInfo multiFaceInfo = FaceUtil.DetectFace(Engine.pVideoEngine, bitmap1);
                if (multiFaceInfo.faceNum >= 1)
                {
                    IntPtr feature = FaceUtil.ExtractFeature(Engine.pImageEngine, bitmap1, out singleFaceInfo);
                    if (faceuser.Count <= 0)
                    {
                        faceuser = userBusiness.GetUsersByface();
                    }
                    for (int i = 0; i < faceuser.Count; i++)
                    {
                        float similarity = 0f;
                        if (faceuser[i].face == null) continue;
                        ASFFunctions.ASFFaceFeatureCompare(Engine.pImageEngine, Engine.PutFeatureByteIntoFeatureIntPtr(faceuser[i].face), feature, ref similarity);
                        // Global.MessageWindow.ShowDialog("相识度" + similarity, Enum.MessageButton.Close, Enum.MessageStyle.Error);
                        if (similarity.ToString().IndexOf("E") > -1)
                        {
                            similarity = 0f;
                        }
                        if (similarity > 0.8)
                        {
                            Dispatcher.BeginInvoke(new Action(delegate
                            {
                                //Global.User = faceuser[i];
                                Global.User = userBusiness.GetUesrInfoByUserID(faceuser[i].UserID);
                                MenuWindows menu = new MenuWindows();
                                menu.ShowDialog();
                                SetTbkVisable(false);
                                isLock = true;
                            }));
                            return;
                        }
                    }
                    isLock = true;
                }
                else
                {
                    isLock = true;
                }
            }));
        }

    }

    public class UserFingerBit
    {
        private string _userID;
        private Person _queryData;
        public string UserID { get => _userID; set => _userID = value; }
        public Person QueryData { get => _queryData; set => _queryData = value; }
    }
}