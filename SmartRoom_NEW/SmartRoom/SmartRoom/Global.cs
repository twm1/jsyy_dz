﻿using Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Xml;

namespace SmartRoom
{
    public class Global
    {
        public static UserInfo User;

        /// <summary>
        /// 锁的端口号
        /// </summary>
        public static string LockPort;
        /// <summary>
        /// 锁的波特率
        /// </summary>
        public static int LockBaud;

        public static int FingerCheckValue=50;

        /// <summary>
        /// 0表示平移门；1表示电磁锁；2表示电吸锁
        /// </summary>
        public static int LockType = 0;

        /// <summary>
        /// 电吸门打开时间  默认5秒
        /// </summary>
        public static int LockTime = 5000;


        public static long SystemIdelTime = 0;

       /// <summary>
       /// 门控锁
       /// </summary>
        public static DoorLockOperation lockOpt=new DoorLockOperation();

        public static MessageWindow MessageWindow = new MessageWindow();

        public static ForcerInfo forcerInfo;

        public static List<Window> WindowsList = new List<Window>();

        /// <summary>
        /// 1表示触摸屏,0表示非触摸屏
        /// </summary>
        public static int IsTouch = 1;


        
        
        /// <summary>
        /// 字符串转16进制数组，字符串以空格分割
        /// </summary>
        /// <param name="strHexValue"></param>
        /// <returns></returns>
        public static byte[] StringToByteArray(string strHexValue)
        {
            string[] strAryHex = strHexValue.Split(' ');
            byte[] btAryHex = new byte[strAryHex.Length];

            try
            {
                int nIndex = 0;
                foreach (string strTemp in strAryHex)
                {
                    btAryHex[nIndex] = Convert.ToByte(strTemp, 16);
                    nIndex++;
                }
            }
            catch (System.Exception ex)
            {

            }

            return btAryHex;
        }

        public static void WriteXml(string number,string message)
        {
            string xmlPath = "Message.xml";
            XmlDocument xmlDoc = new XmlDocument();
            int i = 1;
            if (File.Exists(xmlPath))
            {
                xmlDoc.Load(xmlPath);

                XmlNode root = xmlDoc.SelectSingleNode("MessageInfoList");
                XmlElement node = xmlDoc.CreateElement("MessageInfo");
                root.AppendChild(node);

                XmlElement nodec = xmlDoc.CreateElement("SendPhoneNumber");
                nodec.InnerText = number;
                node.AppendChild(nodec);

                XmlElement nodec1 = xmlDoc.CreateElement("Message");
                nodec1.InnerText = message;
                node.AppendChild(nodec1);

                XmlElement nodec2 = xmlDoc.CreateElement("SendState");
                nodec2.InnerText = "0";
                node.AppendChild(nodec2);
                xmlDoc.Save(xmlPath);
            }
            else
            {
                XmlNode code = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", "");
                xmlDoc.AppendChild(code);

                XmlElement root = xmlDoc.CreateElement("MessageInfoList");
                xmlDoc.AppendChild(root);

                XmlElement node = xmlDoc.CreateElement("MessageInfo");
                root.AppendChild(node);

                XmlElement nodec = xmlDoc.CreateElement("SendPhoneNumber");
                nodec.InnerText = number;
                node.AppendChild(nodec);

                XmlElement nodec1 = xmlDoc.CreateElement("Message");
                nodec1.InnerText = message;
                node.AppendChild(nodec1);

                XmlElement nodec2 = xmlDoc.CreateElement("SendState");
                nodec2.InnerText = "0";
                node.AppendChild(nodec2);
                xmlDoc.Save(xmlPath);
            }
        }

    }
}
