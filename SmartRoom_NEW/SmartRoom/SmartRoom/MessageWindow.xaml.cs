﻿using SmartRoom.Enum;
using System;
using System.ComponentModel;
using System.Speech.Synthesis;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace SmartRoom
{
    /// <summary>
    /// MessageWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MessageWindow : Window
    {
        SpeechSynthesizer synth=new SpeechSynthesizer();
        string message = string.Empty;

        MessageButton msgButton = MessageButton.Cancel;
        MessageStyle msgStyle = MessageStyle.Success;
        public MessageDialogResult dialogResult = MessageDialogResult.Cancel;

        public MessageWindow()
        {
            InitializeComponent();
        }


        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            SetInfo();
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (!(btn is Button))
                return;
            if (btn == btn_1)
            {
                dialogResult = MessageDialogResult.Yes;
            }
            else if (btn == btn_2)
            {
                dialogResult = MessageDialogResult.No;
            }
            else if (btn == btn_3)
            {
                dialogResult = MessageDialogResult.Cancel;
            }
            this.Close();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = true;  // cancels the window close    
            this.Hide();      // Programmatically hides the window
        }
        

        public MessageDialogResult  ShowDialog(string _msg)
        {
            message = _msg;
            msgStyle = MessageStyle.Success;
            msgButton = MessageButton.Close;
            SetInfo();
            this.ShowDialog();
            return dialogResult;
        }

        public MessageDialogResult ShowDialog(string _msg, MessageButton _button, MessageStyle _style)
        {
            msgButton = _button;
            msgStyle = _style;
            message = _msg;
            SetInfo();
            this.ShowDialog();
            return dialogResult;
        }

        private void SetInfo()
        {
            btn_1.Visibility = Visibility.Visible;
            btn_2.Visibility = Visibility.Visible;
            btn_3.Visibility = Visibility.Visible;
            Uri url = null;
            switch (msgStyle)
            {
                case MessageStyle.Success:
                    url = new Uri("Asset/images/对号.png", UriKind.RelativeOrAbsolute);
                    break;

                case MessageStyle.Error:
                    url = new Uri("Asset/images/错号.png", UriKind.RelativeOrAbsolute);
                    break;

                case MessageStyle.Question:
                    url = new Uri("Asset/images/问号.png", UriKind.RelativeOrAbsolute);
                    break;

                case MessageStyle.Warn:
                    url = new Uri("Asset/images/警告.png", UriKind.RelativeOrAbsolute);
                    break;
            }

            switch (msgButton)
            {
                case MessageButton.Cancel:
                    btn_1.Visibility = Visibility.Hidden;
                    btn_2.Visibility = Visibility.Hidden;
                    btn_3.Content = "取消";
                    break;
                case MessageButton.Close:
                    btn_1.Visibility = Visibility.Hidden;
                    btn_2.Visibility = Visibility.Hidden;
                    btn_3.Content = "关闭";
                    break;
                case MessageButton.YesNo:
                    btn_1.Content = "是";
                    btn_2.Content = "否";
                    btn_3.Visibility = Visibility.Hidden;
                    break;
                case MessageButton.YesNoCancel:
                    btn_1.Content = "是";
                    btn_2.Content = "否";
                    btn_3.Content = "取消";
                    break;
            }
            txt_Message.Text = message;
            BitmapImage imgSource = new BitmapImage(url);
            img_Message.Source = imgSource;
        }
    }
}