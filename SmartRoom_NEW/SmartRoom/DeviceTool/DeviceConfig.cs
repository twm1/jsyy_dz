﻿using JW.UHF;
using System;
using System.Collections.Generic;

namespace DeviceTool
{
  
    /// <summary>
    /// 继电器信息
    /// </summary>
    /// 
    [Serializable]
    public class ControlInfo
    {
        private string _port;
        /// <summary>
        /// 端口
        /// </summary>
        public string Port
        {
            get { return _port; }
            set { _port = value; }
        }
        private string _bound;
        /// <summary>
        /// 波特率
        /// </summary>
        public string Bound
        {
            get { return _bound; }
            set { _bound = value; }
        }
        private string _deviceAdd;
        /// <summary>
        /// 设备地址
        /// </summary>
        public string DeviceAdd
        {
            get { return _deviceAdd; }
            set { _deviceAdd = value; }
        }
    }
    /// <summary>
    /// 温湿度信息
    /// </summary>
    /// 
    [Serializable]
    public class TempInfo
    {
        private string _port;
        /// <summary>
        /// 端口
        /// </summary>
        public string Port
        {
            get { return _port; }
            set { _port = value; }
        }

        private string _bound;
        /// <summary>
        /// 波特率
        /// </summary>
        public string Bound
        {
            get { return _bound; }
            set { _bound = value; }
        }

        private string _checkCode;
        /// <summary>
        /// 校验码
        /// </summary>
        public string CheckCode
        {
            get { return _checkCode; }
            set { _checkCode = value; }
        }
    }
    /// <summary>
    /// 锁信息
    /// </summary>
    /// 
    [Serializable]
    public class LockInfo
    {
        private string _lockName;

        public string LockName
        {
            get { return _lockName; }
            set { _lockName = value; }
        }
        private int _lockIndex;

        public int LockIndex
        {
            get { return _lockIndex; }
            set { _lockIndex = value; }
        }
    }
}