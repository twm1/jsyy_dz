﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeviceTool
{
    public class BaudRate
    {
        public string[] GetBaudRate()
        {
            string[] Baud = {"2400","4800","9600","19200","38400","115200"};
            return Baud;
        }
    }
}