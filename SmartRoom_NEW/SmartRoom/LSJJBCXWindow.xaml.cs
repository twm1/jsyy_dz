﻿using Business;
using Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using SmartRoom;
namespace SmartRoom
{
    /// <summary>
    /// DetailWindow.xaml 的交互逻辑
    /// </summary>
    public partial class LSJJBCXWindow : Window
    {
        JJBBusiness _business = new JJBBusiness();
        List<CYInfo> billList = new List<CYInfo>();
        List<LSJJBInfo> lsBillList = new List<LSJJBInfo>();

        /// <summary>
        /// 0:表示码货 从主库出到临时库;1:表示退货 从临时库出到主库;
        /// </summary>
        public LSJJBCXWindow()
        {
            InitializeComponent();
            lblAppTitle.Content = "历史交接班查询";
        }


        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            LoadData();
        }

        void LoadData()
        {
            try
            {
                lvw_RK.ItemsSource = billList;
                lvw_JJB.ItemsSource = lsBillList;
        
                lsBillList = _business.GetLSJJB(Global.forcerInfo.KFID);
                lvw_JJB.ItemsSource = lsBillList;
                lvw_JJB.Items.Refresh();
            }
            catch (Exception ex)
            { }
        }




        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                string strException;
                Global.lockOpt.CloseDoor(out strException);
            }
            catch (Exception ex)
            { }
        }


        private void btn_Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        private void btn_Refresh_Click(object sender, RoutedEventArgs e)
        {
            LoadData();
        }

        private void lvw_JJB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            

          
        }

        private void lvw_JJB_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                LSJJBInfo obj = lvw_JJB.SelectedItem as LSJJBInfo;

                billList = _business.GetLSJJBInfo(obj.ID,Global.forcerInfo.KFID, obj.BeginDate, obj.EndDate);
                lvw_RK.ItemsSource = billList;
                lvw_RK.Items.Refresh();
            }
            catch { }
        }
    }
}
