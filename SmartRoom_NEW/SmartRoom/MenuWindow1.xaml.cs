﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SmartRoom
{
    /// <summary>
    /// MenuWindow1.xaml 的交互逻辑
    /// </summary>
    public partial class MenuWindow1 : Window
    {
        public MenuWindow1()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (Global.User != null)
                hint_name.Content = "登录人:  " + Global.User.RealName;
            if (Global.forcerInfo != null)
                hint_warehouse.Content = "登录库房 : "+Global.forcerInfo.KfName;
            string Exception = string.Empty;

            Global.LockPort = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["LockPort"].Value;
            Global.LockBaud = Convert.ToInt32(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["LockBaud"].Value);
            Global.WindowsList.Add(this);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Global.WindowsList.Remove(this);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try{
                //if (Global.IsTouch == 1) return;
                JJBCXWindow jjb = new JJBCXWindow();
                Global.WindowsList.Add(jjb);
                jjb.ShowDialog();
                Global.WindowsList.Remove(jjb);
            }
            catch (Exception ex)
            { }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (Global.IsTouch == 1)
                //    return;
                //Global.MessageWindow.ShowDialog("领用!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
                InStockBillWindow InStockBillWindow = new InStockBillWindow();
                Global.WindowsList.Add(InStockBillWindow);
                InStockBillWindow.ShowDialog();
                Global.WindowsList.Remove(InStockBillWindow);
            }
            catch (Exception ex)
            { }
        }

        private void Image_MouseDown_2(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (Global.IsTouch == 1)
                //    return;
                //Global.MessageWindow.ShowDialog("领用!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
                OutDetailWindow outWindow = new OutDetailWindow(0);
                Global.WindowsList.Add(outWindow);
                outWindow.ShowDialog();
                Global.WindowsList.Remove(outWindow);
            }
            catch (Exception ex)
            { }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (Global.IsTouch == 1)
                //    return;
                //Global.MessageWindow.ShowDialog("归还!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
                OutDetailWindow outWindow = new OutDetailWindow(1);
                Global.WindowsList.Add(outWindow);
                outWindow.ShowDialog();
                Global.WindowsList.Remove(outWindow);
            }
            catch (Exception ex)
            { }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (Global.IsTouch == 1) return;
                LSJJBCXWindow jjb = new LSJJBCXWindow();
                Global.WindowsList.Add(jjb);
                jjb.ShowDialog();
                Global.WindowsList.Remove(jjb);
            }
            catch (Exception ex)
            { }
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (Global.IsTouch == 1) return;
                LYJLCXWindows jjb = new LYJLCXWindows();
                Global.WindowsList.Add(jjb);
                jjb.ShowDialog();
                Global.WindowsList.Remove(jjb);
            }
            catch (Exception ex)
            { }
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (Global.IsTouch == 1)
                //    return;
                //Global.MessageWindow.ShowDialog("开门!", Enum.MessageButton.Close, Enum.MessageStyle.Error);
                string strException;
                Global.lockOpt.OpenDoor(out strException);
            }
            catch (Exception ex)
            { }
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            DetailsWindow o = new DetailsWindow();
            Global.WindowsList.Add(o);
            o.ShowDialog();
            Global.WindowsList.Remove(o);
        }

        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (Global.IsTouch == 1) return;
                InputNumberWindow gmm = new InputNumberWindow();
                Global.WindowsList.Add(gmm);
                gmm.ShowDialog();
                Global.WindowsList.Remove(gmm);
            }
            catch (Exception ex)
            { }
        }
    }
}
