﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImpinjModule
{
    public class GPIOConfigs
    {
        public GPIValue GPI0_VALUE;
        public GPIValue GPI1_VALUE;
        public GPOValue GPO0_VALUE;
        public GPOValue GPO1_VALUE;
    }

    public enum GPIValue
    {
        None = 0,
        Inventory = 1,
        Input = 2,
        Hign = 3,
        Low = 4,
    }

    public enum GPOValue
    {
        Low = 0,
        Hign = 1,
    }
}
