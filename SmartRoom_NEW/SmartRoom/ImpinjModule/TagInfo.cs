﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImpinjModule
{
    public class TagInfo : INotifyPropertyChanged
    {
        public string Epc { get; set; }
        public int PortnNmber { get; set; }
        public float Rssi { get; set; }
        public string Tid { get; set; }
        public int Count
        {
            get { return _count; }
            set { _count = (int)value; OnPropertyChanged("Count"); }
        }
        public DateTime TimeInFirst { get; set; }
        public DateTime TimeInAlive { get; set; }
        public bool IsAlive
        {
            get { return _isAlive; }
            set { _isAlive = (bool)value; OnPropertyChanged("IsAlive"); }
        }
        public bool IsTimeOut
        {
            get { return _isTimeout; }
            set { _isTimeout = (bool)value; OnPropertyChanged("IsTimeOut"); }
        }
        public bool IsSended { get; set; }

        public TagAlarm AlarmType
        {
            get { return _statue; }
            set { _statue = (TagAlarm)value; OnPropertyChanged("AlarmType"); }
        }

        protected int _count;
        protected bool _isAlive;
        protected bool _isTimeout;
        protected TagAlarm _statue;

        protected void OnPropertyChanged(string info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;



        public TagInfo() { }
        public TagInfo(string epc, int port, float rssi, string tid, int current_count, DateTime intime, DateTime aliveByTime)
        {
            Epc = epc;
            PortnNmber = port;
            Rssi = rssi;
            Tid = tid;
            Count = current_count;
            TimeInFirst = intime;
            TimeInAlive = aliveByTime;
            IsAlive = true;
            IsTimeOut = false;
            IsSended = false;
        }
    }

    public class readTagQueue : ObservableCollection<TagInfo>
    {
        public readTagQueue() { }
        public readTagQueue(string epc, int port, float rssi, string tid, int count, DateTime dtfirst, DateTime dtnow)
        {
            Add(new TagInfo(epc, port, rssi, tid, count, dtfirst, dtnow));
        }
    }

    public class EpcInfo
    {
        private string _epc;

        public string Epc
        {
            get { return _epc; }
            set { _epc = value; }
        }

        private string _rssi;

        public string Rssi
        {
            get { return _rssi; }
            set { _rssi = value; }
        }

        private string _port;

        public string Port
        {
            get { return _port; }
            set { _port = value; }
        }
    }

    public enum TagAlarm
    {
        Silence = 1,
        Alarm = 2,
    }
}