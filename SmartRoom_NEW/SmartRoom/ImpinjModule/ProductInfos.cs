﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImpinjModule
{
    public class ProductInfos
    {
        public string ANTENNA_NUMBER;
        public List<AntennaPorts> ANTENNA_PORT_EXISTS_LIST;
        public string CHIP_TYPE;
        public string COMPANY_NO;
        public string MODEL_SEQUENCE_NUMBER;
        public string MODEL_TYPE;
        public string MODEL_VERSION;
        public string PRODUCT_DATE;
        public string PRODUCT_TYPE;
    }
}
