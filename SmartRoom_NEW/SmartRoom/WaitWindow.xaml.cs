﻿using System;
using System.Windows;

namespace SmartRoom
{
    /// <summary>
    /// WaitWindow.xaml 的交互逻辑
    /// </summary>
    public partial class WaitWindow : Window
    {
        public WaitWindow(string message)
        {
            InitializeComponent();
            lbl_Message.Content = message;
            this.Topmost = true;
            this.Show();
        //    string videoSource = Environment.CurrentDirectory + "\\images\\wait.gif";
         //   if (System.IO.File.Exists(videoSource))
          //  {
            //   this.pic_wait.ImageLocation = videoSource;
           // }
        }

        public WaitWindow()
        {
            InitializeComponent();
            this.Topmost = true;
            this.Show();
        //    string videoSource = Environment.CurrentDirectory + "\\images\\wait.gif";
        //    if (System.IO.File.Exists(videoSource))
         //   {
          //      this.pic_wait.ImageLocation = videoSource;
          //  }
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            lbl_Message.Content = "请稍后.....";
            // string videoSource = Environment.CurrentDirectory + "\\images\\wait.gif";
            //   if (System.IO.File.Exists(videoSource))
            //   {
            //      this.pic_wait.ImageLocation = videoSource;
            //     }
        }

        public string Message
        {
            set 
            {
                lbl_Message.Content = value;
            }
        }
    }
}
