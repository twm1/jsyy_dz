﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Business;
using Entity;
using System.Threading;

namespace SmartRoom
{
    /// <summary>
    /// LYJLCXWindows.xaml 的交互逻辑
    /// </summary>
    public partial class LYJLCXWindows : Window
    {
        InBillInfoBusiness _business = new InBillInfoBusiness();
        List<InBillInfo> billList = new List<InBillInfo>();
        List<InBillDetailInfo> billDetailList = new List<InBillDetailInfo>();
        public LYJLCXWindows()
        {
            InitializeComponent();
        }


        private void date1_ValueChanged(object sender, EventArgs e)
        {
            txt_DT1.Text = date1.Value.ToString("yyyy-MM-dd HH:mm:ss");
        }

        private void date2_ValueChanged(object sender, EventArgs e)
        {
            txt_DT2.Text = date2.Value.ToString("yyyy-MM-dd HH:mm:ss");
        }
        /// <summary>
        /// 领用归还点击查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Refresh_Click(object sender, RoutedEventArgs e)
        {

            //判断是否是当前时间，如果是加一小时如果不是不加
            string datatime = DateTime.Now.ToString("yyyy-MM-dd HH");
            string datatime2 = date2.Value.ToString("yyyy-MM-dd HH");
            if (datatime2.Equals(datatime))
            {
                datatime = DateTime.Now.AddHours(+1).ToString("yyyy-MM-dd HH:mm:ss");
            }
            else
            {
                datatime = date2.Value.ToString("yyyy-MM-dd HH:mm:ss");
            }
            try
            {
                if (chx_GH.IsChecked == false && chx_LY.IsChecked == false)
                {
                    Global.MessageWindow.ShowDialog("请先选择需要查询的单据类型!", Enum.MessageButton.Close, Enum.MessageStyle.Warn);
                    return;
                }

                string LX = "";
                if (chx_LY.IsChecked == true && chx_GH.IsChecked == false)
                {
                    LX = chx_LY.Content.ToString();
                }
                else if (chx_LY.IsChecked == false && chx_GH.IsChecked == true)
                {
                    LX = chx_GH.Content.ToString();
                }
                else if (chx_GH.IsChecked == true && chx_LY.IsChecked == true)
                {
                    LX = "全部";
                }
                /*if (CpName.Text!="")
                {
                    billList = _business.GetBillInfo(Global.forcerInfo.KFID, Global.forcerInfo.SJKFID, date1.Value.ToString("yyyy-MM-dd HH:mm:ss"), datatime, LX);
                }
                else
                {
                    billList = _business.GetBillInfo(Global.forcerInfo.KFID, Global.forcerInfo.SJKFID, date1.Value.ToString("yyyy-MM-dd HH:mm:ss"), datatime, LX);
                }*/
                billList = _business.GetBillInfo(Global.forcerInfo.KFID, date1.Value.ToString("yyyy-MM-dd HH:mm:ss"), datatime, LX);
                billList = billList.OrderByDescending(T => T.SJ).ThenByDescending(T => T.SJ).ToList();
                lvw_DJ.ItemsSource = billList;
                lvw_DJ.Items.Refresh();
            }
            catch (Exception ex)
            {
                Global.MessageWindow.ShowDialog("查询历史单据出现异常！"+ex.Message, Enum.MessageButton.Close, Enum.MessageStyle.Error);
                return;
            }
         }

        private void btn_Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            lvw_DJ.ItemsSource = billList;
            lvw_DJMX.ItemsSource = billDetailList;
            chx_LY.IsChecked = chx_GH.IsChecked = true;
            lbl_LYKS.Content = Global.forcerInfo.KfName;

            string times = date1.Value.ToString("yyyy-MM-dd") + " " + "07:30:00"; ;

            date1.Value = Convert.ToDateTime(times);
            txt_DT1.Text = date1.Value.ToString("yyyy-MM-dd  HH:mm:ss");       
            txt_DT2.Text = date2.Value.ToString("yyyy-MM-dd HH:mm:ss");
            //CpName.Focus();
        }

        private void lvw_DJ_MouseUp(object sender, MouseButtonEventArgs e)
        {
 

           try
             {
                 InBillInfo obj = lvw_DJ.SelectedItem as InBillInfo;
                 if (obj == null) return;

                 billDetailList = _business.GetBillDetailInfo(obj.DJID);
                 lvw_DJMX.ItemsSource = billDetailList;
                 lvw_DJMX.Items.Refresh();
             }
             catch (Exception ex)
             { }
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
               // btn_Refresh_Click(null, null);
            }
        }
    }
}
