﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ProductInfoEx
    {
        /// <summary>
        /// 序号
        /// </summary>
        public int SequenceNumber { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        public string ProductName { get; set; }
        /// <summary>
        /// 规格
        /// </summary>
        public string Model { get; set; }
        /// <summary>
        /// 型号
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 生产厂商
        /// </summary>
        public string Manufacturer { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        public string Unit { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public int Number { get; set; } = 0;
        /// <summary>
        /// 产品ID
        /// </summary>
        public string CPID { get; set; }
    }
}
