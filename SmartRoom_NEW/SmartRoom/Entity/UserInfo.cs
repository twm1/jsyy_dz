﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class UserInfo
    {
        private string _iD;

        public string ID
        {
            get { return _iD; }
            set { _iD = value; }
        }

        private string _userID;

        public string UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }

        private string _userName;

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        private string _depID;

        public string DepID
        {
            get { return _depID; }
            set { _depID = value; }
        }

        private string _gH;

        public string GH
        {
            get { return _gH; }
            set { _gH = value; }
        }
        private byte[] facesinformation;
        public byte[] face
        {
            get { return facesinformation; }
            set { facesinformation = value; }
        }
        private string _pwd;

        public string Pwd
        {
            get { return _pwd; }
            set { _pwd = value; }
        }

        private string _realName;

        public string RealName
        {
            get { return _realName; }
            set { _realName = value; }
        }

        private string _state;

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        private byte[] _fingerPrint;

        public string KeyWork { get => _keyWork; set => _keyWork = value; }
        public List<KFInfo> UserKFList { get => _userKFList; set => _userKFList = value; }
        public byte[] FingerPrint { get => _fingerPrint; set => _fingerPrint = value; }

        private string _keyWork;

        private List<KFInfo> _userKFList;
    }
}
