﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class UserFingerPrint
    {
        private string _userID;
        private byte[] _fingerPrint;

        public string UserID { get => _userID; set => _userID = value; }
        public byte[] FingerPrint { get => _fingerPrint; set => _fingerPrint = value; }
    }
}
