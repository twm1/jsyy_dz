﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class KFInfo
    {
        private string _kFID;
        private string _kFMC;

        public string KFID { get => _kFID; set => _kFID = value; }
        public string KFMC { get => _kFMC; set => _kFMC = value; }
    }
}
