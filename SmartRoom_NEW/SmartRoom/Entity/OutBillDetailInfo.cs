﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class OutBillDetailInfo
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool _xZ;
        private string _cKMXID;

        private string _dJID;

        private string _cPID;
        private string _cPMC;
        private string _gG;
        private string _xH;
        private string _cPBH;

        private string _rKMXID;

        private string _batch;

        private string _yXQ;

        private int _sL;

        private decimal _jE;

        private decimal _dJ;

        private int _kCSL;
        private string _dW;
        private string _tagID;


        public string CKMXID
        {
            get { return _cKMXID; }
            set
            {
                _cKMXID = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("CKMXID"));
            }
        }
        public string DJID
        {
            get { return _dJID; }
            set
            {
                _dJID = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("DJID"));
            }
        }
        public string CPID
        {
            get { return _cPID; }
            set
            {
                _cPID = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("CPID"));
            }
        }
        public string RKMXID
        {
            get { return _rKMXID; }
            set
            {
                _rKMXID = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("RKMXID"));
            }
        }
        public string Batch
        {
            get { return _batch; }
            set
            {
                _batch = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Batch"));
            }
        }
        public string YXQ
        {
            get { return _yXQ; }
            set
            {
                _yXQ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("YXQ"));
            }
        }
        public int SL
        {
            get { return _sL; }
            set
            {
                _sL = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SL"));
            }
        }
        public decimal JE
        {
            get { return _jE; }
            set
            {
                _jE = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("JE"));
            }
        }
        public decimal DJ
        {
            get { return _dJ; }
            set
            {
                _dJ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("DJ"));
            }
        }
        public int KCSL
        {
            get { return _kCSL; }
            set
            {
                _kCSL = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("KCSL"));
            }
        }
        public string CPMC
        {
            get { return _cPMC; }
            set
            {
                _cPMC = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("CPMC"));
            }
        }
        public string GG
        {
            get { return _gG; }
            set
            {
                _gG = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("GG"));
            }
        }
        public string XH
        {
            get { return _xH; }
            set
            {
                _xH = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("XH"));
            }
        }
        public string CPBH
        {
            get { return _cPBH; }
            set
            {
                _cPBH = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("CPBH"));
            }
        }
        public bool XZ
        {
            get { return _xZ; }
            set
            {
                _xZ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("XZ"));
            }
        }

        public string DW
        {
            get { return _dW; }
            set
            {
                _dW = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("DW"));
            }
        }

        public string TagID { get => _tagID; set => _tagID = value; }
    }
}
