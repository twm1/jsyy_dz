﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class OutRangeInfo
    {
        /// <summary>
        /// 序号
        /// </summary>
        public int SequenceNumber { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        public string ProductName { get; set; }
        /// <summary>
        /// 规格
        /// </summary>
        public string Model { get; set; }
        /// <summary>
        /// 型号
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 生产厂商
        /// </summary>
        public string Manufacturer { get; set; }
        /// <summary>
        /// 现有数量
        /// </summary>
        public int NowNumber { get; set; }
        /// <summary>
        /// CPID
        /// </summary>
        public string CPID { get; set; }
    }
}
