﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class LSJJBInfo
    {
        private string _iD;
        private string _cKID;
        private string _ckName;
        private string _beginDate;
        private string _endDate;
        private string _userID;
        private string _userName;
        private string _remark;

        public string ID { get => _iD; set => _iD = value; }
        public string CKID { get => _cKID; set => _cKID = value; }
        public string CkName { get => _ckName; set => _ckName = value; }
        public string BeginDate { get => _beginDate; set => _beginDate = value; }
        public string EndDate { get => _endDate; set => _endDate = value; }
        public string UserID { get => _userID; set => _userID = value; }
        public string UserName { get => _userName; set => _userName = value; }
        public string Remark { get => _remark; set => _remark = value; }
    }
}
