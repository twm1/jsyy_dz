﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class OutBillInfo
    {
        private string _billNo;

        private string _userID;

        private string _dJID;

        private string _cKKFID;

        private string _rKKFID;

        private string _cKType;

        private string _userName;

        private string _remark;

        private string _depID;

        private List<OutBillDetailInfo> _detailList;

        public string BillNo { get => _billNo; set => _billNo = value; }
        public string UserID { get => _userID; set => _userID = value; }
        public string DJID { get => _dJID; set => _dJID = value; }
        public string CKKFID { get => _cKKFID; set => _cKKFID = value; }
        public string RKKFID { get => _rKKFID; set => _rKKFID = value; }
        public string CKType { get => _cKType; set => _cKType = value; }
        public string UserName { get => _userName; set => _userName = value; }
        public string Remark { get => _remark; set => _remark = value; }
        public List<OutBillDetailInfo> DetailList { get => _detailList; set => _detailList = value; }
        public string DepID { get => _depID; set => _depID = value; }
    }
}
