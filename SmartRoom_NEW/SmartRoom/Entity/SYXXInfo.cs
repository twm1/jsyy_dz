﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class SYXXInfo : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string xM;
        private string bRH;
        private string cWH;
        private int sL;

        public string XM { get => xM; set => xM = value; }
        public string BRH { get => bRH; set => bRH = value; }
        public string CWH { get => cWH; set => cWH = value; }
        public int SL { get => sL; set => sL = value; }
    }
}
