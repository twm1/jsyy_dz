﻿/* ==============================================================================
  *	Copyright (C) 2013 cytel, ltd. All right reserved。
  * FileName:  EncodHelper.cs, based on .net framwork 4.0.30319.296
  * Author:  LuoGang
  * Create Date:  LuoGang create at  2013-08-09 13:35:00
  * Version :  1.0.0.0
  * Function:  编码格式转换类
  * Modify:  
  * ==============================================================================
*/
using System;
using System.Text;
namespace SoftTool
{
    public class EncodHelper
    {
        #region MD5
        /// <summary>
        /// MD5加密
        /// </summary>
        /// <param name="pwd">密码</param>
        /// <returns>加密字符串</returns>
        public static string EncrptPwd(string pwd)
        {
            byte[] enPwd = System.Security.Cryptography.MD5.Create().ComputeHash(System.Text.ASCIIEncoding.UTF8.GetBytes(pwd));
            string str = Convert.ToBase64String(enPwd);
            return str;
        }

        /// <summary>
        /// MD5哈希码加密
        /// </summary>
        /// <param name="pwd">密码</param>
        /// <returns>加密字符串</returns>
        public static string EncrptPwdToHex(string pwd)
        {
            byte[] enPwd = System.Security.Cryptography.MD5.Create().ComputeHash(System.Text.ASCIIEncoding.UTF8.GetBytes(pwd));
            string str="";
            for (int i = 0; i < enPwd.Length; i++)
            {
                str = str + enPwd[i].ToString("x2");
            }
            return str.ToLower();
        }
        #endregion

        #region 进制转换
        /// <summary> 
        /// 字符串转16进制字节数组 
        /// </summary> 
        /// <param name="hexString"></param> 
        /// <returns></returns> 
        public static byte[] strToToHexByte(string hexString, char separator)
        {
            string[] hex = hexString.Split(separator);
            byte[] returnBytes = new byte[hex.Length];
            for (int i = 0; i < returnBytes.Length; i++)
            {
                returnBytes[i] = Convert.ToByte(hex[i], 16);
            }
            return returnBytes;
        }

        
        /// <summary>
        /// 进制转换
        /// </summary>
        /// <param name="value">数据</param>
        /// <param name="fromBase">进制</param>
        /// <returns></returns>
        public static string ConvertValue(int value, int fromBase) 
        {
            return Convert.ToString(value, fromBase);    
        }

        /// <summary>
        /// 进制转换
        /// </summary>
        /// <param name="value">数据</param>
        /// <param name="fromBase">进制</param>
        /// <returns></returns>
        public static string ConvertValue(string value, int fromBase)
        {
            return Convert.ToString(int.Parse(value) , fromBase); 
        }

        /// <summary>
        /// 字符串进制转换
        /// </summary>
        /// <param name="fromBase">转换进制</param>
        /// <param name="hexString">字符串</param>
        /// <param name="separator">分隔符</param>
        /// <returns></returns>
        public static string ConvertValue(int fromBase, string hexString, char separator)
        {
            string[] hex = hexString.Substring(0, hexString.Length - 1).Split(separator);

            string str = "";
            foreach (var item in hex)
            {
                str += ConvertValue(item, fromBase) + ",";
            }
            return str.Substring(0, str.Length - 1); ;
        }
        #endregion

        #region 其他
        /// <summary>
        /// 转换Http编码格式
        /// </summary>
        /// <param name="msg">转换消息</param>
        /// <returns>Html编码格式</returns>
        public static string UrlEncode(string strCode)
        {
            StringBuilder sb = new StringBuilder();
            byte[] byStr = System.Text.Encoding.UTF8.GetBytes(strCode); //默认是System.Text.Encoding.Default.GetBytes(str)
            System.Text.RegularExpressions.Regex regKey = new System.Text.RegularExpressions.Regex("^[A-Za-z0-9]+$");
            for (int i = 0; i < byStr.Length; i++)
            {
                string strBy = Convert.ToChar(byStr[i]).ToString();
                if (regKey.IsMatch(strBy))
                {
                    //是字母或者数字则不进行转换  
                    sb.Append(strBy);
                }
                else
                {
                    sb.Append(@"%" + Convert.ToString(byStr[i], 16));
                }
            }
            return (sb.ToString());
        }

        /// <summary>
        /// 转换String类型
        /// </summary>
        /// <param name="o">值</param>
        /// <returns></returns>
        public static string GetString(object o)
        {
            return Convert.ToString(o);
        }
        #endregion

        #region LN加密
        /// <summary>
        /// LN加密转换
        /// </summary>
        /// <param name="value">转换进制</param>
        /// <param name="fromBase">格式</param>
        /// <returns></returns>
        private static string ConvertLNValue(string value, int fromBase)
        {
            return Convert.ToString(int.Parse(value) / 7, fromBase);
        }

        /// <summary>
        /// LN加密转换
        /// </summary>
        /// <param name="fromBase">转换进制</param>
        /// <param name="hexString">字符串</param>
        /// <param name="separator">分隔符</param>
        /// <returns></returns>
        private static string ConvertLNValue(int fromBase, string hexString, char separator)
        {
            string[] hex = hexString.Split(separator);
            
            string LNValue = "";
            foreach (var item in hex)
            {
                LNValue += ConvertLNValue(item, fromBase) + ",";
            }
            return LNValue.Substring(0, LNValue.Length - 1);
        }

        /// <summary>
        /// LN加密
        /// </summary>
        /// <param name="hexString">字符串</param>
        /// <returns>加密字符串</returns>
        public static string CreatSecurityLN(string hexString) 
        {
            byte[] byteString = Encoding.Unicode.GetBytes(hexString);
            string LNvalue = string.Empty;
            for (int i = 0; i < byteString.Length; i++)
            {
                LNvalue = LNvalue + (Convert.ToInt32(byteString[i].ToString("X2"), 16) * 7) + ","; //16进制转10进制
            }
            return LNvalue.Substring(0, LNvalue.Length - 1);
        }

        /// <summary> 
        /// 字符串转16进制字节数组 
        /// </summary> 
        /// <param name="hexString"></param> 
        /// <returns></returns> 
        public static byte[] LNToHexByte(string hexString, char separator)
        {
            string[] hex = hexString.Split(separator);
            byte[] returnBytes = new byte[hex.Length];
            for (int i = 0; i < returnBytes.Length; i++)
            {
                returnBytes[i] = Convert.ToByte(hex[i], 16);
            }
            return returnBytes;
        }

        /// <summary>
        /// LN解密
        /// </summary>
        /// <param name="hexString">加密字符</param>
        /// <returns>转换后数据</returns>
        public static string DecryptionLN(string hexString) 
        {
            hexString = ConvertLNValue(16, hexString, ',');//10进制转16进制
            hexString = Encoding.Unicode.GetString(LNToHexByte(hexString, ',' ));//16进制转模版
            return hexString;
        }
        #endregion
    }
}
