﻿using System;
using System.IO;

namespace SoftTool
{
	/// <summary>
	/// Serialize/Deserialize object to file or stream
	/// </summary>
	public class SerializeHelper
	{
		/// <summary>
		/// Serialize an object to a given file
		/// </summary>
		/// <param name="filePath"></param>
		/// <param name="objToSerialize"></param>
		/// <returns></returns>
        static public bool Serialize(string filePath, object objToSerialize)
		{
				return XmlHelper.Serialize(filePath, objToSerialize);
		}

		/// <summary>
		/// Deserialize an object from a given file
		/// </summary>
		/// <param name="filePath"></param>
		/// <param name="objType"></param>
		/// <returns></returns>
		static public object Deserialize(string filePath, Type objType)
		{
				return XmlHelper.Deserialize(filePath, objType);;
		}

        public static T Deserialize<T>(string filePath)
        {
            return XmlHelper.Deserialize<T>(filePath);
        }

		/// <summary>
		/// Serialize an object to a given stream
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="objToSerialize"></param>
        /// <param name="IsXML">true采用xml序列化</param>
		/// <returns></returns>
		static public bool Serialize(Stream stream, object objToSerialize)
		{
				return XmlHelper.Serialize(stream, objToSerialize);
		}

		/// <summary>
		/// 序列化
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="objType">类型</param>
		/// <returns></returns>
		static public object Deserialize(Stream stream, Type objType)
		{
				return XmlHelper.Deserialize(stream, objType);
		}

        static public T Clone<T>(T RealObject)
        {
            using (Stream objectStream = new MemoryStream())
            {
                //利用 System.Runtime.Serialization序列化与反序列化完成引用对象的复制  
                System.Runtime.Serialization.IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(objectStream, RealObject);
                objectStream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(objectStream);
            }
        }
	}
}
