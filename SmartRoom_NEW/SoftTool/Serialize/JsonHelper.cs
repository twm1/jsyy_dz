﻿using System;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace SoftTool
{
    public static class JsonHelper
    {
        public static string SerializeObject<T>(T t)
        {
            try
            {
                return JsonConvert.SerializeObject(t);
            }
            catch
            {
                return "";
            }
        }

        public static string SerializeObject(object o)
        {
            try
            {
                return JsonConvert.SerializeObject(o);
            }
            catch
            {
                return "";
            }
         
        }

        public static T DeserializeObject<T>(string json)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(json);
            }
            catch
            {
                return default(T);
            }
        }

        public static object DeserializeObject(string json)
        {
            try
            {
                return JsonConvert.DeserializeObject(json);
            }
            catch
            {
                return null;
            }
        }

        public static string GetJsonValue(string json,string key) 
        {
            try
            {
                JObject obj = JObject.Parse(json);
                return obj[key].ToString();
            }
            catch 
            {
                return "";
            }
        }

        public static string CreateJson(string key, string value)
        {
            try
            {
                JObject obj = new JObject(new JProperty(key, value));
                return obj.ToString();
            }
            catch
            {
                return "";
            }
        }

        public static string AddStringValue(string json, string key, string value) 
        {
            try
            {
                JObject o = JObject.Parse(json);
                o.Add(key, value);
                return o.ToString();
            }
            catch
            {
                return "";
            }
        }

        public static string AddJsonValue(string json, string key, string value)
        {
            try
            {
                JObject o = JObject.Parse(json);
                o.Add(key, JToken.Parse(value));
                return o.ToString();
            }
            catch
            {
                return "";
            }
        }
    }
}
